﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator {
    static class EscapeSequences {
        private static bool isInited = false;

        private static Dictionary<string, int> sequences2codes = new Dictionary<string, int>() {
            { "\\0",  0x00 },
            { "\\n",  0x0D },
            
            { "\\\\", 0x5C },
            { "\\\"", 0x22 },
            { "\\\'", 0x27 },

            { "\\x",  -1 }, // hex code prefix
        };

        private static Dictionary<int, string> codes2sequences = null;

        private static Dictionary<char, int> symbols2codes;
        private static Dictionary<int, char> codes2symbols;

        public static Dictionary<string, int> Sequences2Codes { 
            get {
                CheckAndInit();
                return sequences2codes;
            }
        }

        public static Dictionary<int, string> Codes2Sequences {
            get {
                CheckAndInit();
                return codes2sequences;
            }
        }

        public static Dictionary<char, int> Symbols2Codes {
            get {
                CheckAndInit();
                return symbols2codes;
            }
        }

        public static Dictionary<int, char> Codes2Symbols {
            get {
                CheckAndInit();
                return codes2symbols;
            }
        }

        private static void CheckAndInit() {
            if (!isInited) {
                isInited = true;
                codes2sequences = new Dictionary<int, string>();
                symbols2codes = new Dictionary<char, int>();
                codes2symbols = new Dictionary<int, char>();
                foreach (KeyValuePair<string, int> kvp in sequences2codes) {
                    codes2sequences.Add(kvp.Value, kvp.Key);
                    symbols2codes.Add(kvp.Key[1], kvp.Value);
                    codes2symbols.Add(kvp.Value, kvp.Key[1]);
                }
            }
        }
    }
}