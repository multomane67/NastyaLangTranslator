﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator {
    class SpectrumCharSet {
        private static Dictionary<char, int> symbols2codes = null;
        private static Dictionary<int, char> codes2symbols = null;
        private static bool isInited = false;

        public static Dictionary<char, int> Symbols2Codes {
            get {
                CheckAndInit();
                return symbols2codes;
            }
        }

        public static Dictionary<int, char> Codes2Symbols {
            get {
                CheckAndInit();
                return codes2symbols;
            }
        }

        private static void CheckAndInit() {
            if (!isInited) {
                isInited = true;

                
                symbols2codes = new Dictionary<char, int>();
                symbols2codes.Add('\0', 0x00);
                symbols2codes.Add('\n', 0x0D);
                AddSymbolsRange(symbols2codes, '!', ']', 0x21); // digits, upper case alphabet and other
                symbols2codes.Add('_', 0x5F);
                AddSymbolsRange(symbols2codes, 'a', '~', 0x61); // lower case alphabet and other


                codes2symbols = new Dictionary<int, char>();
                foreach (var kvp in symbols2codes) {
                    codes2symbols.Add(kvp.Value, kvp.Key);
                }
            }
        }

        private static void AddSymbolsRange(Dictionary<char,int> dict, char fromChar, char toChar, int fromCode) {
            int code = fromCode;
            for (char c = fromChar; c <= toChar; c++) {
                dict.Add(c, code);
                code++;
            }
        }
    }
}
