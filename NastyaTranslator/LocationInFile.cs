﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator {
    struct LocationInFile {
        public string fileName;
        public int line;
        public int column;

        //public int endingLine;
        //public int endingColumn;

        public bool IsHaveDefaultValue => fileName == null && line == 0 && column == 0;

        private string ConvertToString() {
            return $"{fileName}({line},{column})";
        }

        public static implicit operator string(LocationInFile loc) {
            return loc.ConvertToString();
        }

        public override string ToString() {
            return ConvertToString();
        }
    }
}
