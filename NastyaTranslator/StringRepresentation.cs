﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator {
    class StringRepresentation : ICloneable {

        public StringRepresentation() {
            Reset();
        }

        public int Length => length != -1 ? length : length = ProcessLength();

        public void Reset() {
            parts = new List<Part>();
            sb = new StringBuilder();
            length = -1;
        }

        public void AddChar(char c) {
            Part lastPart = GetLastPart();

            if (lastPart == null || lastPart.mode != Mode.String) {
                lastPart = new Part() { mode = Mode.String };
                parts.Add(lastPart);
            }

            sb.Append(c);

            length = -1;
        }

        public void AddCode(int code) {
            ProcessLastStringPart();

            parts.Add(new Part() { mode = Mode.Code, code = code });

            length = -1;
        }

        public object Clone() {
            var newStrRep = new StringRepresentation() {
                parts = new List<Part>(parts),
                sb = new StringBuilder(sb.ToString(), sb.Capacity),
                length = length,
            };

            return newStrRep;
        }

        public StringRepresentation Copy() {
            return (StringRepresentation)Clone();
        }

        public string BuildAsmStringDefinition() {
            ProcessLastStringPart();

            foreach (Part p in parts) {
                if (p.mode == Mode.String) {
                    sb.Append($"DEFM \"{p.str}\"\n");
                } else if (p.mode == Mode.Code) {
                    sb.Append($"DEFB {p.code}\n");
                } else {
                    throw new Exception("wrong behaviour");
                }
            }

            string result = sb.ToString();
            sb.Clear();
            return result;
        }

        public List<int> BuildCodes() {
            ProcessLastStringPart();

            List<int> result = new List<int>();

            foreach (Part p in parts) {
                if (p.mode == Mode.String) {
                    foreach (char c in p.str) {
                        if (SpectrumCharSet.Symbols2Codes.TryGetValue(c, out int val)) {
                            result.Add(val);
                        } else {
                            throw new Exception($"Undefined symbol '{c}' in spectrum char set");
                        }
                    }
                } else if (p.mode == Mode.Code) {
                    result.Add(p.code);
                } else {
                    throw new Exception("Not implemented!");
                }
            }

            return result;
        }

        private enum Mode {
            String,
            Code,
        }

        private List<Part> parts = new List<Part>();

        private StringBuilder sb = null;

        private int length = -1;

        private Part GetLastPart() {
            return parts.Count > 0 ? parts[parts.Count - 1] : null;
        }

        private void ProcessLastStringPart() {
            Part lastPart = GetLastPart();

            if (lastPart != null && lastPart.mode == Mode.String && sb.Length > 0) {
                lastPart.str = sb.ToString();
                sb.Clear();
            }
        }

        private int ProcessLength() {
            ProcessLastStringPart();

            int total = 0;
            foreach (Part p in parts) {
                if (p.mode == Mode.String) {
                    total += p.str.Length;
                } else if (p.mode == Mode.Code) {
                    total ++;
                } else {
                    throw new Exception("Not implemented mode!");
                }
            }
            return total;
        }

        private class Part {
            public Mode mode;
            public string str;
            public int code;
        }
    }

    static class StringRepresentationExtention {
        public static bool IsNullOrEmpty(this StringRepresentation strRep) {
            return strRep == null || strRep.Length == 0;
        }
    }
}
