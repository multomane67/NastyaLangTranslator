﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator {
    class MessageForUserException : Exception {
        public MessageForUserException(string message) 
            : base(message) 
        { }
    }
}
