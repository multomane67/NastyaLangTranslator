﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator {
    // standart types for translation
    class ST {

        public static readonly Definition BYTE = new Definition {
            name = "byte",
            min = 0,
            max = 255,
            bytes = 1,
        };

        public static readonly Definition WORD = new Definition {
            name = "word",
            min = 0,
            max = 65535,
            bytes = 2,
        };

        public static Dictionary<string, Definition> Name2Def {
            get {
                CheckAndInit();
                return name2def;
            }
        }

        public static List<Definition> Defs {
            get {
                CheckAndInit();
                return defs;
            }
        }

        public static Definition GetMinimumTypeForStorage(int value) {
            if (value < 256)
                return BYTE;
            else
                return WORD;
        }

        private static bool isInited = false;

        private static Dictionary<string, Definition> name2def = null;
        private static List<Definition> defs = null;

        private static void CheckAndInit() 
        {
            if (!isInited) {

                name2def = new Dictionary<string, Definition>();
                name2def.Add(BYTE, BYTE);
                name2def.Add(WORD, WORD);

                defs = new List<Definition>();
                defs.Add(BYTE);
                defs.Add(WORD);

                isInited = true;
            }
        }


        public struct Definition {
            public string name;
            public int min;
            public int max;
            public int bytes;

            public override string ToString() {
                return name;
            }

            public static bool operator == (string str, Definition def) {
                return str == def.name;
            }
            public static bool operator !=(string str, Definition def) {
                return str != def.name;
            }
            public static bool operator ==(Definition def, string str) {
                return str == def.name;
            }
            public static bool operator !=(Definition def, string str) {
                return str != def.name;
            }

            public override bool Equals(object obj) {
                return base.Equals(obj);
            }

            public override int GetHashCode() {
                return base.GetHashCode();
            }

            public static implicit operator string (Definition def) {
                return def.name;
            }
        }
    }
}
