﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using NastyaTranslator.Parsing;
using NastyaTranslator.Parsing.Classes.CommandLine;
using NastyaTranslator.Translation;

namespace NastyaTranslator {

    class NastyaTranslator {

        static void Main(string[] args) {
            bool isNeedWaitAnyKey = false;

            if (args.Length == 0) {
                PrintCommandLineUsage();
            } else {
                var parsedCommandLine = new CommandLineArguments();
                
                if (!parsedCommandLine.TryParseString(string.Join(" ", args), out string error)) {
                    Console.WriteLine($"command line parsing error:\n {error}\n");

                    PrintCommandLineUsage();
                } else {
                    string mode = parsedCommandLine.mode;
                    isNeedWaitAnyKey = parsedCommandLine.isHaveWaitAnyKey;
                    List<string> srcFileNames = parsedCommandLine.sourceFiles;
                    string dstFileName = parsedCommandLine.translatedFile;

                    if (!dstFileName.EndsWith(".asm"))
                        dstFileName += ".asm";

                    Console.WriteLine($"mode:    {mode}");
                    Console.WriteLine($"sources: {srcFileNames.ToStrList("\n         ", (s) => $"\"{s}\"")}");
                    Console.WriteLine($"target:  \"{dstFileName}\"");


                    ASourceParser parser = new NastyaSourceParser();

                    foreach (var srcFileName in srcFileNames) {
                        if (!srcFileName.EndsWith(".n")) {
                            Console.WriteLine($"Source file '{srcFileName}' have a wrong extension. Now supported only '.n'.");
                            return;
                        }
                    }

                    ProgramNode prgNode = parser.MakeProgramNode(srcFileNames);

                    if (prgNode != null) {
                        Console.WriteLine($"Parsing complete.");
                    } else {
                        Console.WriteLine($"Parsing failed.");
                    }

                    if (prgNode != null) {
                        try {
                            prgNode.CheckOnValid();

                            if (mode == "-asm") {
                                prgNode.TranslateToFile_Assembler(dstFileName);
                            } else if (mode == "-bc") {
                                prgNode.TranslateToFile_ByteCode(dstFileName);
                            }

                            Console.WriteLine($"Translation complete.");
                        } catch (MessageForUserException e) {
                            Console.WriteLine($"translation error:\n {e.Message}");
                            Console.WriteLine($"Translation failed.");
                        }
                    } else {
                        Console.WriteLine($"Translation failed.");
                    }
                }
            }

            if (isNeedWaitAnyKey) {
                Console.Write("\n\npress any key..");
                Console.ReadKey();
            }
        }

        public static void PrintCommandLineUsage() {
            // if change this than change command-line.md
            Console.WriteLine("usage: nt <-asm | -bc> [--wait-any-key] <source>... <translated>");
            Console.WriteLine();
            Console.WriteLine("  -asm   Translation into assembler code \"as is\" line by line.");
            Console.WriteLine("         (will be optimized in future)");
            Console.WriteLine();
            Console.WriteLine("  -bc    Translation into byte code + assembler interpretator.");
            Console.WriteLine();
            Console.WriteLine("  --wait-any-key   Wait an any key after work.");
            Console.WriteLine();
            Console.WriteLine("  source           Source code for translation. It is a file name");
            Console.WriteLine("                   with extention \".n\". Can be used one or many");
            Console.WriteLine("                   source files separated by spaces.");
            Console.WriteLine();
            Console.WriteLine("  translated       Result of translation will be saved into");
            Console.WriteLine("                   translated file name with .asm extention.");
            Console.WriteLine("                   This is \".asm\" file.");
        }
    }
}

