﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class PrintTest_OperatorCalc {
    public static void ToConsole() {
        string[][] resAndCheck = new string[][] {
            new string[]{
                "r",
                "<asm>\n\tLD A, {0}\n\tLD (_r_true), A\n\tcall Test_R\n</asm>"
            },
            new string[]{
                "wr",
                "<asm>\n\tLD HL, {0}\n\tLD (_wr_true), HL\n\tcall Test_WR\n</asm>"
            },
        };

        string[] ops = new string[] {
            "+",
            "-",
            //"{{",
            //"}}",
        };

        string[][] args = new string[][] {
            // byte consts
            new string[] { "", "c=\"0\"",   "0"},
            new string[] { "", "c=\"3\"",   "3"},
            new string[] { "", "c=\"255\"", "255"},
            // byte vars
            new string[] { "<set av=\"{0}\" bc=\"0\" />\n",     "v=\"{0}\"",   "0" },
            new string[] { "<set av=\"{0}\" bc=\"3\" />\n",     "v=\"{0}\"",   "3" },
            new string[] { "<set av=\"{0}\" bc=\"255\" />\n",   "v=\"{0}\"",   "255" },
            // word consts
            new string[] { "", "c=\"256\"",    "256"},
            new string[] { "", "c=\"3000\"",   "3000"},
            new string[] { "", "c=\"65535\"",  "65535"},
            // word vars
            new string[] { "<set av=\"w{0}\" bc=\"256\" />\n",   "v=\"w{0}\"",   "256" },
            new string[] { "<set av=\"w{0}\" bc=\"3000\" />\n",  "v=\"w{0}\"",   "3000" },
            new string[] { "<set av=\"w{0}\" bc=\"65535\" />\n", "v=\"w{0}\"",   "65535" },
        };

        Console.WriteLine(sizeof(byte));
        Console.WriteLine(byte.MaxValue);
        Console.WriteLine(sizeof(ushort));
        Console.WriteLine(ushort.MaxValue);
        ushort tw = 257;
        Console.WriteLine((byte)tw);

        int num = 1;
        foreach (string[] resCheck in resAndCheck) {
            for (int oi = 0; oi < ops.Length; oi++) {
                for (int ai = 0; ai < args.Length; ai++) {
                    for (int bi = 0; bi < args.Length; bi++) {
                        Console.WriteLine($"<!-- TEST CALC #{num++} --> ");

                        string initVarA = string.Format(args[ai][0], "a");
                        string initVarB = string.Format(args[bi][0], "b");

                        string varNameOrNumberA = string.Format(args[ai][1], "a");
                        string varNameOrNumberB = string.Format(args[bi][1], "b");

                        Console.WriteLine($"{initVarA}{initVarB}<calc r=\"{resCheck[0]}\" a{varNameOrNumberA} op=\"{ops[oi]}\" b{varNameOrNumberB} />");

                        ushort a = ushort.Parse(args[ai][2]);
                        ushort b = ushort.Parse(args[bi][2]);
                        ushort r = 0;
                        if (ops[oi] == "+") {
                            a += b;
                        } else if (ops[oi] == "-") {
                            a -= b;
                        } else if (ops[oi] == "{{") {
                            a <<= b;
                        } else if (ops[oi] == "}}") {
                            a >>= b;
                        } else {
                            throw new Exception("Error!");
                        }

                        if (resCheck[0] == "r") {
                            r = (byte)a;
                        } else if (resCheck[0] == "wr") {
                            r = a;
                        }

                        Console.WriteLine(string.Format(resCheck[1], r));
                    }
                }
            }
        }
    }
}

