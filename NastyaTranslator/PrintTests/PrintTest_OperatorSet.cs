﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


class PrintTest_OperatorSet {
    public static void ToConsole() {
        string[][] resAndCheck = new string[][] {
            new string[]{
                "r",
                "<asm>\n\tLD A, {0}\n\tLD (_r_true), A\n\tcall Test_R\n</asm>"
            },
            new string[]{
                "wr",
                "<asm>\n\tLD HL, {0}\n\tLD (_wr_true), HL\n\tcall Test_WR\n</asm>"
            },
        };

        string[][] bargs = new string[][] {
            // byte consts
            new string[] { "", "bc=\"0\"",   "0"},
            new string[] { "", "bc=\"3\"",   "3"},
            new string[] { "", "bc=\"6\"",   "6"},
            new string[] { "", "bc=\"255\"", "255"},
            // byte vars
            new string[] { "<set av=\"a\" bc=\"0\" />\n",   "bv=\"a\"",   "0" },
            new string[] { "<set av=\"a\" bc=\"3\" />\n",   "bv=\"a\"",   "3" },
            new string[] { "<set av=\"a\" bc=\"6\" />\n",   "bv=\"a\"",   "6" },
            new string[] { "<set av=\"a\" bc=\"255\" />\n", "bv=\"a\"",   "255" },
            // word consts
            new string[] { "", "bc=\"256\"",    "256"},
            new string[] { "", "bc=\"3000\"",   "3000"},
            new string[] { "", "bc=\"48000\"",  "48000"},
            new string[] { "", "bc=\"65535\"",  "65535"},
            // word vars
            new string[] { "<set av=\"wa\" bc=\"256\" />\n",     "bv=\"wa\"",   "256" },
            new string[] { "<set av=\"wa\" bc=\"3000\" />\n",    "bv=\"wa\"",   "3000" },
            new string[] { "<set av=\"wa\" bc=\"48000\" />\n",   "bv=\"wa\"",   "48000" },
            new string[] { "<set av=\"wa\" bc=\"65535\" />\n",   "bv=\"wa\"",   "65535" },
        };

        int num = 1;
        foreach (var result in resAndCheck) {

            foreach (string[] barg in bargs) {
                Console.WriteLine($"<!-- TEST SET #{num++} --> ");
                Console.WriteLine($"{barg[0]}<set av=\"{result[0]}\" {barg[1]} />");
                string res = "";
                uint val = uint.Parse(barg[2]);

                if (result[0] == "r") {
                    byte register = (byte)val;
                    res = register.ToString();
                } else if (result[0] == "wr") {
                    ushort register = (ushort)val;
                    res = register.ToString();
                } else {
                    throw new Exception("Error!");
                }

                Console.WriteLine(string.Format(result[1], res));
            }
        }
    }
}

