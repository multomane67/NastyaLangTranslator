﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NastyaTranslator.Translation;

namespace NastyaTranslator.Parsing.Classes {
    class Tag : Object {
    }

    enum Status {
        Idle,               // whait for first char
        InProgress,         // some chars was succesfuly putted - whait next chars
        Complete,           // all chars was succesfuly putted - we can get a OperatorNode
        Error,              // something was wrong - we can get error message;
    }

    abstract class AParseExam {
        protected AParseRoot parseRoot = null;

        public Tag tag;

        public string nameForError = null;
        public Status Status { get; protected set; } = Status.Idle;

        public LocationInFile lif;

        protected ErrorMessage errorMessage = null;
        public virtual ErrorMessage ErrorMessage { 
            get {
                if (errorMessage != null 
                 && errorMessage.nearestErrorNodeName == null 
                 && nameForError != null)
                    errorMessage.nearestErrorNodeName = nameForError;

                return errorMessage;
            }
        }

        public virtual void Init(AParseRoot parseRoot) {
            this.parseRoot = parseRoot;
        }

        public abstract void Reset();

        public virtual AParseExam GetBy(Tag tag) {
            return this.tag == tag ? this : null;
        }

        public virtual bool PutSymbol(char sym) {
            bool wasTaken;

            if (Status != Status.Error && Status != Status.Complete) {

                bool isValid = CheckOnValid(sym);

                if (isValid) {

                    bool isComplete = CheckOnComplete_afterValid(out bool isNeedTakeLast);

                    if (isComplete) {
                        Status = Status.Complete;
                        wasTaken = isNeedTakeLast;
                    } else {
                        if (Status == Status.Idle)
                            lif = parseRoot.currLoc;
                        Status = Status.InProgress;
                        wasTaken = true;
                    }

                } else {
                    Status = Status.Error;
                    wasTaken = false;
                }

            } else {
                wasTaken = false;
            }

            return wasTaken;
        }

        public abstract bool CheckOnValid(char sym);

        public abstract bool CheckOnComplete_afterValid(out bool takeLastSymbol);

        protected abstract ANode ConstructNode();

        public virtual ANode GetNode() {
            if (Status == Status.Complete) {
                ANode node = ConstructNode();
                if (node != null)
                    node.lif = lif;
                return node;
            } else {
                throw new Exception("status are not completed");
            }
        }
    }
}
