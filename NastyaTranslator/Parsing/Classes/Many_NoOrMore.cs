﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NastyaTranslator.Translation;

namespace NastyaTranslator.Parsing.Classes {
    class Many_NoOrMore : AParseExam {

        private AParseExam repeatParser = null;

        private List<ANode> tmpNodes = null;

        private bool isPutted = false;
        private bool isValid = false;
        private bool wasInProgress = false;

        public Many_NoOrMore(AParseExam repeatParser) {
            this.repeatParser = repeatParser;
        }

        public override void Init(AParseRoot parseRoot) {
            base.Init(parseRoot);
            repeatParser.Init(parseRoot);
        }

        public override void Reset() {
            Status = Status.Idle;

            if (repeatParser == null) {
                throw new Exception("no repeatable parse exam");
            } else {
                repeatParser.Reset();
            }

            tmpNodes = new List<ANode>();

            isPutted = false;
            isValid = false;
            wasInProgress = false;
        }

        public override bool PutSymbol(char sym) {
            return base.PutSymbol(sym);
        }

        public override bool CheckOnValid(char sym) {

            do {

                isPutted = repeatParser.PutSymbol(sym);

                Status currStatus = repeatParser.Status;

                isValid = currStatus != Status.Error;

                if (currStatus == Status.InProgress) {
                    wasInProgress = true;
                } else if (currStatus == Status.Complete) {
                    ANode node = repeatParser.GetNode();
                    if (node != null)
                        tmpNodes.Add(node);
                    repeatParser.Reset();
                    wasInProgress = false;
                } else if (currStatus == Status.Error && wasInProgress) {
                    errorMessage = repeatParser.ErrorMessage;
                }

            } while (isValid && !isPutted);

            return !(!isValid && wasInProgress);
        }

        public override bool CheckOnComplete_afterValid(out bool takeLastSymbol) {
            takeLastSymbol = false;
            return !isValid && !wasInProgress;
        }

        protected override ANode ConstructNode() {
            return new ListOfNodes() {
                nodes = tmpNodes
            };
        }
    }
}
