﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NastyaTranslator.Translation;

namespace NastyaTranslator.Parsing.Classes
{
    class Spaces_OneOrMore : AParseExam
    {
        private HashSet<char> ValidSymbols => SpaceSymbols.hashSet;

        private char sym;
        private int count = 0;

        public override void Reset()
        {
            Status = Status.Idle;

            count = 0;
        }

        public override bool PutSymbol(char sym) {
            return base.PutSymbol(sym);
        }

        public override bool CheckOnValid(char sym)
        {
            this.sym = sym;

            bool isValid = ValidSymbols.Contains(sym);
            bool isFirst = count++ == 0;

            if (isFirst)
            {
                if (!isValid)
                    errorMessage = "Expected space one or more.";

                return isValid;
            }
            else
            {
                return true;
            }
        }

        public override bool CheckOnComplete_afterValid(out bool takeLastSymbol)
        {
            bool isValid = ValidSymbols.Contains(sym);
            bool isComplete = !isValid;

            takeLastSymbol = false;
            return isComplete;
        }

        protected override ANode ConstructNode()
        {
            return null;
        }
    }
}
