﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NastyaTranslator.Translation;

namespace NastyaTranslator.Parsing.Classes {
    class ParseCreator : AParseExam {
        private Func<AParseExam> createFunc = null;
        private AParseExam createdParser = null;

        protected AParseExam CreatedParser { 
            get {
                if (createdParser == null) {
                    createdParser = createFunc();
                    createdParser.Init(parseRoot);
                    createdParser.Reset();
                }

                return createdParser;
            }
        }

        public ParseCreator(Func<AParseExam> createFunc) {
            this.createFunc = createFunc;
        }

        

        public override void Reset() {
            this.Status = Status.Idle;
            createdParser = null;
        }

        public override bool PutSymbol(char sym) {
            bool result = CreatedParser.PutSymbol(sym);
            this.Status = CreatedParser.Status;
            this.lif = CreatedParser.lif;
            this.errorMessage = CreatedParser.ErrorMessage;
            return result;
        }

        public override bool CheckOnValid(char sym) {
            throw new WrongUsageException();
        }

        public override bool CheckOnComplete_afterValid(out bool takeLastSymbol) {
            throw new WrongUsageException();
        }

        protected override ANode ConstructNode() {
            return CreatedParser.GetNode();
        }
    }
}
