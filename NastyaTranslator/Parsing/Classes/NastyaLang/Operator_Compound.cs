﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NastyaTranslator.Translation;

namespace NastyaTranslator.Parsing.Classes.NastyaLang {
    class Operator_Compound : SequenceOf {

        private static Tag tagOperations = new Tag();

        public Operator_Compound(string nameForError)
            : base(new List<AParseExam>() {
                new Str("{"),
                new OperatorCreator_NoOrMore(nameForError){tag=tagOperations},
                new Str("}"),
            })
        {
        }

        protected override ANode ConstructNode() {
            var parseOperations = GetBy(tagOperations);
            return parseOperations.GetNode();
        }
    }
}
