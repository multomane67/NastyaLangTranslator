﻿using NastyaTranslator.Translation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator.Parsing.Classes.NastyaLang {
    class NumSymLiteral : AnyOf {

        public int value = -1;

        private static Tag tagIntLit = new Tag();
        private static Tag tagSymLit = new Tag();
        public NumSymLiteral() 
            : base (new List<AParseExam>() {
                new IntegerLiteral(){tag=tagIntLit},
                new SymbolLiteral(){tag=tagSymLit},
            })
        {
        }

        public override void Reset() {
            base.Reset();
            value = -1;
        }

        public override bool PutSymbol(char sym) {
            bool isPutted = base.PutSymbol(sym);

            if (Status == Status.Complete) {
                var parseIntLit = GetBy(tagIntLit) as IntegerLiteral;
                var parseSymLit = GetBy(tagSymLit) as SymbolLiteral;

                value = parseIntLit.Status == Status.Complete
                    ? parseIntLit.value
                    : parseSymLit.Status == Status.Complete
                        ? parseSymLit.value
                        : throw new Exception("wrong behaviour");
            }

            return isPutted;
        }

        protected override ANode ConstructNode() {
            throw new WrongUsageException();
        }
    }
}
