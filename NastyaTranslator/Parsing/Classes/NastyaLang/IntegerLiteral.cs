﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NastyaTranslator.Translation;

namespace NastyaTranslator.Parsing.Classes.NastyaLang
{
    class IntegerLiteral : AFillString
    {
        public int value = -1;

        protected override bool IsMatchSymbol(char sym, bool isFirst) {
            bool isDigit = sym.InDiapasone('0', '9');
            bool isHexLetter = sym.InDiapasone('a', 'f') || sym.InDiapasone('A', 'F');
            bool isOtherUsed = sym == 'x' || sym == 'X'; // for '0x' prefix in hexadecimal, binary prefix '0b' guaranted by isHexLetter

            return isFirst ?
                isDigit :
                isDigit || isHexLetter || isOtherUsed;
        }

        public override void Reset() {
            value = -1;
            base.Reset();
        }

        protected override bool PostProcess(StringBuilder sb, out string errorMessage) {
            string symbols = sb.ToString();

            bool isHaveHexPrefix = symbols.IndexOf("0x") == 0
                                || symbols.IndexOf("0X") == 0;

            bool isHaveBinPrefix = symbols.IndexOf("0b") == 0
                                || symbols.IndexOf("0B") == 0;

            try {

                if (isHaveHexPrefix) {
                    value = Convert.ToInt32(symbols.Substring(2), 16);
                } else if (isHaveBinPrefix) {
                    value = Convert.ToInt32(symbols.Substring(2), 2);
                } else {
                    value = Convert.ToInt32(symbols, 10);
                }

                errorMessage = null;
                return true;

            } catch (Exception e) {
                errorMessage = $"Can't convert '{symbols}' into Int32,\nmessage: '{e.Message}'.";
                return false;
            }
        }

        protected override string GetStartupMessage(char sym) {
            return $"Expected integer literal, finded '{sym}' (must  begins from digit, and be one or more characters in length).";
        }
    }
}
