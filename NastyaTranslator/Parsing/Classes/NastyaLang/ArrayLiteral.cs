﻿using NastyaTranslator.Translation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator.Parsing.Classes.NastyaLang {
    class ArrayLiteral : SequenceOf {

        public string numbersString = null;

        private static Tag tagFirstInteger = new Tag();
        private static Tag tagNextIntegers = new Tag();

        public ArrayLiteral()
            : base(new List<AParseExam>() {
                new Str("{"),
                new Spaces_NoOrMore(),
                new NumSymLiteral(){tag=tagFirstInteger},
                new Spaces_NoOrMore(),
                new Many_NoOrMore(
                    new NextNumSymLiteral()
                ){tag=tagNextIntegers},
                new Spaces_NoOrMore(),
                new Str("}"),
            }) 
        {

        }

        public override void Reset() {
            numbersString = null;
            base.Reset();
        }

        public override bool CheckOnComplete_afterValid(out bool takeLastSymbol) {
            bool complete = base.CheckOnComplete_afterValid(out takeLastSymbol);

            if (complete) {

                StringBuilder sb = new StringBuilder();

                var firstInteger = GetBy(tagFirstInteger) as NumSymLiteral;
                var nextIntegers = GetBy(tagNextIntegers) as Many_NoOrMore;

                if (firstInteger.Status == Status.Complete) {
                    sb.Append(firstInteger.value.ToString());
                    if (nextIntegers.Status == Status.Complete) {
                        var listIntegerLiterals = nextIntegers.GetNode() as ListOfNodes;
                        foreach (NextIntegerNode integerNode in listIntegerLiterals.nodes) {
                            sb.Append($", {integerNode.value.ToString()}");
                        }
                    }
                    numbersString = sb.ToString();
                } else {
                    // no any literal
                    // numersString stay null
                }

            }

            return complete;
        }


        protected class NextNumSymLiteral : SequenceOf {

            private static Tag tagNumSymLit = new Tag();

            public NextNumSymLiteral() 
                : base(new List<AParseExam>() {
                        new Str(","),
                        new Spaces_NoOrMore(),
                        new NumSymLiteral(){tag=tagNumSymLit},
                        new Spaces_NoOrMore(),
                })
            {
                
            }

            protected override ANode ConstructNode() {
                var parseNumSym = GetBy(tagNumSymLit) as NumSymLiteral;
                return new NextIntegerNode() { value = parseNumSym.value };
            }
        }

        protected class NextIntegerNode : ANode {
            public int value;

            #region ANode implementation is NOT USED
            public override void CheckOnValid() {
                throw new WrongUsageException();
            }

            public override void CreateVarsForConsts() {
                throw new WrongUsageException();
            }

            public override void TranslateToAsm(StringBuilder sb) {
                throw new WrongUsageException();
            }

            public override void TranslateToBc(StringBuilder sb, InterpretatorBuilder ib) {
                throw new WrongUsageException();
            }
            #endregion
        }
    }
}
