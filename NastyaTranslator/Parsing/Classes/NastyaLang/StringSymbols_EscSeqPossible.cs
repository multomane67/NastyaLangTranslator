﻿using NastyaTranslator.Translation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator.Parsing.Classes.NastyaLang {

    class StringSymbols_EscSeqPossible : AParseExam {
        private const char wrongTerminatingSymbol = '\n';
        private const char escapeSequencePrefix = '\\';
        private const char hexCodePrefix = 'x';

        private char normalTerminatingSymbol;

        public StringRepresentation symbols = null;
        private bool normalTerminating = false;
        private bool escapeSequenceBegined = false;
        private HexCode_OneByte hexCodeParser = null;

        protected static readonly string allEscapeNames = EscapeSequences.Symbols2Codes.ToStrList(customToString: (kvp) => $"'{kvp.Key}'");

        public StringSymbols_EscSeqPossible(char terminatingSymbol) : base() 
        {
            normalTerminatingSymbol = terminatingSymbol;
        }

        public override void Reset() {
            Status = Status.Idle;
            symbols = new StringRepresentation();
            normalTerminating = false;
            escapeSequenceBegined = false;
            hexCodeParser = null;
        }

        public override bool CheckOnValid(char sym) {
            bool wrongTerminating = false;

            if (hexCodeParser != null) {
                bool wasTaken = hexCodeParser.PutSymbol(sym);
                var status = hexCodeParser.Status;
                if (status == Status.Complete) {
                    symbols.AddCode(hexCodeParser.value);
                    hexCodeParser = null;
                    if (!wasTaken)
                        return CheckOnValid(sym);
                } else if (status == Status.Error) {
                    wrongTerminating = true;
                }

                return !wrongTerminating;
            } else if (escapeSequenceBegined) {
                bool isConst = EscapeSequences.Symbols2Codes.ContainsKey(sym);
                wrongTerminating = !isConst;
                escapeSequenceBegined = !isConst;

                if (!wrongTerminating) {
                    if (sym == hexCodePrefix) {
                        hexCodeParser = new HexCode_OneByte();
                        hexCodeParser.Init(this.parseRoot);
                        hexCodeParser.Reset();
                    } else {
                        symbols.AddCode(EscapeSequences.Symbols2Codes[sym]);
                    }
                }

                return !wrongTerminating;
            } else {
                wrongTerminating = sym == wrongTerminatingSymbol;
                normalTerminating = sym == normalTerminatingSymbol;
                escapeSequenceBegined = sym == escapeSequencePrefix;

                if (!wrongTerminating && !normalTerminating && !escapeSequenceBegined) {
                    symbols.AddChar(sym);
                }

                return !wrongTerminating;
            }
        }

        public override bool PutSymbol(char sym) {
            bool isPutted = base.PutSymbol(sym);

            if (Status == Status.Error) {
                if (hexCodeParser != null)
                    errorMessage = hexCodeParser.ErrorMessage;
                else if (escapeSequenceBegined)
                    errorMessage = $"Expected true escape const ({allEscapeNames}).";
                else
                    errorMessage = $"Expected '{normalTerminatingSymbol}' but finded end of line!";
            }

            return isPutted;
        }

        public override bool CheckOnComplete_afterValid(out bool takeLastSymbol) {
            takeLastSymbol = false;
            return normalTerminating;
        }

        protected override ANode ConstructNode() {
            throw new WrongUsageException();
        }

        private class HexCode_OneByte : AFillString {

            const int minSymbols = 1;
            const int maxSymbols = 2;

            public int value;
            public int len = 0;

            public override void Reset() {
                base.Reset();
                value = 0;
                len = 0;
            }

            protected override bool IsMatchSymbol(char sym, bool isFirst) {
                bool isMatch = sym.InDiapasone('0', '9')
                            || sym.InDiapasone('a', 'f')
                            || sym.InDiapasone('A', 'F');

                isMatch = isMatch && len < maxSymbols;

                if (isMatch)
                    len++;

                return isMatch;
            }

            protected override bool PostProcess(StringBuilder sb, out string errorMessage) {
                int count = sb.Length;
                if (count < minSymbols) {
                    errorMessage = "HexCode don't have any hex symbols.";
                    return false;
                } else if (count > maxSymbols) {
                    errorMessage = "HexCode have too many hex symbols.";
                    return false;
                } else {
                    string strForParse = $"0x{sb.ToString()}";
                    try {
                        value = Convert.ToInt32(strForParse, 16);
                    } catch (Exception e) {
                        errorMessage = e.Message;
                        return false;
                    } 
                    errorMessage = null;
                    return true;
                }
            }
            protected override string GetStartupMessage(char sym) {
                return "Expected HexCode for escape sequence \\xNN.";
            }
        }
    }
}
