﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NastyaTranslator.Translation;

namespace NastyaTranslator.Parsing.Classes.NastyaLang {
    class VariableCalc : AnyOf {
        public VariableCalc(string ending = ";") 
            : base(new List<AParseExam>() {
                new VariantRAB(ending),
                new VariantBinary(ending),
                new VariantUnary(ending),
            }) 
        {
        }

        private class VariantRAB : SequenceOf {

            private static Tag tagResultVar = new Tag();
            private static Tag tagArithExpr = new Tag();
            public VariantRAB(string ending)
                : base(new List<AParseExam>() {
                    new Name(){tag=tagResultVar},
                    new Spaces_NoOrMore(),
                    new Str("="),
                    new Spaces_NoOrMore(),
                    new ArithmeticExpression(){tag=tagArithExpr},
                    new Spaces_NoOrMore(),
                    new Str(ending),
                }) {
                nameForError = "variable calculation";
            }

            protected override ANode ConstructNode() {
                var resultVarName = GetBy(tagResultVar) as Name;
                var arithmeticExpression = GetBy(tagArithExpr) as ArithmeticExpression;

                return new OperatorCalc() {
                    rVar = resultVarName.symbols,
                    aVar = arithmeticExpression.aVar,
                    aConst = arithmeticExpression.aConst,
                    operation = arithmeticExpression.expOperator,
                    bVar = arithmeticExpression.bVar,
                    bConst = arithmeticExpression.bConst,
                };
            }
        }

        private class VariantBinary : SequenceOf {
            private static Tag tagOperandA = new Tag();
            private static Tag tagArithOp = new Tag();
            private static Tag tagOperandB = new Tag();

            public VariantBinary(string ending)
                : base(new List<AParseExam>() {
                    new Name(){tag=tagOperandA},
                    new Spaces_NoOrMore(),
                    new ArithmeticOperatorBinary(){tag=tagArithOp},
                    new Spaces_NoOrMore(),
                    new VarNameOrLiteral(){tag=tagOperandB},
                    new Spaces_NoOrMore(),
                    new Str(ending),
                }) {
                nameForError = "variable calculation binary";
            }

            protected override ANode ConstructNode() {
                var operandA = GetBy(tagOperandA) as Name;
                var arithOp = GetBy(tagArithOp) as ArithmeticOperatorBinary;
                var operandB = GetBy(tagOperandB) as VarNameOrLiteral;

                string processedOp = arithOp.symbols.Replace("=", "");

                return new OperatorCalc() {
                    rVar = operandA.symbols,
                    aVar = operandA.symbols,
                    operation = processedOp,
                    bVar = operandB.VarName,
                    bConst = operandB.ConstText_CastTypePossible,
                };
            }
        }

        private class VariantUnary : SequenceOf {
            private static Tag tagVarName = new Tag();
            private static Tag tagArithOp = new Tag();

            public VariantUnary(string ending)
                : base(new List<AParseExam>() {
                    new Name(){tag=tagVarName},
                    new Spaces_NoOrMore(),
                    new ArithmeticOperatorUnary(){tag=tagArithOp},
                    new Spaces_NoOrMore(),
                    new Str(ending),
                }) {
                nameForError = "variable calculation unary";
            }

            protected override ANode ConstructNode() {
                var varName = GetBy(tagVarName) as Name;
                var arithOperator = GetBy(tagArithOp) as ArithmeticOperatorUnary;

                string processedOp = arithOperator.symbols;
                processedOp = processedOp == "++" ? "+" : processedOp;
                processedOp = processedOp == "--" ? "-" : processedOp;

                return new OperatorCalc() {
                    rVar = varName.symbols,
                    aVar = varName.symbols,
                    operation = processedOp,
                    bConst = "1",
                };
            }
        }

    }
}
