﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NastyaTranslator.Translation;

namespace NastyaTranslator.Parsing.Classes.NastyaLang
{
    class NameSpace : SequenceOf
    {
        private static Tag tagName = new Tag();
        private static Tag tagMaxVarsAttr = new Tag();
        private static Tag tagVarsAndFuncs = new Tag();
        public NameSpace()
            : base(new List<AParseExam>() {
                new Possible(
                    new SequenceOf(new List<AParseExam>(){
                        new MaxNumberOfVars(){tag=tagMaxVarsAttr},
                        new Spaces_NoOrMore(),
                    })
                ),
                new Str("namespace"),
                new Spaces_OneOrMore(),
                new Name(){tag=tagName},
                new Spaces_NoOrMore(),
                new Str("{"),
                new Many_NoOrMore(
                    new AnyOf(new List<AParseExam>() {
                        new Spaces_OneOrMore(), // in AnyOf you can use _OneOrMore variant only, because it can wrongy terminate this AnyOf
                        new VariableDefinition(),
                        new ArrayDefinition(),
                        new FunctionDefinition(),
                    })
                ){tag=tagVarsAndFuncs},
                new Str("}"),
            })
        {
            nameForError = "name space";
        }

        public override void Reset() {
            base.Reset();
        }

        public override bool PutSymbol(char sym)
        {
            bool isPutted = base.PutSymbol(sym);

            return isPutted;
        }

        public override bool CheckOnValid(char sym) {
            return base.CheckOnValid(sym);
        }

        public override bool CheckOnComplete_afterValid(out bool takeLastSymbol) {
            return base.CheckOnComplete_afterValid(out takeLastSymbol);
        }

        protected override ANode ConstructNode()
        {
            var parseName = GetBy(tagName) as Name;
            var parseMaxVars = GetBy(tagMaxVarsAttr) as MaxNumberOfVars;
            var parseVarsAndFuncs = GetBy(tagVarsAndFuncs) as Many_NoOrMore;

            var lstVarsAndFuncs = (ListOfNodes)parseVarsAndFuncs.GetNode();


            NameSpaceNode nameSpaceNode = new NameSpaceNode() {
                name = parseName.symbols,
                
                attributes =
                    parseMaxVars.Status == Status.Complete
                        ? new List<AttributeNode>() { (AttributeNode)parseMaxVars.GetNode() }
                        : new List<AttributeNode>(),

                variables = (from node in lstVarsAndFuncs.nodes
                             where node is Variable
                             select (Variable)node).ToList(),

                functions = (from node in lstVarsAndFuncs.nodes
                             where node is FuncNode
                             select (FuncNode)node).ToList(),
            };


            return nameSpaceNode;
        }
    }
}
