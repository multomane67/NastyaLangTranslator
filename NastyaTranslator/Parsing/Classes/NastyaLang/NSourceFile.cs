﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NastyaTranslator.Translation;

namespace NastyaTranslator.Parsing.Classes.NastyaLang
{
    class NSourceFile : AParseRoot {

        private static Tag tagNameSpaces = new Tag();

        public NSourceFile(string locationFileName) 
            : base(locationFileName, new Many_NoOrMore(
                new AnyOf(new List<AParseExam>() {
                    new NameSpace(),
                    new Spaces_OneOrMore(),
                })
            ){tag=tagNameSpaces})
        {
        }


        protected override ANode ConstructNode()
        {
            var listOfNameSpaces = (ListOfNodes)base.ConstructNode();

            return listOfNameSpaces;
        }

    }
}
