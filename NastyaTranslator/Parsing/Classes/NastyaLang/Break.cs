﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NastyaTranslator.Translation;

namespace NastyaTranslator.Parsing.Classes.NastyaLang {
    class Break : SequenceOf {
        public Break()
            : base(new List<AParseExam>() {
                new Str("break"),
                new Spaces_NoOrMore(),
                new Str(";"),
            })
        {
            nameForError = "cycle break";
        }

        protected override ANode ConstructNode() {
            return new OperatorBreak();
        }
    }
}
