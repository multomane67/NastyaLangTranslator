﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NastyaTranslator.Translation;

namespace NastyaTranslator.Parsing.Classes.NastyaLang {
    class CommentaryFilter : AParseExam {

        private const int BUFFER_SIZE = 2;
        private const int BUFFER_LAST_INDEX = BUFFER_SIZE - 1;
        private char[] buffer = new char[BUFFER_SIZE];
        private int bufferFilled = 0;
        private Commentary openedCommentary = null;

        private readonly List<Commentary> commentaries = new List<Commentary>() {
            new Commentary("//", "\n"),
            new Commentary("/*", "*/"),
        };

        public AParseExam parser = null;

        public CommentaryFilter(AParseExam parser) {
            this.parser = parser;
        }

        public override void Init(AParseRoot parseRoot) {
            throw new WrongUsageException();
        }

        public override void Reset() {
            parser.Reset();

            FillBufferBySymbol('\0');
            bufferFilled = 0;

            openedCommentary = null;
        }

        public override bool PutSymbol(char sym) {

            // remove one at first and shift by one
            for (int i = 0; i < BUFFER_LAST_INDEX; i++) {
                buffer[i] = buffer[i + 1];
            }

            // put next by last position
            buffer[BUFFER_LAST_INDEX] = sym;

            if (bufferFilled < BUFFER_SIZE) {
                bufferFilled++;
            }

            if (bufferFilled < BUFFER_SIZE) {
                return true;
            } else {
                // check buffer on commentary

                if (openedCommentary == null) {
                    // find match for open
                    foreach (var commentary in commentaries) {
                        if (CompareBufferFromLast(commentary.beginig)) {
                            openedCommentary = commentary;
                            break;
                        }
                    }
                } else {
                    // find match for close - check openened only
                    if (CompareBufferFromLast(openedCommentary.ending)) {
                        openedCommentary = null;
                        ExchangeBufferedToSpaces();
                    }
                }

                char firstBufferSym = buffer[0];

                if (openedCommentary != null) {
                    if (firstBufferSym != '\n') {
                        firstBufferSym = ' ';
                    }
                }

                bool isPutted = parser.PutSymbol(firstBufferSym);
                Status = parser.Status;
                if (Status == Status.Error)
                    errorMessage = parser.ErrorMessage;
                return isPutted;
            }

        }

        public override bool CheckOnComplete_afterValid(out bool takeLastSymbol) {
            throw new WrongUsageException();
        }

        public override bool CheckOnValid(char sym) {
            throw new WrongUsageException();
        }

        protected override ANode ConstructNode() {
            return parser.GetNode();
        }


        private bool CompareBufferFromLast(string str) {
            bool isMatch = true;

            int strLastIndex = str.Length - 1;
            int compLen = Math.Min(BUFFER_SIZE, str.Length);

            for (int i = 0; i < compLen; i++) {
                if (buffer[BUFFER_LAST_INDEX - i] != str[strLastIndex - i]) {
                    isMatch = false;
                    break;
                }
            }

            return isMatch;
        }

        private void FillBufferBySymbol(char symbol) {
            for (int i = 0; i < BUFFER_SIZE; i++) {
                buffer[i] = symbol;
            }
        }

        private void ExchangeBufferedToSpaces() {
            for (int i = 0; i < BUFFER_SIZE; i++) {
                if (buffer[i] != '\n') {
                    buffer[i] = ' ';
                }
            }
        }

        private class Commentary {
            public string beginig = null;
            public string ending = null;
            public Commentary(string beginig, string ending) {
                this.beginig = beginig;
                this.ending = ending;
            }
        }
    }
}
