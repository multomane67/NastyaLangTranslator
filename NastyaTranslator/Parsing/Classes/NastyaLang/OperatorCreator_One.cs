﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator.Parsing.Classes.NastyaLang {
    class OperatorCreator_One : ParseCreator {
        public OperatorCreator_One(string nameForError) 
            : base (() => new AnyOperation())
        {
            this.nameForError = nameForError;
        }
    }
}
