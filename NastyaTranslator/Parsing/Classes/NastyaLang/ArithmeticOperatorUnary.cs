﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator.Parsing.Classes.NastyaLang {
    class ArithmeticOperatorUnary : AExpressionOperator {

        private static readonly HashSet<char> arithmeticChars = new HashSet<char>() {
            '+', '-', '<', '>'
        };

        private static readonly HashSet<string> arithmeticOperators = new HashSet<string>() {
            "++", "--", "<<", ">>",
        };

        private static readonly string strAllArithmeticChars =
            arithmeticChars.ToStrList(customToString: (chr) => $"'{chr}'");

        private static readonly string strAllArithmeticOperators =
            arithmeticOperators.ToStrList(customToString: (str) => $"\"{str}\"");

        public ArithmeticOperatorUnary() 
            : base(
                  "arithmetic operator",
                  arithmeticChars,  arithmeticOperators,
                  strAllArithmeticChars, strAllArithmeticOperators
            )
        {

        }
    }
}
