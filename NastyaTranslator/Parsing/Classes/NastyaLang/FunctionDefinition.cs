﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NastyaTranslator.Translation;

namespace NastyaTranslator.Parsing.Classes.NastyaLang
{
    class FunctionDefinition : SequenceOf
    {
        private static Tag tagName = new Tag();
        private static Tag tagOperators = new Tag();

        public FunctionDefinition()
            : base(new List<AParseExam>() {
                new Str("void"),
                new Spaces_OneOrMore(),
                new Name(){tag=tagName},
                new Spaces_NoOrMore(),
                new Str("("),
                new Spaces_NoOrMore(),
                new Str(")"),
                new Spaces_NoOrMore(),
                new Operator_Compound("inside function"){tag=tagOperators},
            })
        {
            nameForError = "function definition";
        }

        public override void Reset() {
            base.Reset();
        }

        public override bool PutSymbol(char sym) {
            return base.PutSymbol(sym);
        }

        public override bool CheckOnValid(char sym) {
            return base.CheckOnValid(sym);
        }

        public override bool CheckOnComplete_afterValid(out bool takeLastSymbol) {
            return base.CheckOnComplete_afterValid(out takeLastSymbol);
        }

        protected override ANode ConstructNode() {
            var funcNode = new FuncNode();

            funcNode.name = (GetBy(tagName) as Name).symbols;

            List<AOperatorNode> operators = (GetBy(tagOperators).GetNode() as ListOfNodes).nodes.ToOperatorsNodes();

            funcNode.operators = operators;

            return funcNode;
        }
    }
}
