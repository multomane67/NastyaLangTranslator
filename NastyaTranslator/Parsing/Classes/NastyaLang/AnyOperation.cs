﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator.Parsing.Classes.NastyaLang {
    class AnyOperation : AnyOf {

        public AnyOperation() 
            : base( new List<AParseExam>() {
                new VariableSet(),
                new VariableCalc(),
                new FunctionCall(),
                new Asm(),
                new If(),
                new For(),
                new While(),
                new DoWhile(),
                new Break(),
                new Continue(),
                new Ptr(),
            })
        {

        }
    }
}
