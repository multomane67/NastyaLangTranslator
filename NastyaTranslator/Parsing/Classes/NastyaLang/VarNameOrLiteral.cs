﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator.Parsing.Classes.NastyaLang {
    class VarNameOrLiteral : AnyOf {

        public string VarName =>
            Status == Status.Complete
                ? (GetCompleted() as Name)?.symbols
                : null;

        public string ConstText_CastTypePossible =>
            Status == Status.Complete
                ? (GetCompleted() as NumSymLiteral_CastTypePossible)?.symbols
                : null;

        public VarNameOrLiteral() 
            : base(new List<AParseExam>() {
                    new Name(),
                    new NumSymLiteral_CastTypePossible(),
            })
        {
        }
    }
}
