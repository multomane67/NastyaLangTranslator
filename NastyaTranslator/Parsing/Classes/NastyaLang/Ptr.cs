﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NastyaTranslator.Translation;

namespace NastyaTranslator.Parsing.Classes.NastyaLang {
    class Ptr : AnyOf {
        public Ptr()
            : base(new List<AParseExam>() {
                new PutValByPointer(),
                new GetValByPointer(),
                new PutValIntoArrElement(),
                new GetValFromArrElement(),
                new GetAddrOfVar(),
            })
        {
            nameForError = "pointer operation";
        }

        #region NESTED CLASSES FOR VARIANTS

        // *ptrName = varName/literal
        protected class PutValByPointer : SequenceOf {
            private static Tag tagPtrName = new Tag();
            private static Tag tagVarNameOrLit = new Tag();

            public PutValByPointer()
                : base(new List<AParseExam>() {
                    new Str("*"),
                    new Spaces_NoOrMore(),
                    new Name(){tag=tagPtrName}, 
                    new Spaces_NoOrMore(),
                    new Str("="),
                    new Spaces_NoOrMore(),
                    new VarNameOrLiteral(){tag=tagVarNameOrLit},
                    new Spaces_NoOrMore(),
                    new Str(";"),
                }) 
            {
                nameForError = "put value by pointer";
            }

            protected override ANode ConstructNode() {
                var parsePtrName = GetBy(tagPtrName) as Name;
                var parseVarNameOrLiteral = GetBy(tagVarNameOrLit) as VarNameOrLiteral;

                var opPtr = new OperatorPtr() {
                    aUnPtr = parsePtrName.symbols,

                    bVar = parseVarNameOrLiteral.VarName,
                    bConst = parseVarNameOrLiteral.ConstText_CastTypePossible,
                };

                return opPtr;
            }
        }

        // varName = *ptrName
        protected class GetValByPointer : SequenceOf {
            private static Tag tagVarName = new Tag();
            private static Tag tagPtrName = new Tag();

            public GetValByPointer()
                : base(new List<AParseExam>() {
                    new Name(){tag=tagVarName},
                    new Spaces_NoOrMore(),
                    new Str("="),
                    new Spaces_NoOrMore(),
                    new Str("*"),
                    new Spaces_NoOrMore(),
                    new Name(){tag=tagPtrName},
                    new Spaces_NoOrMore(),
                    new Str(";"),
                })
            {
                nameForError = "get value by pointer";
            }

            protected override ANode ConstructNode() {
                var parseVarName = GetBy(tagVarName) as Name;
                var parsePtrName = GetBy(tagPtrName) as Name;

                var opPtr = new OperatorPtr() {
                    aVar = parseVarName.symbols,

                    bUnPtr = parsePtrName.symbols,
                };

                return opPtr;
            }
        }

        // ptrName[varName/literal] = varName/literal
        protected class PutValIntoArrElement : SequenceOf {
            private static Tag tagPtrName = new Tag();
            private static Tag tagIndexVarOrConst = new Tag();
            private static Tag tagVarOrConst = new Tag();

            public PutValIntoArrElement()
                : base(new List<AParseExam>() {
                    new Name(){tag=tagPtrName},
                    new Spaces_NoOrMore(),
                    new Str("["),
                    new Spaces_NoOrMore(),
                    new VarNameOrLiteral(){tag=tagIndexVarOrConst},
                    new Spaces_NoOrMore(),
                    new Str("]"),
                    new Spaces_NoOrMore(),
                    new Str("="),
                    new Spaces_NoOrMore(),
                    new VarNameOrLiteral(){tag=tagVarOrConst},
                    new Spaces_NoOrMore(),
                    new Str(";"),
                })
            {
                nameForError = "put value into array element";
            }

            protected override ANode ConstructNode() {
                var parsePtrName = GetBy(tagPtrName) as Name;
                var parseIndexVarOrConst = GetBy(tagIndexVarOrConst) as VarNameOrLiteral;
                var parseVarOrConst = GetBy(tagVarOrConst) as VarNameOrLiteral;

                var opPtr = new OperatorPtr() {
                    aUnPtr = parsePtrName.symbols,
                    aIndexVar = parseIndexVarOrConst.VarName,
                    aIndexConst = parseIndexVarOrConst.ConstText_CastTypePossible,

                    bVar = parseVarOrConst.VarName,
                    bConst = parseVarOrConst.ConstText_CastTypePossible,
                };

                return opPtr;
            }
        }

        // varName = ptrName[varName/literal]
        protected class GetValFromArrElement : SequenceOf {
            private static Tag tagVarName = new Tag();
            private static Tag tagPtrName = new Tag();
            private static Tag tagIndexVarOrConst = new Tag();

            public GetValFromArrElement()
                : base(new List<AParseExam>() {
                    new Name(){tag=tagVarName},
                    new Spaces_NoOrMore(),
                    new Str("="),
                    new Spaces_NoOrMore(),
                    new Name(){tag=tagPtrName},
                    new Spaces_NoOrMore(),
                    new Str("["),
                    new Spaces_NoOrMore(),
                    new VarNameOrLiteral(){tag=tagIndexVarOrConst},
                    new Spaces_NoOrMore(),
                    new Str("]"),
                    new Spaces_NoOrMore(),
                    new Str(";"),
                })
            {
                nameForError = "get value from array element";
            }

            protected override ANode ConstructNode() {
                var parseVarName = GetBy(tagVarName) as Name;
                var parsePtrName = GetBy(tagPtrName) as Name;
                var parseIndexVarOrConst = GetBy(tagIndexVarOrConst) as VarNameOrLiteral;

                var opPtr = new OperatorPtr() {
                    aVar = parseVarName.symbols,

                    bUnPtr = parsePtrName.symbols,
                    bIndexVar = parseIndexVarOrConst.VarName,
                    bIndexConst = parseIndexVarOrConst.ConstText_CastTypePossible,
                };

                return opPtr;
            }
        }

        // varName = &varName
        protected class GetAddrOfVar : SequenceOf {
            private static Tag tagPtrName = new Tag();
            private static Tag tagVarName = new Tag();

            public GetAddrOfVar() 
                : base(new List<AParseExam>() {
                    new Name(){tag=tagPtrName},
                    new Spaces_NoOrMore(),
                    new Str("="),
                    new Spaces_NoOrMore(),
                    new Str("&"),
                    new Spaces_NoOrMore(),
                    new Name(){tag=tagVarName},
                    new Spaces_NoOrMore(),
                    new Str(";"),
                }) 
            {
                nameForError = "get addr of var";
            }

            protected override ANode ConstructNode() {
                var parsePtrName = GetBy(tagPtrName) as Name;
                var parseVarName = GetBy(tagVarName) as Name;

                var opPtr = new OperatorPtr() {
                    aPtr = parsePtrName.symbols,
                    bAddrOf = parseVarName.symbols,
                };

                return opPtr;
            }
        }

        #endregion
    }
}
