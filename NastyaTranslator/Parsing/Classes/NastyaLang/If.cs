﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NastyaTranslator.Translation;

namespace NastyaTranslator.Parsing.Classes.NastyaLang {
    class If : SequenceOf {

        private static Tag tagComparison = new Tag();
        private static Tag tagOperators = new Tag();
        private static Tag tagElseSeq = new Tag();
        private static Tag tagElseOperators = new Tag();

        public If()
            : base(new List<AParseExam> {
                new Str("if"),
                new Spaces_NoOrMore(),
                new Str("("),
                new Spaces_NoOrMore(),
                new ComparisonExpression(){tag=tagComparison},
                new Spaces_NoOrMore(),
                new Str(")"),
                new Spaces_NoOrMore(),
                new Operator_OneOrCompound("inside if"){tag=tagOperators},
                new Spaces_NoOrMore(),
                new Possible(
                    new SequenceOf(new List<AParseExam>(){
                        new Str("else"),
                        new Spaces_NoOrMore(),
                        new Operator_OneOrCompound("inside else"){tag=tagElseOperators},
                    }){tag=tagElseSeq}
                )
            }) 
        {
            nameForError = "if";
        }

        public override void Reset() {
            base.Reset();
        }

        public override bool PutSymbol(char sym) {
            return base.PutSymbol(sym);
        }

        public override bool CheckOnValid(char sym) {
            return base.CheckOnValid(sym);
        }

        public override bool CheckOnComplete_afterValid(out bool takeLastSymbol) {
            return base.CheckOnComplete_afterValid(out takeLastSymbol);
        }

        protected override ANode ConstructNode() {

            var comparisonExpression = GetBy(tagComparison) as ComparisonExpression;

            List<AOperatorNode> operators = (GetBy(tagOperators).GetNode() as ListOfNodes).nodes.ToOperatorsNodes();

            var operatorIf = new OperatorIf() {
                aVar = comparisonExpression.aVar,
                aConst = comparisonExpression.aConst,
                comparisonOperator = comparisonExpression.expOperator,
                bVar = comparisonExpression.bVar,
                bConst = comparisonExpression.bConst,
                ifOperators = operators,
            };

            var elseSeq = GetBy(tagElseSeq);

            if (elseSeq.Status == Status.Complete) {
                var elseOpCreator = GetBy(tagElseOperators);

                var elseInsideOperators = elseOpCreator.Status == Status.Complete ? (elseOpCreator.GetNode() as ListOfNodes).nodes.ToOperatorsNodes() : null;

                if (elseInsideOperators != null) {
                    operatorIf.elseOperators = elseInsideOperators;
                } else {
                    throw new Exception("Wrong behaviour!");
                }
            }

            return operatorIf;
        }
    }
}
