﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NastyaTranslator.Translation;

namespace NastyaTranslator.Parsing.Classes.NastyaLang
{
    class VariableDefinition : SequenceOf
    {
        private static Tag tagType = new Tag();
        private static Tag tagVarName = new Tag();
        private static Tag tagDefValLit = new Tag();

        public VariableDefinition()
            : base(new List<AParseExam>() {
                new AnyType(){tag=tagType},
                new Spaces_OneOrMore(),
                new Name(){tag=tagVarName},
                new Spaces_NoOrMore(),
                new Possible( 
                    new SequenceOf( new List<AParseExam>(){
                        new Str("="),
                        new Spaces_NoOrMore(),
                        new NumSymLiteral(){tag=tagDefValLit},
                        new Spaces_NoOrMore(),
                    })
                ),
                new Str(";"),
            })
        {
            nameForError = "variable definition";
        }

        public override void Reset() {
            base.Reset();
        }

        public override bool PutSymbol(char sym) {
            return base.PutSymbol(sym);
        }

        public override bool CheckOnValid(char sym) {
            return base.CheckOnValid(sym);
        }

        public override bool CheckOnComplete_afterValid(out bool takeLastSymbol) {
            return base.CheckOnComplete_afterValid(out takeLastSymbol);
        }

        protected override ANode ConstructNode()
        {
            var parseType = GetBy(tagType) as AnyType;
            var parseVariableName = GetBy(tagVarName) as Name;
            var parseDefaultValueLit = GetBy(tagDefValLit) as NumSymLiteral;

            var newVariable = new Variable() {
                type = parseType.GetTypeSymbols(),
                name = parseVariableName.symbols,
                defValue =
                    parseDefaultValueLit.Status == Status.Complete
                        ? parseDefaultValueLit.value.ToString()
                        : null
            };

            return newVariable;
        }
    }
}
