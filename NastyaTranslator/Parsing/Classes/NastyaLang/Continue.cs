﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NastyaTranslator.Translation;

namespace NastyaTranslator.Parsing.Classes.NastyaLang {

    class Continue : SequenceOf {
        public Continue()
            : base(new List<AParseExam>() {
                new Str("continue"),
                new Spaces_NoOrMore(),
                new Str(";"),
            }) {
            nameForError = "cycle continue";
        }

        protected override ANode ConstructNode() {
            return new OperatorContinue();
        }
    }
}
