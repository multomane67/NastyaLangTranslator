﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator.Parsing.Classes.NastyaLang {
    class SymbolLiteral : SequenceOf {
        public int value = -1;

        private static Tag tagSymbols = new Tag();

        public SymbolLiteral()
            : base(new List<AParseExam>() {
                new Str("'"),
                new StringSymbols_EscSeqPossible('\''){tag=tagSymbols},
                new Str("'"),
            }) 
        {
            
        }

        public override void Reset() {
            base.Reset();
            value = -1;
        }

        public override bool PutSymbol(char sym) {
            bool isPutted = base.PutSymbol(sym);

            if (Status == Status.Complete) {
                var parseSymbols = GetBy(tagSymbols) as StringSymbols_EscSeqPossible;

                if (parseSymbols.symbols.Length <= 0) {
                    errorMessage = "No symbols in symbol literal.";
                } else if (parseSymbols.symbols.Length > 1) { 
                    errorMessage = "Too many symbols in symbol literal.";
                } else {
                    errorMessage = null;
                    List<int> codes = parseSymbols.symbols.BuildCodes();
                    value = codes[0];
                }

                if (errorMessage != null)
                    Status = Status.Error;
            }

            return isPutted;
        }

    }
}
