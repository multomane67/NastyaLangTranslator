﻿using NastyaTranslator.Translation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator.Parsing.Classes.NastyaLang {
    class Asm : SequenceOf {

        private static Tag tagAsmCode = new Tag();

        public Asm()
            : base(new List<AParseExam>() {
                new Str("asm"),
                new Spaces_NoOrMore(),
                new Str("{"),
                new Possible(
                    new AsmCode(){tag=tagAsmCode}
                ),
                new Str("}"),
            }) 
        {
            nameForError = "asm insertion";
        }

        protected override ANode ConstructNode() {

            var parseAsmCode = GetBy(tagAsmCode) as AsmCode;

            return new OperatorAsm() {
                verbose = true,
                asmText = parseAsmCode.Status == Status.Complete ? parseAsmCode.asmText : ""
            };
        }


        private class AsmCode : AFillString {
            private const char terminatingSymbol_willBeExcluded = '}';

            public string asmText = null;

            public override void Reset() {
                asmText = null;
                base.Reset();
            }

            protected override bool IsMatchSymbol(char sym, bool isFirst) {
                return sym != terminatingSymbol_willBeExcluded;
            }

            protected override bool PostProcess(StringBuilder sb, out string errorMessage) {
                asmText = sb.ToString();

                errorMessage = null;
                return true;
            }

            protected override string GetStartupMessage(char sym) {
                return $"Expected assembler code (can't be empty).";
            }
        }
    }
}
