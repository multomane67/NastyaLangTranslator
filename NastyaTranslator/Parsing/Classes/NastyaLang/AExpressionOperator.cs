﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator.Parsing.Classes.NastyaLang {
    abstract class AExpressionOperator : AFillString {
        public string symbols = null;

        private readonly string loggingOperatorName = null;

        private readonly HashSet<char> possibleChars = null;

        private readonly HashSet<string> possibleOperators = null;
        
        private string possibleCharsString = null;

        private string possibleOperatorsString = null;

        public AExpressionOperator(
            string loggingOperatorName,
            HashSet<char> possibleChars, HashSet<string> possibleOperators,
            string possibleCharsString, string possibleOperatorsString
        ) 
        {
            this.loggingOperatorName = loggingOperatorName;
            this.possibleChars = possibleChars;
            this.possibleOperators = possibleOperators;
            this.possibleCharsString = possibleCharsString;
            this.possibleOperatorsString = possibleOperatorsString;
        }

        public override void Reset() {
            symbols = null;
            base.Reset();
        }

        protected override bool IsMatchSymbol(char sym, bool isFirst) {
            return possibleChars.Contains(sym);
        }

        protected override bool PostProcess(StringBuilder sb, out string errorMessage) {
            symbols = sb.ToString();
            if (possibleOperators.Contains(symbols)) {
                errorMessage = null;
                return true;
            } else {

                errorMessage = $"Illegal {loggingOperatorName} '{symbols}',\npossible operators: [{possibleOperatorsString}].";
                return false;
            }
        }

        protected override string GetStartupMessage(char sym) {
            return $"Expected {loggingOperatorName}, finded '{sym}',\npossible chars: [{possibleCharsString}].";
        }
    }
}

