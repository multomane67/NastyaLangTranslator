﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NastyaTranslator.Translation;

namespace NastyaTranslator.Parsing.Classes.NastyaLang {
    class ArrayDefinition : SequenceOf 
    {
        private static Tag tagType = new Tag();
        private static Tag tagVarName = new Tag();
        private static Tag tagArrLen = new Tag();
        private static Tag tagStrLit = new Tag();
        private static Tag tagArrLit = new Tag();

        public ArrayDefinition()
            : base(new List<AParseExam>() {
                new AnyType(){tag=tagType},
                new Spaces_OneOrMore(),
                new Name(){tag=tagVarName},
                new Spaces_NoOrMore(),
                new Str("["),
                new Spaces_NoOrMore(),
                new Possible(
                    new NumSymLiteral(){tag=tagArrLen}
                ),
                new Spaces_NoOrMore(),
                new Str("]"),
                new Spaces_NoOrMore(),
                new Possible(
                    new SequenceOf( new List<AParseExam>(){
                        new Str("="),
                        new Spaces_NoOrMore(),
                        new AnyOf(new List<AParseExam>(){
                            new StringLiteral(){tag=tagStrLit},
                            new ArrayLiteral(){tag=tagArrLit},
                        }),
                        new Spaces_NoOrMore(),
                    })
                ),
                new Str(";"),
            })
        {
            nameForError = "array definition";
        }


        public override void Reset() {
            base.Reset();
        }

        public override bool PutSymbol(char sym) {
            return base.PutSymbol(sym);
        }

        public override bool CheckOnValid(char sym) {
            return base.CheckOnValid(sym);
        }

        public override bool CheckOnComplete_afterValid(out bool takeLastSymbol) {
            return base.CheckOnComplete_afterValid(out takeLastSymbol);
        }

        protected override ANode ConstructNode() {
            var parseType = GetBy(tagType) as AnyType;
            var parseVariableName = GetBy(tagVarName) as Name;
            var parseArrayLength = GetBy(tagArrLen) as NumSymLiteral;
            var parseStringLiteral = GetBy(tagStrLit) as StringLiteral;
            var parseArrayLiteral = GetBy(tagArrLit) as ArrayLiteral;

            var newVariable = new Variable() {
                type = ST.WORD,
                name = parseVariableName.symbols,

                arrLen =
                    parseArrayLength.Status == Status.Complete
                        ? parseArrayLength.value.ToString()
                        : null,

                arrChars =
                    parseStringLiteral.Status == Status.Complete
                        ? parseStringLiteral.symbols
                        : null,

                arrType =
                    parseType.GetTypeSymbols(),

                arrValues =
                    parseArrayLiteral.Status == Status.Complete
                        ? parseArrayLiteral.numbersString
                        : null
            };

            return newVariable;
        }
    }
}
