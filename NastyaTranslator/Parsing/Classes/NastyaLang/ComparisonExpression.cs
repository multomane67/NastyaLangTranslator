﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NastyaTranslator.Translation;

namespace NastyaTranslator.Parsing.Classes.NastyaLang {
    class ComparisonExpression : AExpressionAB {

        public ComparisonExpression()
            : base(new ComparisonOperator()) 
        {

        }

    }
}
