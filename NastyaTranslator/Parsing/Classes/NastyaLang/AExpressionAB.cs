﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NastyaTranslator.Translation;

namespace NastyaTranslator.Parsing.Classes.NastyaLang {
    abstract class AExpressionAB : SequenceOf {

        public string aVar;
        public string aConst;

        public string expOperator;

        public string bVar;
        public string bConst;

        private static Tag tagOperand1 = new Tag();
        private static Tag tagOperand2 = new Tag();

        private AExpressionOperator parseOperator = null;

        public AExpressionAB(AExpressionOperator operatorParser)
            : base(new List<AParseExam> {
                        new VarNameOrLiteral(){tag=tagOperand1},
                        new Spaces_NoOrMore(),
                        operatorParser,
                        new Spaces_NoOrMore(),
                        new VarNameOrLiteral(){tag=tagOperand2},
            }) 
        {
            parseOperator = operatorParser;
        }

        public override void Reset() {
            base.Reset();
            aVar = null;
            aConst = null;
            expOperator = null;
            bVar = null;
            bConst = null;
        }

        public override bool PutSymbol(char sym) {
            bool result = base.PutSymbol(sym);

            if (Status == Status.Complete) {
                VarNameOrLiteral operandParser = null;

                operandParser = GetBy(tagOperand1) as VarNameOrLiteral;
                aVar = operandParser.VarName;
                aConst = operandParser.ConstText_CastTypePossible;

                expOperator = parseOperator.symbols;

                operandParser = GetBy(tagOperand2) as VarNameOrLiteral;
                bVar = operandParser.VarName;
                bConst = operandParser.ConstText_CastTypePossible;
            }
            return result;
        }


        public override bool CheckOnValid(char sym) {
            return base.CheckOnValid(sym);
        }

        public override bool CheckOnComplete_afterValid(out bool takeLastSymbol) {
            return base.CheckOnComplete_afterValid(out takeLastSymbol);
        }

        protected override ANode ConstructNode() {
            throw new WrongUsageException();
        }
    }
}
