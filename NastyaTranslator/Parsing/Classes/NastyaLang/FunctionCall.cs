﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NastyaTranslator.Translation;

namespace NastyaTranslator.Parsing.Classes.NastyaLang {
    class FunctionCall : SequenceOf {
        private static Tag tagName = new Tag();
        
        public FunctionCall()
            : base( new List<AParseExam>() {
                new Name(){tag=tagName},
                new Spaces_NoOrMore(),
                new Str("("),
                new Spaces_NoOrMore(),
                new Str(")"),
                new Spaces_NoOrMore(),
                new Str(";"),
            })
        {
            nameForError = "function call";
        }

        protected override ANode ConstructNode() {
            var parseName = GetBy(tagName) as Name;

            var opCall = new OperatorCall() {
                callFuncName = parseName.symbols,
            };

            return opCall;
        }
    }
}
