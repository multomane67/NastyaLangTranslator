﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NastyaTranslator.Translation;

namespace NastyaTranslator.Parsing.Classes.NastyaLang {
    class VariableSet : SequenceOf {
        private static Tag tagAVar = new Tag();
        private static Tag tagBVarOrConst = new Tag();

        public VariableSet(string ending = ";") 
            : base(new List<AParseExam>() {
                new Name(){tag=tagAVar},
                new Spaces_NoOrMore(),
                new Str("="),
                new Spaces_NoOrMore(),
                new VarNameOrLiteral(){tag=tagBVarOrConst},
                new Spaces_NoOrMore(),
                new Str(ending),
            }) 
        {
            nameForError = "variable assigment";
        }

        protected override ANode ConstructNode() {
            var parseAVarName = GetBy(tagAVar) as Name;
            var parseBVarOrConst = GetBy(tagBVarOrConst) as VarNameOrLiteral;

            string bVarName = parseBVarOrConst.VarName;
            string bConstValue = parseBVarOrConst.ConstText_CastTypePossible;

            var opSet = new OperatorSet() {
                aVar = parseAVarName.symbols,
                bVar = bVarName,
                bConst = bConstValue,
            };

            return opSet;
        }
    }
}
