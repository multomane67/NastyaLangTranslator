﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NastyaTranslator.Translation;

namespace NastyaTranslator.Parsing.Classes.NastyaLang {
    class While : SequenceOf {
        private static Tag tagComparison = new Tag();
        private static Tag tagOperators = new Tag();

        public While()
            : base(new List<AParseExam>() {
                new Str("while"),
                new Spaces_NoOrMore(),
                new Str("("),

                new Spaces_NoOrMore(),
                new ComparisonExpression(){tag=tagComparison},
                new Spaces_NoOrMore(),

                new Str(")"),
                new Spaces_NoOrMore(),
                new Operator_OneOrCompound("inside 'while' cycle"){tag=tagOperators},
            }) 
        {
            nameForError = "cycle 'while'";
        }

        protected override ANode ConstructNode() {
            var parseComparison = GetBy(tagComparison) as ComparisonExpression;
            var parseOperators = GetBy(tagOperators);

            var operatorWhile = new OperatorWhile() {
                check = new OperatorIf() {
                    aVar = parseComparison.aVar,
                    aConst = parseComparison.aConst,
                    comparisonOperator = parseComparison.expOperator,
                    bVar = parseComparison.bVar,
                    bConst = parseComparison.bConst,
                    lif = parseComparison.lif,
                },

                operators = (parseOperators.GetNode() as ListOfNodes).nodes.ToOperatorsNodes(),
            };
            return operatorWhile;
        }
    }
}
