﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NastyaTranslator.Translation;

namespace NastyaTranslator.Parsing.Classes.NastyaLang
{
    class Name : AFillString {
        public string symbols = null;

        protected override bool IsMatchSymbol(char sym, bool isFirst) {
            bool isDigit = sym.InDiapasone('0', '9');
            bool isLetter = sym.InDiapasone('A', 'Z') || sym.InDiapasone('a', 'z');
            bool isOtherUsed = sym == '_';

            return isFirst ?
                isLetter || isOtherUsed :
                isLetter || isOtherUsed || isDigit;
        }

        protected override bool PostProcess(StringBuilder sb, out string errorMessage) {
            symbols = sb.ToString();

            errorMessage = null;
            return true;
        }

        protected override string GetStartupMessage(char sym) {
            return $"Expected name, posible '[a..z, A..Z, 0..9]', finded '{sym}'.";
        }


    }
}
