﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NastyaTranslator.Translation;

namespace NastyaTranslator.Parsing.Classes.NastyaLang {
    class DoWhile : SequenceOf {

        private static Tag tagComparison = new Tag();
        private static Tag tagOperators = new Tag();
        
        public DoWhile()
            : base(new List<AParseExam>() {
                new Str("do"),
                new Spaces_NoOrMore(),
                new Operator_OneOrCompound("inside 'do while' cycle"){tag=tagOperators},
                new Spaces_NoOrMore(),
                new Str("while"),
                new Spaces_NoOrMore(),
                new Str("("),

                new Spaces_NoOrMore(),
                new ComparisonExpression(){tag=tagComparison},
                new Spaces_NoOrMore(),

                new Str(")"),
                new Spaces_NoOrMore(),
                new Str(";"),
            }) 
        {
            nameForError = "cycle 'do while'";
        }

        protected override ANode ConstructNode() {
            var parseOperators = GetBy(tagOperators);
            var parseComparison = GetBy(tagComparison) as ComparisonExpression;

            var operatorDoWhile = new OperatorDoWhile() {
                check = new OperatorIf() {
                    aVar = parseComparison.aVar,
                    aConst = parseComparison.aConst,
                    comparisonOperator = parseComparison.expOperator,
                    bVar = parseComparison.bVar,
                    bConst = parseComparison.bConst,
                    lif = parseComparison.lif,
                },

                operators = (parseOperators.GetNode() as ListOfNodes).nodes.ToOperatorsNodes(),
            };
            return operatorDoWhile;
        }
    }
}
