﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator.Parsing.Classes.NastyaLang {
    class OperatorCreator_NoOrMore : ParseCreator {

        public OperatorCreator_NoOrMore(string nameForError)
            : base(() => new Many_NoOrMore(
                new AnyOf(new List<AParseExam>() {
                    new Spaces_OneOrMore(),
                    new AnyOperation()
                })
            ))
        {
            this.nameForError = nameForError;
        }

    }
}
