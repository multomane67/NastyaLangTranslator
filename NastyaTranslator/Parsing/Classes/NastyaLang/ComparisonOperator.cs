﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator.Parsing.Classes.NastyaLang {
    class ComparisonOperator : AExpressionOperator {
        private static readonly HashSet<char> comparisonChars = new HashSet<char>() {
            '=', '!', '>', '<',
        };

        private static readonly HashSet<string> comparisonOperators = new HashSet<string>() {
            "==", "!=", ">=", "<=", ">", "<"
        };

        private static readonly string allComparisonChars = comparisonChars.ToStrList(customToString: (ch)=>$"'{ch}'");

        private static readonly string allComparisonOperators = comparisonOperators.ToStrList(customToString: (str)=>$"\"{str}\"");


        public ComparisonOperator()  
            : base(
                  "comparison operator",
                  comparisonChars, comparisonOperators,
                  allComparisonChars, allComparisonOperators
            )
        {

        }
    }
}
