﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NastyaTranslator.Translation;

namespace NastyaTranslator.Parsing.Classes.NastyaLang {
    class AnyType : AnyOf {

        public AnyType() 
            : base(new List<AParseExam>() {
                new Str("byte"),
                new Str("word"),
            }) 
        { }

        public string GetTypeSymbols() {
            return (GetCompleted() as Str).GetStrSymbols();
        }

        protected override ANode ConstructNode() {
            throw new WrongUsageException();
        }
    }
}
