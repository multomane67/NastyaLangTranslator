﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NastyaTranslator.Translation;

namespace NastyaTranslator.Parsing.Classes.NastyaLang {
    class For : SequenceOf {
        private static Tag tagVarSet = new Tag();
        private static Tag tagCondition = new Tag();
        private static Tag tagVarCalc = new Tag();
        private static Tag tagOperators = new Tag();

        public For()
            : base(new List<AParseExam>() {
                new Str("for"),
                new Spaces_NoOrMore(),
                new Str("("),
                new Spaces_NoOrMore(),
                new AnyOf(new List<AParseExam>(){
                    new VariableSet(ending:";"){tag=tagVarSet},
                    new Str(";")
                }),
                new Spaces_NoOrMore(),
                new Possible( new ComparisonExpression(){tag=tagCondition} ),
                new Spaces_NoOrMore(),
                new Str(";"),
                new Spaces_NoOrMore(),
                new AnyOf(new List<AParseExam>(){
                    new VariableCalc(ending:")"){tag=tagVarCalc},
                    new Str(")")
                }),
                new Spaces_NoOrMore(),
                new Operator_OneOrCompound("inside 'for' cycle"){tag=tagOperators},
            })
        {
            nameForError = "cycle 'for'";
        }

        protected override ANode ConstructNode() {
            var parseVarSet = GetBy(tagVarSet) as VariableSet;
            var parseCondition = GetBy(tagCondition) as ComparisonExpression;
            var parseVarCalc = GetBy(tagVarCalc) as VariableCalc;
            var parseOperators = GetBy(tagOperators);

            var operatorFor = new OperatorFor() {
                initv = parseVarSet.Status == Status.Complete
                    ? parseVarSet.GetNode() as OperatorSet
                    : new OperatorSet(),

                check = parseCondition.Status == Status.Complete
                    ? new OperatorIf() {
                        aVar = parseCondition.aVar,
                        aConst = parseCondition.aConst,
                        comparisonOperator = parseCondition.expOperator,
                        bVar = parseCondition.bVar,
                        bConst = parseCondition.bConst,
                        lif = parseCondition.lif,
                    }
                    : new OperatorIf(),

                iter = parseVarCalc.Status == Status.Complete
                    ? parseVarCalc.GetNode() as OperatorCalc
                    : new OperatorCalc(),

                operators = (parseOperators.GetNode() as ListOfNodes).nodes.ToOperatorsNodes(),
            };
            return operatorFor;
        }
    }

}
