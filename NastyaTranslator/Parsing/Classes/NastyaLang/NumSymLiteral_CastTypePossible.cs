﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NastyaTranslator.Translation;

namespace NastyaTranslator.Parsing.Classes.NastyaLang {
    class NumSymLiteral_CastTypePossible : SequenceOf {
        public string symbols = null;
        public string typeSymbols = null;
        public int intValue = -1;

        private static Tag tagCastType = new Tag();
        private static Tag tagNumSymLit = new Tag();

        public NumSymLiteral_CastTypePossible()
            : base(new List<AParseExam>() {
                new Possible(
                    new CastType(){tag=tagCastType}
                ),
                new Spaces_NoOrMore(),
                new NumSymLiteral(){tag=tagNumSymLit},
            })
        { }

        public override void Reset() {
            base.Reset();
            intValue = -1;
        }


        public override bool PutSymbol(char sym) {
            bool result = base.PutSymbol(sym);

            if (Status == Status.Complete) {
                var parseCastType = GetBy(tagCastType) as CastType;
                var parseNumSymLit = GetBy(tagNumSymLit) as NumSymLiteral;

                typeSymbols = parseCastType.Status == Status.Complete
                    ? parseCastType.GetTypeSymbols()
                    : null;

                intValue = parseNumSymLit.value;

                if (typeSymbols != null) {
                    symbols = $"({typeSymbols}){intValue.ToString()}";
                } else {
                    symbols = intValue.ToString();
                }
            }

            return result;
        }

        protected override ANode ConstructNode() {
            throw new WrongUsageException();
        }
    }
}
