﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NastyaTranslator.Translation;

namespace NastyaTranslator.Parsing.Classes.NastyaLang {
    class CastType : SequenceOf {

        private static Tag tagAnyType = new Tag();
        public CastType()
            : base(new List<AParseExam>() {
                new Str("("),
                new Spaces_NoOrMore(),
                new AnyType(){tag=tagAnyType},
                new Spaces_NoOrMore(),
                new Str(")"),
            }) 
        { }

        public string GetTypeSymbols() {
            return (GetBy(tagAnyType) as AnyType)?.GetTypeSymbols();
        }

        protected override ANode ConstructNode() {
            throw new WrongUsageException();
        }
    }
}
