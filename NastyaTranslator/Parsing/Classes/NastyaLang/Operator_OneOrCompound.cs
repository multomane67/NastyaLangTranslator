﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NastyaTranslator.Translation;

namespace NastyaTranslator.Parsing.Classes.NastyaLang {
    class Operator_OneOrCompound : AnyOf {
        public Operator_OneOrCompound(string nameForError)
            : base(new List<AParseExam>() {
                new OperatorCreator_One(nameForError),
                new Operator_Compound(nameForError),
            })
        {
        }

        protected override ANode ConstructNode() {
            ANode node = base.ConstructNode();


            ListOfNodes listOfNodes = node as ListOfNodes;

            if (listOfNodes == null && node != null) {
                listOfNodes = new ListOfNodes() { nodes = new List<ANode>() { node } };
            }

            return listOfNodes;
        }
    }
}
