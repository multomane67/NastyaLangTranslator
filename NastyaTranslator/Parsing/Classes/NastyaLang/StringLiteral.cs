﻿using NastyaTranslator.Translation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator.Parsing.Classes.NastyaLang {
    class StringLiteral : SequenceOf {
        public StringRepresentation symbols = null;

        private static Tag tagSymbols = new Tag();
        private static Tag tagNZ = new Tag();

        public StringLiteral()
            : base(new List<AParseExam> {
                new Str("\""),
                new Possible(
                    new StringSymbols_EscSeqPossible('"'){tag=tagSymbols}
                ),
                new Str("\""),
                new Possible(
                    new AnyOf(new List<AParseExam>(){
                        new Str("nz"),
                        new Str("Nz"),
                        new Str("nZ"),
                        new Str("NZ"),
                    }){tag=tagNZ}
                ),
            })
        {
        }

        public override void Reset() {
            base.Reset();
            symbols = null;
        }

        public override bool PutSymbol(char sym) {
            bool isPutted = base.PutSymbol(sym);

            if (Status == Status.Complete) {
                var parseSymbols = GetBy(tagSymbols) as StringSymbols_EscSeqPossible;
                var parseNoZeroAtTheEnd = GetBy(tagNZ) as AnyOf;

                bool noZeroAtTheEnd = parseNoZeroAtTheEnd.Status == Status.Complete;
                symbols = parseSymbols.symbols.Copy();
                if (!noZeroAtTheEnd) {
                    symbols.AddCode(0);
                }
            }

            return isPutted;
        }

        public override bool CheckOnValid(char sym) {
            return base.CheckOnValid(sym);
        }

        public override bool CheckOnComplete_afterValid(out bool takeLastSymbol) {
            return base.CheckOnComplete_afterValid(out takeLastSymbol);
        }

        protected override ANode ConstructNode() {
            throw new WrongUsageException();
        }

    }
}
