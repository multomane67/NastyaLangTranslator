﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NastyaTranslator.Translation;

namespace NastyaTranslator.Parsing.Classes.NastyaLang
{
    class MaxNumberOfVars : SequenceOf
    {
        public int maxNumberOfVars = -1;

        private static Tag tagIntLit = new Tag();

        public MaxNumberOfVars() : base(new List<AParseExam>() {
            new Str("["),
            new Spaces_NoOrMore(),
            new Str("MaxNumberOfVars"),
            new Spaces_NoOrMore(),
            new Str("("),
            new Spaces_NoOrMore(),
            new IntegerLiteral(){tag=tagIntLit},
            new Spaces_NoOrMore(),
            new Str(")"),
            new Spaces_NoOrMore(),
            new Str("]"),
        })
        {

        }

        public override void Reset() {
            base.Reset();
        }

        public override bool PutSymbol(char sym) {
            return base.PutSymbol(sym);
        }

        public override bool CheckOnValid(char sym) {
            return base.CheckOnValid(sym);
        }

        public override bool CheckOnComplete_afterValid(out bool takeLastSymbol)
        {
            bool isComplete = base.CheckOnComplete_afterValid(out takeLastSymbol);

            if (isComplete)
            {
                maxNumberOfVars = (GetBy(tagIntLit) as IntegerLiteral).value;
            }

            return isComplete;
        }

        protected override ANode ConstructNode()
        {
            return new Translation.MaxNumberOfVars() { value = maxNumberOfVars };
        }
    }
}
