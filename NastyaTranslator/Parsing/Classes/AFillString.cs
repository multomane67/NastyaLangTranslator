﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NastyaTranslator.Translation;

namespace NastyaTranslator.Parsing.Classes {
    abstract class AFillString : AParseExam {

        private StringBuilder sb = null;

        private bool isValid = false;

        protected abstract bool IsMatchSymbol(char sym, bool isFirst);
        protected abstract bool PostProcess(StringBuilder sb, out string errorMessage);
        protected abstract string GetStartupMessage(char sym);

        public override void Reset() {
            Status = Status.Idle;
            sb = new StringBuilder();
            isValid = false;
        }

        public override bool PutSymbol(char sym) {
            return base.PutSymbol(sym);
        }

        public override bool CheckOnValid(char sym) {

            bool isFirst = sb.Length == 0;

            isValid = IsMatchSymbol(sym, isFirst);

            if (isValid) {
                sb.Append(sym);
            } else {
                if (isFirst) {
                    // error can be in one case only - when you don't have any character of name
                    errorMessage = GetStartupMessage(sym);
                    return false;
                } else {
                    bool succesPostProcess = PostProcess(sb, out string postProcessErrorMessage);

                    if (!succesPostProcess) {
                        errorMessage = postProcessErrorMessage;
                        return false;
                    }

                    sb = null;
                }
            }

            return true;
        }

        public override bool CheckOnComplete_afterValid(out bool takeLastSymbol) {
            takeLastSymbol = false;
            return !isValid;
        }

        protected override ANode ConstructNode() {
            throw new WrongUsageException();
        }
    }
}
