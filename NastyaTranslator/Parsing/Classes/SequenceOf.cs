﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NastyaTranslator.Translation;

namespace NastyaTranslator.Parsing.Classes {
    class SequenceOf : AParseExam {
        public readonly List<AParseExam> seqList = null;

        private int currIndex = 0;
        private bool takeLastSymbol = false;

        public SequenceOf(List<AParseExam> seqList) {
            this.seqList = seqList;
        }

        public override void Init(AParseRoot parseRoot) {
            base.Init(parseRoot);

            if (seqList.Count == 0)
                throw new Exception("Empty sequence!");

            foreach (AParseExam p in seqList) {
                p.Init(parseRoot);
            }
        }

        public override void Reset() {
            Status = Status.Idle;

            foreach (AParseExam p in seqList) {
                p.Reset();
            }

            currIndex = 0;
            takeLastSymbol = false;
        }

        public override AParseExam GetBy(Tag tag) {
            AParseExam result = base.GetBy(tag);
            if (result == null) {
                foreach (AParseExam pe in seqList) {
                    result = pe.GetBy(tag);
                    if (result != null)
                        break;
                }
            }
                
            return result;
        }

        public override bool PutSymbol(char sym) {
            return base.PutSymbol(sym);
        }

        public override bool CheckOnValid(char sym) {

            bool isPutted = false;
            bool isValid = false;

            do {

                isPutted = seqList[currIndex].PutSymbol(sym);

                Status currStatus = seqList[currIndex].Status;

                isValid = currStatus != Status.Error;

                if (currStatus == Status.Complete) {
                    currIndex++;
                    takeLastSymbol = isPutted;
                } else if (currStatus == Status.Error) {
                    errorMessage = seqList[currIndex].ErrorMessage;
                }

            } while (isValid && !isPutted && currIndex < seqList.Count);
            
            return isValid;
        }

        public override bool CheckOnComplete_afterValid(out bool takeLastSymbol) {
            AParseExam lastInSequence = seqList[seqList.Count-1];

            takeLastSymbol = this.takeLastSymbol;

            return lastInSequence.Status == Status.Complete;
        }

        protected override ANode ConstructNode() {
            throw new WrongUsageException();
        }
    }
}
