﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NastyaTranslator.Translation;

namespace NastyaTranslator.Parsing.Classes.CommandLine {
    class FileNames : Many_NoOrMore {

        private int minimalNumber;

        public List<string> fileNames = null;


        public FileNames(int minimalNumber)
            : base(new AnyOf(new List<AParseExam>() {
                new FileName(),
                new Spaces_OneOrMore(),
            }))
        {
            this.minimalNumber = minimalNumber;
        }

        public override void Reset() {
            base.Reset();
            fileNames = null;
        }

        public override bool PutSymbol(char sym) {
            bool isPutted = base.PutSymbol(sym);

            if (Status == Status.Complete) {
                var listOfNodes = (ListOfNodes)GetNode();

                var fileNames = from node in listOfNodes.nodes
                                    where node is FileNameNode
                                    select ((FileNameNode)node).fileName;

                this.fileNames = new List<string>(fileNames);

                if (this.fileNames.Count < minimalNumber) {
                    Status = Status.Error;
                    errorMessage = $"Requires at least {this.minimalNumber} file names!";
                }
            }

            return isPutted;
        }


        #region NESTED CLASSES

        #endregion

    }
}
