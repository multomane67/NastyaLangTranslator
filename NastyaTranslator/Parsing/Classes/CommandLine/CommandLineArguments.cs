﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator.Parsing.Classes.CommandLine {
    class CommandLineArguments : AParseRoot {


        public string mode;
        public bool isHaveWaitAnyKey;
        public List<string> sourceFiles;
        public string translatedFile;

        private static Tag tagMode = new Tag();
        private static Tag tagWaitAnyKey = new Tag();
        private static Tag tagAllFiles = new Tag();

        public CommandLineArguments()
            : base("commandline", new SequenceOf(new List<AParseExam>{
                new AnyOf(new List<AParseExam>(){
                    new Str("-asm"),
                    new Str("-bc"),
                }){tag=tagMode},
                new Spaces_OneOrMore(),
                new Possible(
                    new SequenceOf(new List<AParseExam>(){
                        new Str("--wait-any-key"),
                        new Spaces_OneOrMore(),
                    }){tag=tagWaitAnyKey}
                ),
                new FileNames(2){tag=tagAllFiles},
                new Str("\0"),
            }))
        {
            
        }

        public override bool PutSymbol(char sym) {
            bool isPutted = base.PutSymbol(sym);

            if (Status == Status.Complete) {
                
                var parseMode = (GetBy(tagMode) as AnyOf).GetCompleted() as Str;

                mode = parseMode.GetStrSymbols();

                isHaveWaitAnyKey = (GetBy(tagWaitAnyKey) as SequenceOf).Status == Status.Complete;

                List<string> allFiles = (GetBy(tagAllFiles) as FileNames).fileNames;
                
                sourceFiles = allFiles.GetRange(0, allFiles.Count - 1);

                translatedFile = allFiles[allFiles.Count - 1];
            }

            return isPutted;
        }

        public bool TryParseString(string commandLine, out string error) {

            error = null;
            Reset();

            foreach (char c in commandLine) {
                PutSymbol(c);
                if (Status == Status.Error) {
                    break;
                }
            }

            if (Status != Status.Complete && Status != Status.Error) {
                PutSymbol('\0');
            }

            if (Status != Status.Complete) {
                if (Status == Status.Error)
                    error = ErrorMessage;
                else
                    error = "undefined command line parsing error!";

                return false;
            } else {
                return true;
            }

        }
    }

}
