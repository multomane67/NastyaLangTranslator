﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NastyaTranslator.Translation;

namespace NastyaTranslator.Parsing.Classes.CommandLine {
    class FileName : AFillString {
        public string symbols = null;

        protected override string GetStartupMessage(char sym) {
            return $"No symbols in file name!";
        }

        protected override bool IsMatchSymbol(char sym, bool isFirst) {
            return !SpaceSymbols.hashSet.Contains(sym) && sym != '\0';
        }

        protected override bool PostProcess(StringBuilder sb, out string errorMessage) {
            symbols = sb.ToString();

            errorMessage = null;
            return true;
        }

        protected override ANode ConstructNode() {
            return new FileNameNode() { fileName = symbols };
        }
    }

    class FileNameNode : ANode {
        public string fileName = null;

        public override void CheckOnValid() {
            throw new WrongUsageException();
        }

        public override void CreateVarsForConsts() {
            throw new WrongUsageException();
        }

        public override void TranslateToAsm(StringBuilder sb) {
            throw new WrongUsageException();
        }

        public override void TranslateToBc(StringBuilder sb, InterpretatorBuilder ib) {
            throw new WrongUsageException();
        }
    }
}
