﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator.Parsing.Classes {
    static class SpaceSymbols {
        public static readonly HashSet<char> hashSet = new HashSet<char>() {
            ' ', '\t', '\r', '\n',
        };
    }
}
