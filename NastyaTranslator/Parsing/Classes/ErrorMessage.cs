﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator.Parsing.Classes {
    class ErrorMessage {
        public string nearestErrorNodeName = null;
        public string message = null;


        private string ConvertToString() {
            return nearestErrorNodeName != null
                ? $"{nearestErrorNodeName} - {message}"
                : message;
        }

        public static implicit operator string(ErrorMessage msg) {
            return msg.ConvertToString();
        }

        public override string ToString() {
            return ConvertToString();
        }


        private static ErrorMessage ConvertFromString(string msg) {
            return new ErrorMessage() { message = msg };
        }

        public static implicit operator ErrorMessage(string msg) {
            return ConvertFromString(msg);
        }
    }
}
