﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NastyaTranslator.Translation;

namespace NastyaTranslator.Parsing.Classes {
    /// <remarks>
    /// Don't use exams who equal to begining of other.
    /// Beacause more longer variant of this can't be completed.
    /// Smaller variant will always be completed earlier
    /// example:
    /// '>=' - will never be completed
    /// '>' - because this completed earlier
    /// </remarks>
    class AnyOf : AParseExam {

        protected class ExamInfo {
            public AParseExam parser;
            public int numberOfFail;
            public bool wasPutted;
        }

        protected List<ExamInfo> examList = null;

        private AParseExam completeParser = null;

        private bool isPuttedAtComplete = false;

        private int currentSymbolNumber = 0;

        public AnyOf(List<AParseExam> posibleList) {
            examList = new List<ExamInfo>();
            foreach (var p in posibleList) {
                examList.Add(new ExamInfo() { parser = p });
            }
        }

        public override void Init(AParseRoot parseRoot) {
            base.Init(parseRoot);
            foreach (ExamInfo examInfo in examList) {
                examInfo.parser.Init(parseRoot);
            }
        }

        public override void Reset() {
            Status = Status.Idle;

            if (examList.Count == 0)
                throw new Exception("empty posible list");

            foreach (var e in examList) {
                e.parser.Reset();
                e.numberOfFail = -1;
            }

            completeParser = null;

            isPuttedAtComplete = false;

            currentSymbolNumber = 0;
        }

        public override AParseExam GetBy(Tag tag) {
            AParseExam result = base.GetBy(tag);

            if (result == null) {
                foreach (ExamInfo ei in examList) {
                    result = ei.parser.GetBy(tag);
                    if (result != null)
                        break;
                }
            }

            return result;
        }

        public override bool PutSymbol(char sym) {
            return base.PutSymbol(sym);
        }

        public override bool CheckOnValid(char sym) {

            bool isValidSymbol = false;
            foreach (var exam in examList) {
                if (exam.parser.Status != Status.Error) {
                    exam.wasPutted = exam.parser.PutSymbol(sym);

                    if (exam.parser.Status == Status.Error) {
                        exam.numberOfFail = currentSymbolNumber;
                        
                    } else {
                        isValidSymbol = true;
                    }
                }
            }

            if (isValidSymbol) {

                List<ExamInfo> completed = (from exam in examList
                                           where exam.parser.Status == Status.Complete
                                           select exam)
                                           .ToList();

                if (completed.Count == 0) {

                    // just nothing

                } else if (completed.Count == 1) {

                    ExamInfo exam = completed[0];
                    completeParser = exam.parser;
                    isPuttedAtComplete = exam.wasPutted;

                } else {
                    throw new Exception("Wrong AnyOf! A few exams was completed! Must be only one!");
                }

            } else {
                // find max number
                int max = -1;
                examList.ForEach((e) => max = Math.Max(max, e.numberOfFail));
                if (max == -1)
                    throw new Exception("can't find any errors in exam");

                // find all errors whith max number
                List<ErrorMessage> lastErrors = (from e in examList 
                                           where e.numberOfFail == max 
                                           select e.parser.ErrorMessage).ToList();

                if (lastErrors.Count == 1) {
                    errorMessage = lastErrors[0];
                } else {
                    // concat all finded errors to error message
                    // I don't sure - maybe better get first error instead concatinc all with same number
                    StringBuilder sb = new StringBuilder();
                    sb.Append($"possible errors({lastErrors.Count}):\n");
                    foreach (string errStr in lastErrors) {
                        sb.Append($"{errStr}\n");
                    }

                    errorMessage = sb.ToString();
                }
            }

            currentSymbolNumber++;

            return isValidSymbol;
        }

        public override bool CheckOnComplete_afterValid(out bool takeLastSymbol) {

            takeLastSymbol = isPuttedAtComplete;

            return completeParser != null;
        }

        protected override ANode ConstructNode() {
            return completeParser.GetNode();
        }

        public AParseExam GetCompleted() {
            return completeParser;
        }
    }
}
