﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NastyaTranslator.Translation;

namespace NastyaTranslator.Parsing.Classes {
    /// <remarks>
    /// Dont use this spaces variant inside 'Posible' and 'AnyOf'
    /// because it can wrongy terminate it
    /// use Spaces_OneOrMore please
    /// </remarks>
    class Spaces_NoOrMore : AParseExam
    {

        private HashSet<char> ValidSymbols => SpaceSymbols.hashSet;

        private char sym;

        public override void Reset()
        {
            Status = Status.Idle;

            // i don't know
        }

        public override bool PutSymbol(char sym) {
            return base.PutSymbol(sym);
        }

        public override bool CheckOnValid(char sym)
        {
            this.sym = sym;

            return true; // real checking removed into check complete method - for non error behaviour
        }

        public override bool CheckOnComplete_afterValid(out bool takeLastSymbol)
        {
            takeLastSymbol = false;
            return !ValidSymbols.Contains(sym);
        }

        protected override ANode ConstructNode()
        {
            return null;
        }


    }
}
