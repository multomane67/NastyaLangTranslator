﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NastyaTranslator.Translation;

namespace NastyaTranslator.Parsing.Classes {
    class Str : AParseExam {

        private readonly string str;

        private int currIndex = 0;

        public Str(string str) {
            this.str = str;
        }

        public override void Reset() {
            Status = Status.Idle;

            currIndex = 0;
        }

        public override bool PutSymbol(char sym) {
            return base.PutSymbol(sym);
        }

        public override bool CheckOnValid(char sym) {
            char trueSymbol = str[currIndex++];
            bool isValid = sym == trueSymbol;
            if (!isValid) {
                if (str.Length == 1) {
                    errorMessage = $"Finded '{sym}' expected '{trueSymbol}'.";
                } else {
                    errorMessage = $"Illegal symbol '{sym}' in word '{str}' expected '{trueSymbol}'.";
                }
            }

            return isValid;
        }

        public override bool CheckOnComplete_afterValid(out bool takeLastSymbol) {
            bool result = currIndex == str.Length;
            takeLastSymbol = true;
            return result;
        }

        protected override ANode ConstructNode() {
            throw new WrongUsageException();
        }

        public string GetStrSymbols() {
            return str;
        }

    }
}
