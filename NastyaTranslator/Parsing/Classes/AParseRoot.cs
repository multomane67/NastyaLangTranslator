﻿using NastyaTranslator.Translation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator.Parsing.Classes {
    abstract class AParseRoot : AParseExam {

        protected AParseExam parser = null;

        public LocationInFile currLoc = new LocationInFile();

        public override ErrorMessage ErrorMessage {
            get {
                return $"{currLoc}: {base.ErrorMessage}";
            }
        }

        public AParseRoot(string fileName, AParseExam parser) {
            this.parser = parser;
            currLoc.fileName = fileName;
            currLoc.line = 1;
            currLoc.column = 0;
        }

        public override void Init(AParseRoot parseRoot) {
            throw new WrongUsageException();
        }

        public override void Reset() {
            parser.Init(this);
            parser.Reset();
        }

        public override AParseExam GetBy(Tag tag) {
            AParseExam result = base.GetBy(tag);

            if (result == null) {
                result = parser.GetBy(tag);
            }

            return result;
        }

        public override bool PutSymbol(char sym) {

            if (sym == '\n') {
                currLoc.line++;
                currLoc.column = 0;
            } else if (sym == '\t') {
                currLoc.column += 4;
            } else {
                currLoc.column ++;
            }

            bool isPutted = parser.PutSymbol(sym);

            Status = parser.Status;
            if (Status == Status.Error)
                errorMessage = parser.ErrorMessage;
            
            return isPutted;
        }

        public override bool CheckOnComplete_afterValid(out bool takeLastSymbol) {
            throw new WrongUsageException();
        }

        public override bool CheckOnValid(char sym) {
            throw new WrongUsageException();
        }

        protected override ANode ConstructNode() {
            return parser.GetNode();
        }
    }
}
