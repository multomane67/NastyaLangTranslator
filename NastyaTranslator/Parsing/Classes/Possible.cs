﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NastyaTranslator.Translation;

namespace NastyaTranslator.Parsing.Classes {
    class Possible : AParseExam {

        public AParseExam parser = null;

        private bool isFirst = true;
        private bool needCompleteWithNoTake = false;

        public Possible(AParseExam parser) {
            this.parser = parser;
        }

        public override void Init(AParseRoot parseRoot) {
            base.Init(parseRoot);
            parser.Init(parseRoot);
        }

        public override AParseExam GetBy(Tag tag) {
            AParseExam result = base.GetBy(tag);
            if (result == null)
                result = parser.GetBy(tag);
            return result;
        }

        public override void Reset() {
            Status = Status.Idle;
            parser.Reset();
            isFirst = true;
            needCompleteWithNoTake = false;
        }

        public override bool PutSymbol(char sym) {
            return base.PutSymbol(sym);
        }

        public override bool CheckOnValid(char sym) {
            bool wasPutted = parser.PutSymbol(sym);

            bool isValid = parser.Status != Status.Error;
            bool result;

            if (isFirst) {
                needCompleteWithNoTake = !isValid;
                result = true;
            } else {
                result = isValid;
            }

            isFirst = false;

            if (result == false)
                errorMessage = parser.ErrorMessage;

            return result;
        }

        public override bool CheckOnComplete_afterValid(out bool takeLastSymbol) {
            if (needCompleteWithNoTake) {
                takeLastSymbol = false;
                return true;
            } else {
                return parser.CheckOnComplete_afterValid(out takeLastSymbol);
            }
        }

        protected override ANode ConstructNode() {
            return parser.GetNode();
        }
    }
}
