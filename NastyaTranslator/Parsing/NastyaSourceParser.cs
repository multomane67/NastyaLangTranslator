﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

using NastyaTranslator.Translation;
using NastyaTranslator.Parsing.Classes;
using NastyaTranslator.Parsing.Classes.NastyaLang;


namespace NastyaTranslator.Parsing
{
    [System.Serializable]
    class NastyaSourceParser : ASourceParser {
        public override ProgramNode MakeProgramNode(List<string> fileNames) {

            List<NameSpaceNode> nameSpaces = new List<NameSpaceNode>();

            bool succesParsing = true;

            foreach (string fileName in fileNames) {
                var fileParser = new CommentaryFilter(new NSourceFile(fileName));

                fileParser.Reset();

                using (FileStream fs = File.OpenRead(fileName)) {
                    bool stop = false;

                    byte[] b = new byte[1024];
                    UTF8Encoding encoder = new UTF8Encoding(true);

                    while (fs.Read(b, 0, b.Length) > 0 && !stop) {
                        string str = encoder.GetString(b);
                        //Console.WriteLine(str);

                        foreach (char sym in str) {
                            PutSymbolAndCheck(sym, fileParser, ref stop, ref nameSpaces);
                            if (stop)
                                break;
                        }
                    }

                    if (!stop) {
                        // Parser status is not complete or error.
                        // Because parser wait for new symbol now.
                        // But file is ended and we don't have any symbols to put.
                        // And we can put zero - '\0' to parser
                        // for make a readable error about unfinished parsing construction.
                        // But we have a commnetary filter and must put two zeros.
                        PutSymbolAndCheck('\0', fileParser, ref stop, ref nameSpaces);
                        PutSymbolAndCheck('\0', fileParser, ref stop, ref nameSpaces);

                        if (!stop) {
                            // Parser ignore zero.
                            // We must confirm about unexpected end of file
                            // without any information about unfinished construction.
                            LogLexicError("unexpected EOF");
                        } else {
                            // parser log an error.
                        }
                    }
                }

                if (fileParser.Status != Status.Complete) {
                    succesParsing = false;
                    break;
                }
            }

            if (succesParsing) {
                ProgramNode prgNode = null;

                //merge nameSpaces, confirm errors about duplicates

                Dictionary<string, TotalNameSpace> totalNameSpaces = new Dictionary<string, TotalNameSpace>();

                StringBuilder sbErrors = new StringBuilder();

                foreach (var nameSpace in nameSpaces) {
                    TotalNameSpace tns = null;
                    if (!totalNameSpaces.TryGetValue(nameSpace.name, out tns)) {
                        tns = new TotalNameSpace() { name = nameSpace.name };
                        totalNameSpaces.Add(tns.name, tns);
                    }

                    tns.TryMergeIn(nameSpace, sbErrors);
                }

                // this checking will be removed after feature with multiple namespaces will be realized
                foreach (var kvpTns in totalNameSpaces) {
                    if (kvpTns.Key != "Main") {
                        sbErrors.Append($" namespace '{kvpTns.Key}' has illegal name use 'Main' only! (Other names are not supported now.)\n");
                        foreach (var lif in kvpTns.Value.locations) {
                            sbErrors.Append($"  {lif}\n");
                        }
                    }
                }

                if (sbErrors.Length > 0) {
                    Console.WriteLine(sbErrors.ToString());
                } else {
                    if (totalNameSpaces.TryGetValue("Main", out TotalNameSpace totalMain)) {

                        int maxNumberOfVars = -1;
                        if (totalMain.attributesDict.TryGetValue("MaxNumberOfVars", out AttributeNode attrNode)) {
                            maxNumberOfVars = ((Translation.MaxNumberOfVars)attrNode).value;
                        }

                        prgNode = new ProgramNode() {
                            globalVars = totalMain.variablesDict.Values.ToList(),
                            functions = totalMain.functionsDict.Values.ToList(),
                        };

                        if (maxNumberOfVars != -1) {
                            prgNode.maxNumberOfVars = maxNumberOfVars;
                        }

                        prgNode.Init();
                    }
                }

                return prgNode;
            } else {
                return null;
            }
        }

        private void PutSymbolAndCheck(char sym, AParseExam parser, ref bool stop, ref List<NameSpaceNode> nameSpaces) {
            parser.PutSymbol(sym);
            stop = parser.Status == Status.Complete || parser.Status == Status.Error;
            if (stop) {
                if (parser.Status == Status.Error) {
                    LogLexicError(parser.ErrorMessage);
                } else if (parser.Status == Status.Complete) {
                    var listOfNameSpaces = (ListOfNodes)parser.GetNode();
                    nameSpaces.AddRange(from node in listOfNameSpaces.nodes
                                        where node is NameSpaceNode
                                        select (NameSpaceNode)node);
                }
            }
        }

        private void LogLexicError(string message) {
            Console.WriteLine($"lexic error:\n {message}");
        }


        private class TotalNameSpace {
            public string name;

            public List<LocationInFile> locations = new List<LocationInFile>();

            public Dictionary<string, AttributeNode> attributesDict = new Dictionary<string, AttributeNode>();
            public Dictionary<string, Variable> variablesDict = new Dictionary<string, Variable>();
            public Dictionary<string, FuncNode> functionsDict = new Dictionary<string, FuncNode>();

            public bool TryMergeIn(NameSpaceNode nameSpaceNode, StringBuilder sbErrors) {

                locations.Add(nameSpaceNode.lif);

                Dictionary<string, List<LocationInFile>> duplicatedAttributes = new Dictionary<string, List<LocationInFile>>();

                foreach (AttributeNode attribute in nameSpaceNode.attributes) {
                    AttributeNode existedAttribute = null;
                    if (!attributesDict.TryGetValue(attribute.name, out existedAttribute)) {
                        attributesDict.Add(attribute.name, attribute);
                    } else {
                        List<LocationInFile> listOfLocations = null;
                        if (!duplicatedAttributes.TryGetValue(attribute.name, out listOfLocations)) {
                            listOfLocations = new List<LocationInFile>();
                            listOfLocations.Add(existedAttribute.lif);
                            duplicatedAttributes.Add(attribute.name, listOfLocations);
                        }
                        listOfLocations.Add(attribute.lif);
                    }
                }

                Dictionary<string, List<LocationInFile>> duplicatedFunctions = new Dictionary<string, List<LocationInFile>>();

                foreach (FuncNode func in nameSpaceNode.functions) {
                    FuncNode existedFunc = null;
                    if (!functionsDict.TryGetValue(func.name, out existedFunc)) {
                        functionsDict.Add(func.name, func);
                    } else {
                        List<LocationInFile> listOfLocations = null;
                        if (!duplicatedFunctions.TryGetValue(func.name, out listOfLocations)) {
                            listOfLocations = new List<LocationInFile>();
                            listOfLocations.Add(existedFunc.lif);
                            duplicatedFunctions.Add(func.name, listOfLocations);
                        }
                        listOfLocations.Add(func.lif);
                    }
                }

                Dictionary<string, List<LocationInFile>> duplicatedVariables = new Dictionary<string, List<LocationInFile>>();

                foreach (Variable variable in nameSpaceNode.variables) {
                    Variable existedVariable = null;
                    if (!variablesDict.TryGetValue(variable.name, out existedVariable)) {
                        variablesDict.Add(variable.name, variable);
                    } else {
                        List<LocationInFile> listOfLocations = null;
                        if (!duplicatedVariables.TryGetValue(variable.name, out listOfLocations)) {
                            listOfLocations = new List<LocationInFile>();
                            listOfLocations.Add(existedVariable.lif);
                            duplicatedVariables.Add(variable.name, listOfLocations);
                        }
                        listOfLocations.Add(variable.lif);
                    }
                }


                bool haveErrors = duplicatedAttributes.Count > 0 || duplicatedFunctions.Count > 0 || duplicatedVariables.Count > 0;

                if (haveErrors) {

                    foreach (var kvp in duplicatedAttributes) {
                        sbErrors.Append($" duplicate attributes '{kvp.Key}'\n");

                        foreach (var lif in kvp.Value) {
                            sbErrors.Append($"  {lif}\n");
                        }
                    }

                    foreach (var kvp in duplicatedFunctions) {
                        sbErrors.Append($" duplicate functions '{kvp.Key}'\n");

                        foreach (var lif in kvp.Value) {
                            sbErrors.Append($"  {lif}\n");
                        }
                    }

                    foreach (var kvp in duplicatedVariables) {
                        sbErrors.Append($" duplicate variables '{kvp.Key}'\n");

                        foreach (var lif in kvp.Value) {
                            sbErrors.Append($"  {lif}\n");
                        }
                    }

                }

                return !haveErrors;
            }
        }
    }
}
