﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NastyaTranslator.Translation;

namespace NastyaTranslator.Parsing {
    abstract class ASourceParser {
        public abstract ProgramNode MakeProgramNode(List<string> files);
    }
}
