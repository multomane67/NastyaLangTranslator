﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NastyaTranslator.Translation;

namespace NastyaTranslator {
    static class Utils {
        // string utils
        public static bool IsNullOrEmpty(this string str) {
            return string.IsNullOrEmpty(str);
        }

        public static bool IsNullOrWhiteSpace(this string str) {
            return string.IsNullOrWhiteSpace(str);
        }

        public static bool InDiapasone(this char symbol, char min, char max) {
            return min <= symbol && symbol <= max;
        }

        // alligning
        private const string ALLIGN_SPACES_NUM_EXCEPTION = "maxCount should be one more than line.Length for better edge controlling";
        public static string AlignBySpacesLeft(this string line, int maxCount) {
            int lineLength = line.Length;
            int spacesForAdd = maxCount - lineLength;
            if (spacesForAdd < 0) {
                throw new Exception(ALLIGN_SPACES_NUM_EXCEPTION);
            } else {
                string spaces = new string(' ', spacesForAdd);
                return line + spaces;
            }
        }

        public static string AlignBySpacesRight(this string line, int maxCount) {
            int lineLength = line.Length;
            int spacesForAdd = maxCount - lineLength;
            if (spacesForAdd < 0) {
                throw new Exception(ALLIGN_SPACES_NUM_EXCEPTION);
            } else {
                string spaces = new string(' ', spacesForAdd);
                return spaces + line;
            }
        }

        // list utils
        public static string ToStrList<T>(this IEnumerable<T> arr, string separator = ", ", Func<T, string> customToString = null)
        {
            StringBuilder sb = new StringBuilder();

            bool isFirst = true;

            foreach (T obj in arr) {
                if (!isFirst)
                    sb.Append(separator);

                if (customToString != null) {
                    sb.Append(customToString.Invoke(obj));
                } else {
                    sb.Append(obj.ToString());
                }

                isFirst = false;
            }

            return sb.ToString();
        }

        // prog nodes utils
        public static string FuncNameToAsm(string name) {
            return "func_" + name;
        }

        public static string VarNameToAsm(string name, bool isLocal = false) {
            if (isLocal) {
                throw new Exception("Not implemented for local vars!");
            } else {
                return "_" + name;
            }
        }

        public static List<AOperatorNode> ToOperatorsNodes(this List<ANode> nodes) {
            List<AOperatorNode> operators = (from node in nodes
                                             where node is AOperatorNode
                                             select (AOperatorNode)node).ToList();

            return operators;
        }
    }
}
