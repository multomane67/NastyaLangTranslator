﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator.Translation {
    class OperatorFor : AOperatorCycle {
        public OperatorFor() {
            this.opName = "for";

            this.forRepeatLabel = $"for{forNumber}_repeat";

            this.breakLabel = $"for{forNumber}_break";
            this.continueLabel = $"for{forNumber}_continue";

            forNumber++;
        }

        private string forRepeatLabel = null;

        private static int forNumber = 0;

        // first init section
        public OperatorSet initv = null;

        public OperatorIf check = null;

        public OperatorCalc iter = null;

        public List<AOperatorNode> operators = new List<AOperatorNode>();

        public override void Init(FuncNode fn) {
            base.Init(fn);

            initv.Init(fn);
            check.Init(fn);
            iter.Init(fn);

            foreach (var on in operators)
                on.Init(fn);
        }

        private bool IsInitVarExists() {
            return initv.aVar != null || initv.bConst != null || initv.bVar != null;
        }

        private bool IsCheckExists() {
            return check.aVar != null || check.aConst != null || check.comparisonOperator != null || check.bVar != null || check.bConst != null;
        }

        private bool IsIterExists() {
            return iter.rVar != null || iter.aVar != null || iter.aConst != null || iter.operation != null || iter.bVar != null || iter.bConst != null;
        }

        public override void CheckOnValid() {
            CheckLoc();

            if (IsInitVarExists()) {
                try {
                    initv.CheckOnValid();
                } catch (Exception e) {
                    throw new MessageForUserException($"{LogPrefix} error in init part:\n {e.Message}");
                }
            }

            if (IsCheckExists()) {
                try {
                    check.CheckOnValid();
                } catch (Exception e) {
                    string message = e.Message.Replace(" 'if'", "");
                    throw new MessageForUserException($"{LogPrefix} error in check part:\n {message}");
                }
            }

            if (IsIterExists()) {
                try {
                    iter.CheckOnValid();
                } catch (Exception e) {
                    throw new MessageForUserException($"{LogPrefix} error in iteration part:\n {e.Message}");
                }
            }

            CyclesStack_Add(this);

            // check inside
            foreach (var op in operators) {
                op.CheckOnValid();
            }

            CyclesStack_Remove(this);
        }

        public override void TranslateToAsm(StringBuilder sb) {
            bool isInitVarExists = IsInitVarExists();
            bool isCheckExists = IsCheckExists();
            bool isIterExists = IsIterExists();

            string initv_val = !string.IsNullOrEmpty(initv.bConst) ? initv.bConst : initv.bVar;
            string sInitv = isInitVarExists ? $"{initv.aVar} = {initv_val}" : "";

            string check_a = !string.IsNullOrEmpty(check.aConst) ? check.aConst : check.aVar;
            string check_b = !string.IsNullOrEmpty(check.bConst) ? check.bConst : check.bVar;
            string sCheck = isCheckExists ? $"{check_a} {check.comparisonOperator} {check_b}" : "";

            string iter_a = !string.IsNullOrEmpty(iter.aConst) ? iter.aConst : iter.aVar;
            string iter_b = !string.IsNullOrEmpty(iter.bConst) ? iter.bConst : iter.bVar;
            string sIter = isIterExists ? $"{iter.rVar} = {iter_a} {iter.operation} {iter_b}" : "";

            sb.Append($"                    ; for ({sInitv}; {sCheck}; {sIter}) \n");


            CyclesStack_Add(this);

            // prepare inside operators
            List<AOperatorNode> insideOperators = new List<AOperatorNode>(operators);

            // 
            insideOperators.Add(new OperatorAsm { asmText = this.continueLabel + ":" });

            // add iteration to end of inside operators
            if (isIterExists) {
                insideOperators.Add(iter);
            }

            // add jump to repeat at the end of inside operators
            insideOperators.Add(new OperatorAsm { asmText = $"    JP {forRepeatLabel}" });


            
            // 1) PRINT INIT VARS PART
            if (isInitVarExists) {
                initv.TranslateToAsm(sb);
            }

            // 2) PRINT REPEAT LABEL
            sb.Append(forRepeatLabel + ":\n");

            if (isCheckExists) {
                // add inside operators to IF for checking
                check.ifOperators = insideOperators;
                // 3) PRINT THE CHECKING "IF" WITH INSIDE OPERATORS
                check.TranslateToAsm(sb);
                check.ifOperators = null; // nulling for correct repeat compiling
            } else {
                // write insideOperators one by one without "IF" checking
                foreach (var insideOp in insideOperators)
                    insideOp.TranslateToAsm(sb);
            }

            sb.Append(this.breakLabel + ":\n");

            CyclesStack_Remove(this);
        }

        public override void TranslateToBc(StringBuilder sb, InterpretatorBuilder ib) {
            bool isInitVarExists = IsInitVarExists();
            bool isCheckExists = IsCheckExists();
            bool isIterExists = IsIterExists();

            string initv_val = !string.IsNullOrEmpty(initv.bConst) ? initv.bConst : initv.bVar;
            string sInitv = isInitVarExists ? $"{initv.aVar} = {initv_val}" : "";

            string check_a = !string.IsNullOrEmpty(check.aConst) ? check.aConst : check.aVar;
            string check_b = !string.IsNullOrEmpty(check.bConst) ? check.bConst : check.bVar;
            string sCheck = isCheckExists ? $"{check_a} {check.comparisonOperator} {check_b}" : "";

            string iter_a = !string.IsNullOrEmpty(iter.aConst) ? iter.aConst : iter.aVar;
            string iter_b = !string.IsNullOrEmpty(iter.bConst) ? iter.bConst : iter.bVar;
            string sIter = isIterExists ? $"{iter.rVar} = {iter_a} {iter.operation} {iter_b}" : "";

            sb.Append($"                    ; for ({sInitv}; {sCheck}; {sIter}) \n");


            CyclesStack_Add(this);

            // prepare inside operators
            List<AOperatorNode> insideOperators = new List<AOperatorNode>(operators);

            // 
            insideOperators.Add(new OperatorByteCodeText {
                action = null,
                bcText = this.continueLabel + ":\n"
            });

            // add iteration to end of inside operators
            if (isIterExists) {
                insideOperators.Add(iter);
            }

            // add jump to repeat at the end of inside operators
            insideOperators.Add(new OperatorByteCodeText {
                action = () => { ib.Registrate(PBCI.inter_opJmpW); },
                bcText = $"    DEFB {PBCI.inter_opJmpW.opCode}\n"+
                         $"    DEFW {forRepeatLabel}\n",
            });



            // 1) PRINT INIT VARS PART
            if (isInitVarExists) {
                initv.TranslateToBc(sb, ib);
            }

            // 2) PRINT REPEAT LABEL
            sb.Append(forRepeatLabel + ":\n");

            if (isCheckExists) {
                // add inside operators to IF for checking
                check.ifOperators = insideOperators;
                // 3) PRINT THE CHECKING "IF" WITH INSIDE OPERATORS
                check.TranslateToBc(sb, ib);
                check.ifOperators = null; // nulling for correct repeat compiling
            } else {
                // write insideOperators one by one without "IF" checking
                foreach (var insideOp in insideOperators)
                    insideOp.TranslateToBc(sb, ib);
            }

            sb.Append(this.breakLabel + ":\n");

            CyclesStack_Remove(this);
        }

        public override void CreateVarsForConsts() {
            if (IsInitVarExists()) {
                initv.CreateVarsForConsts();
            }

            if (IsCheckExists()) {
                check.CreateVarsForConsts();
            }

            if (IsIterExists()) {
                iter.CreateVarsForConsts();
            }

            foreach (AOperatorNode op in operators) {
                op.CreateVarsForConsts();
            }
        }
    }
}
