﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator.Translation {
    class FuncNode : ANode {
        public override string LogPrefix => $"{base.LogPrefix} 'function'";

        public string name = null;

        public ProgramNode pn = null;

        public List<Variable> localVars = new List<Variable>();
        public List<AOperatorNode> operators = new List<AOperatorNode>();

        public virtual void Init(ProgramNode pn) {
            this.pn = pn;

            foreach (AOperatorNode on in operators)
                on.Init(this);
        }

        public Variable GetLocalVariableWithName(string varName) {
            foreach (var var in localVars) {
                if (var.name == varName)
                    return var;
            }

            return null;
        }

        // is var visible from function
        public bool IsVariableNameExists(string varName) {
            bool isExists = false;
            foreach (var someLocVar in localVars)
                if (varName == someLocVar.name) {
                    isExists = true;
                    break;
                }
            foreach (var someGlobVar in pn.globalVars)
                if (varName == someGlobVar.name) {
                    isExists = true;
                    break;
                }
            return isExists;
        }

        // get var from function
        public Variable GetVariableWithName(string varName) {
            Variable var = null;

            var = GetLocalVariableWithName(varName);
            if (var != null) return var;

            var = pn.GetVariableWithName(varName);
            if (var != null) return var;

            //TODO remove exception from this place ???
            throw new Exception($"Variable with name '{varName}' was not finded.");
        }

        public override void CheckOnValid() {
            CheckLoc();

            foreach (var op in operators) {
                op.CheckOnValid();
            }
        }

        public override void TranslateToAsm(StringBuilder sb) {
            sb.Append(Utils.FuncNameToAsm(name) + ": \n");

            foreach (var op in operators) {
                op.TranslateToAsm(sb);
                
            }

            sb.Append("\n");
            sb.Append("    ret \n");
        }

        public override void TranslateToBc(StringBuilder sb, InterpretatorBuilder ib) {
            sb.Append(Utils.FuncNameToAsm(name) + ": \n");

            foreach (var op in operators) {
                op.TranslateToBc(sb, ib);
            }

            sb.Append("    \n");
            sb.Append("    DEFB opRet\n");
        }

        public override void CreateVarsForConsts() {

        }
    }
}
