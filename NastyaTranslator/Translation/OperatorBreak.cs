﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator.Translation {
    class OperatorBreak : AOperatorNode {
        public OperatorBreak() {
            this.opName = "break";
        }

        public override void CheckOnValid() {
            CheckLoc();

            if (AOperatorCycle.currCycle == null) {
                throw new MessageForUserException($"{LogPrefix} is outside the cycle.");
            }
        }

        public override void TranslateToAsm(StringBuilder sb) {
            sb.Append($"                    ; break \n");

            sb.Append($"    JP {AOperatorCycle.currBreakLab}\n");
        }

        public override void TranslateToBc(StringBuilder sb, InterpretatorBuilder ib) {
            sb.Append($"                    ; break \n");

            ib.Registrate(PBCI.inter_opJmpW);
            string opCode = PBCI.inter_opJmpW.opCode;

            sb.Append($"DEFB {opCode}\n");
            sb.Append($"DEFW {AOperatorCycle.currBreakLab}\n");
        }

        public override void CreateVarsForConsts() {

        }
    }
}
