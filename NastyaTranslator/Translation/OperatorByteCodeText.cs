﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator.Translation {
    // this class need's for help translating the cycles into byte code
    class OperatorByteCodeText : AOperatorNode { // maybe beter name - OperatorRawByteCode or OperatorRawByteCodeText

        public Action action = null;
        public string bcText = null;

        public override void CheckOnValid() {
            throw new Exception("Not expecting using for CheckOnValid()");
        }

        public override void CreateVarsForConsts() {
            throw new Exception("Not expecting using for CreateVarsForConsts()");
        }

        public override void TranslateToAsm(StringBuilder strBld) {
            throw new Exception("Not expecting using for TranslateToAsm()");
        }

        public override void TranslateToBc(StringBuilder sb, InterpretatorBuilder ib) {
            action?.Invoke();

            if (bcText != null) {
                sb.Append(bcText);
            }
        }
    }
}
