﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator.Translation {
    class OperatorCalc : AOperatorNode {

        // TODO: optimization - if you use few clc operations with one result variable then you can skip some operations like save result or load previous resul values
        // TODO: optimization - if you want make sub with bvar  you can make - SUB (bvar) without LD 

        public OperatorCalc() {
            this.opName = "calc";
        }

        public string rVar = null;

        public string aVar = null;
        public string aConst = null;

        public string operation = null;

        public string bVar = null;
        public string bConst = null;

        private bool isChecked = false;

        public static int logicShiftNumber = 0;

        public static HashSet<string> posibleOps = new HashSet<string>() {
            "+", "-", "<<", ">>"
        };
        public static HashSet<string> commutativeOps = new HashSet<string>() {
            "+" //, "*", "&", "|", "^", "!=", "=="
        };

        private static SuitableCreator<AOperatorNode> suitableCreator = new SuitableCreator<AOperatorNode>() {
            candidates = new List<SuitableCreator<AOperatorNode>.Candidate>() {
                new SuitableCreator<AOperatorNode>.Candidate {
                    isMatch = (forExchange) => (forExchange as OperatorCalc).CanBeAnUnary(),
                    getSuitable = (forExchange) => new OperatorCalcUnary(forExchange as OperatorCalc),
                },
                new SuitableCreator<AOperatorNode>.Candidate {
                    isMatch = (forExchange) => (forExchange as OperatorCalc).CanBeABinary(),
                    getSuitable = (forExchange) => new OperatorCalcBinary(forExchange as OperatorCalc),
                },
                new SuitableCreator<AOperatorNode>.Candidate {
                    isMatch = (forExchange) => {
                        var calc = forExchange as OperatorCalc;
                        return !calc.CanBeAnUnary() && !calc.CanBeABinary();
                    },
                    getSuitable = (forExchange) => new OperatorCalcRAB(forExchange as OperatorCalc),
                },
            }
        };

        private AOperatorNode subOperator = null;
        private AOperatorNode SubOperator {
            get {
                if (!isChecked)
                    throw new Exception("Don't create calc realization agent before checking");
                
                if (subOperator == null) {
                    if (suitableCreator.TryCreateSuitable(this, out subOperator, out string error)) {
                        subOperator.Init(fn);
                    } else {
                        throw new Exception($"{LogPrefix} fail to create a suitable agent with error '{error}'.");
                    }
                }

                return subOperator;
            }
        }

        private bool IsCommutative() {
            return commutativeOps.Contains(operation);
        }

        private bool IsHaveConstValueOne() {
            return (!aConst.IsNullOrWhiteSpace() && Literal.GetAsmValue(aConst) == "1") || (!bConst.IsNullOrWhiteSpace() && Literal.GetAsmValue(bConst) == "1");

        }

        private bool CanBeABinary() {
            bool resultIsA = rVar == aVar;
            bool resultIsB = rVar == bVar;

            if (IsCommutative()) {
                return (resultIsA || resultIsB) && !IsHaveConstValueOne();
            } else {
                return resultIsA && !IsHaveConstValueOne();
            }
        }

        private bool CanBeAnUnary() {
            bool resultIsA = rVar == aVar;
            bool resultIsB = rVar == bVar;

            if (IsCommutative()) {
                return (resultIsA || resultIsB) && IsHaveConstValueOne();
            } else {
                return resultIsA && IsHaveConstValueOne();
            }
        }

        public override void TranslateToAsm(StringBuilder sb) {
            SubOperator.TranslateToAsm(sb);
        }

        public override void TranslateToBc(StringBuilder sb, InterpretatorBuilder ib) {
            SubOperator.TranslateToBc(sb, ib);
        }

        public override void CheckOnValid() {
            CheckLoc();

            if (string.IsNullOrEmpty(rVar)) {
                throw new MessageForUserException($"{LogPrefix} doesn't has a result var.");
            } else if (!fn.IsVariableNameExists(rVar)) {
                throw new MessageForUserException($"{LogPrefix} result var '{rVar}' is not defined.");
            }
            if (string.IsNullOrEmpty(aVar) && string.IsNullOrEmpty(aConst)) {
                throw new MessageForUserException($"{LogPrefix} doesn't has a first argument.");
            }
            if (string.IsNullOrEmpty(bVar) && string.IsNullOrEmpty(bConst)) {
                throw new MessageForUserException($"{LogPrefix} doesn't has a second argument.");
            }
            if (!string.IsNullOrEmpty(aVar) && !fn.IsVariableNameExists(aVar)) {
                throw new MessageForUserException($"{LogPrefix} first argument '{aVar}' is not defined.");
            }
            if (!string.IsNullOrEmpty(bVar) && !fn.IsVariableNameExists(bVar)) {
                throw new MessageForUserException($"{LogPrefix} second argument '{bVar}' is not defined.");
            }

            if (string.IsNullOrEmpty(operation)) {
                throw new MessageForUserException($"{LogPrefix} has no arithmetic operator.");
            } else if (!posibleOps.Contains(operation)) {
                throw new MessageForUserException($"{LogPrefix} has illegal arithmetic operator '{operation}'.");
            }

            isChecked = true;
        }

        public override void CreateVarsForConsts() {
            SubOperator.CreateVarsForConsts();
        }

        private class SuitableCreator<T> where T : class {
            public class Candidate {
                public Predicate<T> isMatch = null;
                public Func<T,T> getSuitable = null;
            }

            public List<Candidate> candidates = null;

            public bool TryCreateSuitable(T forExchange, out T exchanged, out string error) {

                if (candidates != null) {
                    List<Candidate> matched = (from c in candidates
                                               where c.isMatch(forExchange)
                                               select c).ToList();

                    if (matched.Count == 1) {
                        exchanged = matched[0].getSuitable(forExchange);
                        error = null;
                        return true;
                    } else {
                        exchanged = null;
                        error = matched.Count < 1 ? "No matched for create" : $"Finded {matched.Count} for create must be a one.";
                        return false;
                    }
                } else {
                    exchanged = null;
                    error = "List of candidates are null";
                    return false;
                }
            }
        }
    }
}
