﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator.Translation {
    class OperatorPtr : AOperatorNode {
        public OperatorPtr() {
            this.opName = "ptr";
        }
        /*
         posibilities
            aUnPtr                    = bVar(Byte)      (memory by a-pointer takes Byte from b-var)
            aUnPtr, aIndexVar(Byte)   = bVar(Byte)      +
            aUnPtr, aIndexVar(Word)   = bVar(Byte)      +
            aUnPtr, aIndexConst(Byte) = bVar(Byte)      +
            aUnPtr, aIndexConst(Word) = bVar(Byte)      +

            aUnPtr                    = bVar(Word)      (memory by a-pointer takes Word from b-var)
            aUnPtr, aIndexVar(Byte)   = bVar(Word)      +
            aUnPtr, aIndexVar(Word)   = bVar(Word)      +
            aUnPtr, aIndexConst(Byte) = bVar(Word)      +
            aUnPtr, aIndexConst(Word) = bVar(Word)      +

            aUnPtr                    = bConst(Byte)    (memory by a-pointer takes Byte from b-const)
            aUnPtr, aIndexVar(Byte)   = bConst(Byte)    +
            aUnPtr, aIndexVar(Word)   = bConst(Byte)    +
            aUnPtr, aIndexConst(Byte) = bConst(Byte)    +
            aUnPtr, aIndexConst(Word) = bConst(Byte)    +

            aUnPtr                    = bConst(Word)    (memory by a-pointer takes Word from b-const)
            aUnPtr, aIndexVar(Byte)   = bConst(Word)    +
            aUnPtr, aIndexVar(Word)   = bConst(Word)    +
            aUnPtr, aIndexConst(Byte) = bConst(Word)    +
            aUnPtr, aIndexConst(Word) = bConst(Word)    +


            aVar(Byte) = bUnPtr                         (a-var takes Byte from memory by b-pointer )
            aVar(Byte) = bUnPtr, bIndexVar(Byte)        +
            aVar(Byte) = bUnPtr, bIndexVar(Word)        +
            aVar(Byte) = bUnPtr, bIndexConst(Byte)      +
            aVar(Byte) = bUnPtr, bIndexConst(Word)      +

            aVar(Word) = bUnPtr                         (a-var takes Word from memory by b-pointer)
            aVar(Word) = bUnPtr, bIndexVar(Byte)        +
            aVar(Word) = bUnPtr, bIndexVar(Word)        +
            aVar(Word) = bUnPtr, bIndexConst(Byte)      +
            aVar(Word) = bUnPtr, bIndexConst(Word)      +

            make checks
            aUnPtr is incompartible with bIndex


            aPtr = bAddrOf(Byte)                        (set a-pointer addres of b-var)
            aPtr = bAddrOf(Word)                        (set a-pointer addres of b-var)
             */
        public string aUnPtr = null;
        public string aIndexVar = null;     // word and byte
        public string aIndexConst = null;   // word and byte but without check
        public string aPtr = null;          // use only with bAddrOf

        public string aVar = null;


        public string bUnPtr = null;
        public string bIndexVar = null;     // word and byte
        public string bIndexConst = null;   // word and byte but without checknm 
        public string bAddrOf = null;

        public string bVar = null;
        public string bConst = null;

        public override void TranslateToAsm(StringBuilder sb) {
            string argPtrA = (aIndexVar.IsNullOrEmpty() && aIndexConst.IsNullOrEmpty()) ? $"*{aUnPtr}" : (!aIndexVar.IsNullOrEmpty()) ? $"{aUnPtr}[{aIndexVar}]" : (!aIndexConst.IsNullOrEmpty()) ? $"{aUnPtr}[{aIndexConst}]" : null;
            string argPtrB = (bIndexVar.IsNullOrEmpty() && bIndexConst.IsNullOrEmpty()) ? $"*{bUnPtr}" : (!bIndexVar.IsNullOrEmpty()) ? $"{bUnPtr}[{bIndexVar}]" : (!bIndexConst.IsNullOrEmpty()) ? $"{bUnPtr}[{bIndexConst}]" : null;

            string commentaryArgA = !aUnPtr.IsNullOrEmpty() ? argPtrA : !aPtr.IsNullOrEmpty() ? aPtr : !aVar.IsNullOrEmpty() ? aVar : "{error}";
            string commentaryArgB = !bUnPtr.IsNullOrEmpty() ? argPtrB : !bAddrOf.IsNullOrEmpty() ? "&" + bAddrOf : !bVar.IsNullOrEmpty() ? bVar : !bConst.IsNullOrEmpty() ? bConst : "{error}";
            sb.Append($"                    ; ptr {commentaryArgA} = {commentaryArgB}\n");

            if (!aUnPtr.IsNullOrEmpty()) {
                sb.Append($"    LD HL, ({Utils.VarNameToAsm(aUnPtr)}) \n"); // load the value of first variable(pointer) to HL

                if (!bVar.IsNullOrEmpty()) {
                    //check byte or word operation we compile (use second var arg bv)
                    Variable vbVar = fn.GetVariableWithName(bVar);

                    if (!aIndexVar.IsNullOrEmpty() || !aIndexConst.IsNullOrEmpty()) {
                        AddIndexToHL(sb, aIndexVar, aIndexConst, vbVar.type);
                    }

                    if (vbVar.type == ST.BYTE) {
                        sb.Append($"    LD A, ({Utils.VarNameToAsm(bVar)}) \n"); // load the value of second varible to A
                        sb.Append($"    LD (HL), A \n"); // set second var value to place addressed by first variable
                    } else if (vbVar.type == ST.WORD) {
                        sb.Append($"    LD BC, ({Utils.VarNameToAsm(bVar)}) \n"); // load the value of second varible to HL);
                        sb.Append($"    LD (HL), C \n");    // set lo byte of val
                        sb.Append($"    INC HL \n");        // inc HL for second byte addr
                        sb.Append($"    LD (HL), B \n");    // set hi byte of val
                    } else {
                        throw new Exception("error");
                    }
                } else if (!bConst.IsNullOrEmpty()) {
                    //check byte or word operation we compile (use second var arg bc)
                    string bConstType = Literal.GetTypeOfCast(bConst);

                    if (!aIndexVar.IsNullOrEmpty() || !aIndexConst.IsNullOrEmpty()) {
                        AddIndexToHL(sb, aIndexVar, aIndexConst, bConstType);
                    }

                    if (bConstType == ST.BYTE) {
                        sb.Append($"    LD A, {Literal.GetAsmValue(bConst)} \n"); // load the value of second varible to A
                        sb.Append($"    LD (HL), A \n");                          // set second var value to place addressed by first variable
                    } else if (bConstType == ST.WORD) {
                        sb.Append($"    LD BC, {Literal.GetAsmValue(bConst)} \n");// load the value of second varible to HL
                        sb.Append($"    LD (HL), C \n");                          // set lo byte of val
                        sb.Append($"    INC HL \n");                              // inc HL for second byte addr
                        sb.Append($"    LD (HL), B \n");                          // set hi byte of val
                    } else {
                        throw new Exception("error");
                    }
                } else {
                    throw new Exception("error");
                }
            } else if (!aPtr.IsNullOrEmpty()) {
                Variable vbAddrOf = fn.GetVariableWithName(bAddrOf);
                // booth is equal
                if (vbAddrOf.type == ST.BYTE) {
                    sb.Append($"    LD BC, {Utils.VarNameToAsm(bAddrOf)} \n"); // load the addr of second var
                    sb.Append($"    LD ({Utils.VarNameToAsm(aPtr)}), BC \n");  // set addr like value of first
                } else if (vbAddrOf.type == ST.WORD) {
                    sb.Append($"    LD BC, {Utils.VarNameToAsm(bAddrOf)} \n"); // load the addr of second var
                    sb.Append($"    LD ({Utils.VarNameToAsm(aPtr)}), BC \n");  // set addr like value of first
                } else {
                    throw new Exception("error");
                }
            } else if (!aVar.IsNullOrEmpty()) {
                sb.Append($"    LD HL, ({Utils.VarNameToAsm(bUnPtr)}) \n"); // load the value of second variable(pointer) to HL

                //check byte or word operation we compile (use second var arg bv and bc)
                Variable vaVar = fn.GetVariableWithName(aVar);

                if (!bIndexVar.IsNullOrEmpty() || !bIndexConst.IsNullOrEmpty()) {
                    AddIndexToHL(sb, bIndexVar, bIndexConst, vaVar.type);
                }

                if (vaVar.type == ST.BYTE) {
                    sb.Append($"    LD A, (HL) \n");                          // load the value by addres
                    sb.Append($"    LD ({Utils.VarNameToAsm(aVar)}), A \n");  // set loaded to var
                } else if (vaVar.type == ST.WORD) {
                    sb.Append($"    LD C, (HL) \n");                          // load the lo value by addres
                    sb.Append($"    INC HL \n");                              // next byte
                    sb.Append($"    LD B, (HL) \n");                          // load the hi value by addres
                    sb.Append($"    LD ({Utils.VarNameToAsm(aVar)}), BC \n"); // set loaded to var
                } else {
                    throw new Exception("error");
                }
            } else {
                throw new Exception("error");
            }
        }

        public override void TranslateToBc(StringBuilder sb, InterpretatorBuilder ib) {
            string argPtrA = (aIndexVar.IsNullOrEmpty() && aIndexConst.IsNullOrEmpty()) ? $"*{aUnPtr}" : (!aIndexVar.IsNullOrEmpty()) ? $"{aUnPtr}[{aIndexVar}]" : (!aIndexConst.IsNullOrEmpty()) ? $"{aUnPtr}[{aIndexConst}]" : null;
            string argPtrB = (bIndexVar.IsNullOrEmpty() && bIndexConst.IsNullOrEmpty()) ? $"*{bUnPtr}" : (!bIndexVar.IsNullOrEmpty()) ? $"{bUnPtr}[{bIndexVar}]" : (!bIndexConst.IsNullOrEmpty()) ? $"{bUnPtr}[{bIndexConst}]" : null;

            string commentaryArgA = !aUnPtr.IsNullOrEmpty() ? argPtrA : !aPtr.IsNullOrEmpty() ? aPtr : !aVar.IsNullOrEmpty() ? aVar : "{error}";
            string commentaryArgB = !bUnPtr.IsNullOrEmpty() ? argPtrB : !bAddrOf.IsNullOrEmpty() ? "&" + bAddrOf : !bVar.IsNullOrEmpty() ? bVar : !bConst.IsNullOrEmpty() ? bConst : "{error}";
            sb.Append($"                    ; ptr {commentaryArgA} = {commentaryArgB}\n");

            string opCode = null;
            byte argP;
            byte argI;
            byte argV;

            if (!aUnPtr.IsNullOrEmpty()) {

                argP = fn.pn.GetVariableWithName(aUnPtr).idForByteCode;
                argV = fn.pn.GetVariableWithName(bVar).idForByteCode;

                if (aIndexVar.IsNullOrEmpty()) {
                    ib.Registrate(PBCI.inter_opPtr_Put_PV);
                    opCode = PBCI.inter_opPtr_Put_PV.opCode;

                    sb.Append($"DEFB {opCode}, {argP}, {argV}\n");
                } else {
                    ib.Registrate(PBCI.inter_opPtr_Put_PIV);
                    opCode = PBCI.inter_opPtr_Put_PIV.opCode;

                    argI = fn.pn.GetVariableWithName(aIndexVar).idForByteCode;

                    sb.Append($"DEFB {opCode}, {argP}, {argI}, {argV}\n");
                }

            } else if (!aPtr.IsNullOrEmpty()) {

                argP = fn.pn.GetVariableWithName(aPtr).idForByteCode;
                argV = fn.pn.GetVariableWithName(bAddrOf).idForByteCode;

                ib.Registrate(PBCI.inter_opPtr_AddrOf_VP);
                opCode = PBCI.inter_opPtr_AddrOf_VP.opCode;

                sb.Append($"DEFB {opCode}, {argV}, {argP}\n");

            } else if (!aVar.IsNullOrEmpty()) {

                argP = fn.pn.GetVariableWithName(bUnPtr).idForByteCode;
                argV = fn.pn.GetVariableWithName(aVar).idForByteCode;

                if (bIndexVar.IsNullOrEmpty()) {
                    ib.Registrate(PBCI.inter_opPtr_Get_PV);
                    opCode = PBCI.inter_opPtr_Get_PV.opCode;

                    sb.Append($"DEFB {opCode}, {argP}, {argV}\n");
                } else {
                    ib.Registrate(PBCI.inter_opPtr_Get_PIV);
                    opCode = PBCI.inter_opPtr_Get_PIV.opCode;

                    argI = fn.pn.GetVariableWithName(bIndexVar).idForByteCode;

                    sb.Append($"DEFB {opCode}, {argP}, {argI}, {argV}\n");
                }
            }
        }

        private void AddIndexToHL(StringBuilder sb, string indexVar, string indexConst, string transferType) {
            if (!indexVar.IsNullOrEmpty()) {
                // load to BC the index value from var
                Variable vIndexVar = fn.GetVariableWithName(indexVar);

                if (vIndexVar.type == ST.BYTE) {
                    sb.Append($"    LD B, 0 \n");
                    sb.Append($"    LD A, ({Utils.VarNameToAsm(indexVar)}) \n");
                    sb.Append($"    LD C, A \n");
                } else if (vIndexVar.type == ST.WORD) {
                    sb.Append($"    LD BC, ({Utils.VarNameToAsm(indexVar)}) \n");
                } else {
                    throw new Exception("error");
                }

            } else if (!indexConst.IsNullOrEmpty()) {
                // load to BC the index value from const
                sb.Append($"    LD BC, {Literal.GetAsmValue(indexConst)} \n");
            } else {
                throw new Exception("error");
            }

            if (transferType == ST.BYTE) {
                // just nothing
            } else if (transferType == ST.WORD) {
                // shift BC to left for multiply on 2
                sb.Append("    SLA C \n");
                sb.Append("    RL B \n");
            }

            sb.Append("    ADD HL, BC \n");
        }

        public override void CheckOnValid() {
            CheckLoc();

            if (!aUnPtr.IsNullOrEmpty()) {
                // check UnPtr
                if (!fn.IsVariableNameExists(aUnPtr)) {
                    throw new MessageForUserException($"{LogPrefix} has undefined var name '{aUnPtr}'.");
                }

                if (fn.GetVariableWithName(aUnPtr).type != ST.WORD) {
                    throw new MessageForUserException($"{LogPrefix} pointer '{aUnPtr}' must be a {ST.WORD} type.");
                }

                if (!aIndexVar.IsNullOrEmpty() && !fn.IsVariableNameExists(aIndexVar)) {
                    throw new MessageForUserException($"{LogPrefix} has undefined var name '{aIndexVar}'.");
                }

                // check second
                if (!bVar.IsNullOrEmpty()) {
                    if (!fn.IsVariableNameExists(bVar)) {
                        throw new MessageForUserException($"{LogPrefix} has undefined var name '{bVar}'.");
                    }
                } else if (!bConst.IsNullOrEmpty()) {
                    if (Literal.GetTypeOfCast(bConst).IsNullOrEmpty()) {
                        throw new MessageForUserException($"{LogPrefix} has constant without type '{bConst}'. Need (byte/word) type cast.");
                    }
                } else {
                    throw new MessageForUserException($"{LogPrefix} doesn't has a second argument.");
                }
            } else if (!aPtr.IsNullOrEmpty()) {
                // check aVar
                if (!fn.IsVariableNameExists(aPtr)) {
                    throw new MessageForUserException($"{LogPrefix} has undefined var name '{aPtr}'.");
                }

                if (fn.GetVariableWithName(aPtr).type != ST.WORD) {
                    throw new MessageForUserException($"{LogPrefix} pointer '{aPtr}' must be a {ST.WORD} type.");
                }

                // check second
                if (!bAddrOf.IsNullOrEmpty()) {
                    if (!fn.IsVariableNameExists(bAddrOf)) {
                        throw new MessageForUserException($"{LogPrefix} has undefined var name '{bAddrOf}'.");
                    }
                } else {
                    throw new MessageForUserException($"{LogPrefix} doesn't has a second argument.");
                }
            } else if (!aVar.IsNullOrEmpty()) {
                if (!fn.IsVariableNameExists(aVar)) {
                    throw new MessageForUserException($"{LogPrefix} has undefined var name '{aVar}'.");
                }

                if (!bUnPtr.IsNullOrEmpty()) {
                    if (!fn.IsVariableNameExists(bUnPtr)) {
                        throw new MessageForUserException($"{LogPrefix} has undefined var name '{bUnPtr}'.");
                    }

                    if (fn.GetVariableWithName(bUnPtr).type != ST.WORD) {
                        throw new MessageForUserException($"{LogPrefix} pointer '{bUnPtr}' must be a {ST.WORD} type.");
                    }

                    if (!bIndexVar.IsNullOrEmpty() && !fn.IsVariableNameExists(bIndexVar)) {
                        throw new MessageForUserException($"{LogPrefix} has undefined var name '{bIndexVar}'.");
                    }
                } else {
                    throw new MessageForUserException($"{LogPrefix} doesn't has a second argument.");
                }
        } else {
                throw new MessageForUserException($"{LogPrefix} don't have any 'aup=', 'ap=', 'av='. ");
            }
        }

        public override void CreateVarsForConsts() {

            this.fn.pn.ExchangeConstToVar(ref aIndexConst, ref aIndexVar, this);
            this.fn.pn.ExchangeConstToVar(ref bIndexConst, ref bIndexVar, this);

            this.fn.pn.ExchangeConstToVar(ref bConst, ref bVar, this);

        }
    }
}
