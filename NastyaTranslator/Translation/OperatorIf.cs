﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator.Translation {
    class OperatorIf : AOperatorNode {
        public OperatorIf() {
            this.opName = "if";
        }

        private static int ifNumber = 0;

        public string aVar = null;
        public string aConst = null;

        public string comparisonOperator = null;

        public string bVar = null;
        public string bConst = null;

        public const string EQUAL = "==";
        public const string NOT_EQUAL = "!=";
        public const string LESS = "<";
        public const string GREATER = ">";
        public const string LESS_EQUAL = "<=";
        public const string GREATER_EQUAL = ">=";

        protected static HashSet<string> posibleLogicOps = new HashSet<string>() {
            EQUAL, NOT_EQUAL, LESS, GREATER, LESS_EQUAL, GREATER_EQUAL
        };

        public List<AOperatorNode> ifOperators = new List<AOperatorNode>();

        public List<AOperatorNode> elseOperators = new List<AOperatorNode>();

        protected void GetAsmString_forExpressionChecking(StringBuilder sb, string ifSectionName, string ifNotLabel, OperatorIf ifSection) {

            string aOperand = !string.IsNullOrEmpty(ifSection.aConst) ? ifSection.aConst : !string.IsNullOrEmpty(ifSection.aVar) ? ifSection.aVar : "undefined";
            string bOperand = !string.IsNullOrEmpty(ifSection.bConst) ? ifSection.bConst : !string.IsNullOrEmpty(ifSection.bVar) ? ifSection.bVar : "undefined";
            sb.Append($"                    ; {ifSectionName} ({aOperand} {ifSection.comparisonOperator} {bOperand}) \n");

            // emulate ">" and "<=" by reversing arguments and using reverse logic operator
            bool needArgumentsSwitching = false;
            if (ifSection.comparisonOperator == GREATER) {
                ifSection.comparisonOperator = LESS;
                needArgumentsSwitching = true;
            } else if (ifSection.comparisonOperator == LESS_EQUAL) {
                ifSection.comparisonOperator = GREATER_EQUAL;
                needArgumentsSwitching = true;
            }
            if (needArgumentsSwitching) {
                string a_tmp;
                a_tmp = ifSection.aConst;
                ifSection.aConst = ifSection.bConst;
                ifSection.bConst = a_tmp;

                a_tmp = ifSection.aVar;
                ifSection.aVar = ifSection.bVar;
                ifSection.bVar = a_tmp;
            }

            string vaType = !string.IsNullOrEmpty(ifSection.aVar) ? fn.GetVariableWithName(ifSection.aVar).type : null;
            string vbType = !string.IsNullOrEmpty(ifSection.bVar) ? fn.GetVariableWithName(ifSection.bVar).type : null;
            string caType = !string.IsNullOrEmpty(ifSection.aConst) ? Literal.GetTypeForConstStorage(ifSection.aConst) : null;
            string cbType = !string.IsNullOrEmpty(ifSection.bConst) ? Literal.GetTypeForConstStorage(ifSection.bConst) : null;
            string aType = !vaType.IsNullOrEmpty() ? vaType : caType;
            string bType = !vbType.IsNullOrEmpty() ? vbType : cbType;

            bool isByte = aType == ST.BYTE && bType == ST.BYTE;
            bool isWord = !isByte;

            if (!isByte && !isWord)
                isWord = true;

            // LOAD operands and COMPARE
            if (isByte) {
                // B
                if (!string.IsNullOrEmpty(ifSection.bConst))
                    sb.Append($"    LD B, {Literal.GetAsmValue(ifSection.bConst)} \n");
                else if (!string.IsNullOrEmpty(ifSection.bVar)) {
                    sb.Append($"    LD A, ({Utils.VarNameToAsm(ifSection.bVar)}) \n");
                    sb.Append($"    LD B, A \n");
                } else
                    throw new Exception("assembling  error");

                // A
                if (!string.IsNullOrEmpty(ifSection.aConst))
                    sb.Append($"    LD A, {Literal.GetAsmValue(ifSection.aConst)} \n");
                else if (!string.IsNullOrEmpty(ifSection.aVar))
                    sb.Append($"    LD A, ({Utils.VarNameToAsm(ifSection.aVar)}) \n");
                else
                    throw new Exception("assembling  error");

                // comparison operation
                sb.Append($"    CP B \n");
            } else if (isWord) {
                // A

                if (!string.IsNullOrEmpty(ifSection.aConst)) {
                    sb.Append($"    LD HL, {Literal.GetAsmValue(ifSection.aConst)} \n");
                } else if (!string.IsNullOrEmpty(ifSection.aVar)) {
                    if (vaType == ST.BYTE) {
                        sb.Append($"    LD HL, ({Utils.VarNameToAsm(ifSection.aVar)}) \n"); // load 2 bytes
                        sb.Append($"    LD H, 0 \n"); // but only lower will be used
                    } else if (vaType == ST.WORD) {
                        sb.Append($"    LD HL, ({Utils.VarNameToAsm(ifSection.aVar)}) \n");
                    } else {
                        throw new Exception("assembling  error");
                    }
                } else {
                    throw new Exception("assembling  error");
                }

                // B
                if (!string.IsNullOrEmpty(ifSection.bConst)) { 
                    sb.Append($"    LD BC, {Literal.GetAsmValue(ifSection.bConst)} \n");
                } else if (!string.IsNullOrEmpty(ifSection.bVar)) {
                    if (vbType == ST.BYTE) {
                        sb.Append($"    LD BC, ({Utils.VarNameToAsm(ifSection.bVar)}) \n"); // load 2 bytes
                        sb.Append($"    LD B, 0 \n"); // but only lower will be used
                    } else if (vbType == ST.WORD) {
                        sb.Append($"    LD BC, ({Utils.VarNameToAsm(ifSection.bVar)}) \n");
                    } else {
                        throw new Exception("assembling  error");
                    }
                } else {
                    throw new Exception("assembling  error");
                }    

                // comparison operation
                sb.Append($"    AND A \n");
                sb.Append($"    SBC HL, BC \n");
            } else {
                throw new Exception("assembling  error");
            }

            // jump
            if (ifSection.comparisonOperator == EQUAL) {
                sb.Append($"    JP NZ, {ifNotLabel} \n");
            } else if (ifSection.comparisonOperator == NOT_EQUAL) {
                sb.Append($"    JP Z, {ifNotLabel} \n");
            } else if (ifSection.comparisonOperator == LESS) {
                sb.Append($"    JP NC, {ifNotLabel} \n");
            } else if (ifSection.comparisonOperator == GREATER_EQUAL) {
                sb.Append($"    JP C, {ifNotLabel} \n");
            }
        }

        public override void TranslateToAsm(StringBuilder sb) {
            // LABELS
            string ifLabelPattern = $"if{ifNumber++}";
            // "not true value of expression"  label
            string ifNotLabel = ifLabelPattern + $"_not";
            string ifEndLabel = ifLabelPattern +  "_end";

            GetAsmString_forExpressionChecking(sb, "if", ifNotLabel, this);

            foreach (AOperatorNode op in ifOperators) {
                op.TranslateToAsm(sb);
            }

            if (elseOperators.Count > 0)
                sb.Append($"    JP {ifEndLabel} \n");

            sb.Append(ifNotLabel + ": \n");

            if (elseOperators.Count == 0) {
                return;
            }

            OperatorIf currSection = this;

            // ONE or NO "else" section at the end
            if (elseOperators.Count > 0) {
                sb.Append($"                    ; else \n");
                foreach (AOperatorNode op in elseOperators) {
                    op.TranslateToAsm(sb);
                }
            }

            sb.Append(ifEndLabel + ": \n");
        }

        protected void GetBcString_forExpressionChecking(StringBuilder sb, InterpretatorBuilder ib, string ifSectionName, string ifNotLabel, OperatorIf ifSection) {
            string aOperand = !string.IsNullOrEmpty(ifSection.aConst) ? ifSection.aConst : !string.IsNullOrEmpty(ifSection.aVar) ? ifSection.aVar : "undefined";
            string bOperand = !string.IsNullOrEmpty(ifSection.bConst) ? ifSection.bConst : !string.IsNullOrEmpty(ifSection.bVar) ? ifSection.bVar : "undefined";
            sb.Append($"                    ; {ifSectionName} ({aOperand} {ifSection.comparisonOperator} {bOperand}) \n");

            // emulate ">" and "<=" by reversing arguments and using reverse logic operator
            bool needArgumentsSwitching = false;
            if (ifSection.comparisonOperator == GREATER) {
                ifSection.comparisonOperator = LESS;
                needArgumentsSwitching = true;
            } else if (ifSection.comparisonOperator == LESS_EQUAL) {
                ifSection.comparisonOperator = GREATER_EQUAL;
                needArgumentsSwitching = true;
            }
            if (needArgumentsSwitching) {
                string a_tmp = ifSection.aVar;
                ifSection.aVar = ifSection.bVar;
                ifSection.bVar = a_tmp;
            }

            byte argA = fn.pn.GetVariableWithName(ifSection.aVar).idForByteCode;
            byte argB = fn.pn.GetVariableWithName(ifSection.bVar).idForByteCode;

            string opCode = null;

            if (ifSection.comparisonOperator == EQUAL) {
                ib.Registrate(PBCI.inter_opIf_Equal_AB);
                opCode = PBCI.inter_opIf_Equal_AB.opCode;
            } else if (ifSection.comparisonOperator == NOT_EQUAL) {
                ib.Registrate(PBCI.inter_opIf_NotEqual_AB);
                opCode = PBCI.inter_opIf_NotEqual_AB.opCode;
            } else if (ifSection.comparisonOperator == LESS) {
                ib.Registrate(PBCI.inter_opIf_Less_AB);
                opCode = PBCI.inter_opIf_Less_AB.opCode;
            } else if (ifSection.comparisonOperator == GREATER_EQUAL) {
                ib.Registrate(PBCI.inter_opIf_GreaterEqual_AB);
                opCode = PBCI.inter_opIf_GreaterEqual_AB.opCode;
            } else {
                throw new Exception("assembling  error");
            }

            sb.Append($"    DEFB {opCode}, {argA}, {argB}\n");
            sb.Append($"    DEFW {ifNotLabel}\n");
        }

        public override void TranslateToBc(StringBuilder sb, InterpretatorBuilder ib) {
            // LABELS
            string ifLabelPattern = $"if{ifNumber++}";
            // "not true value of expression"  label
            string ifNotLabel = ifLabelPattern + $"_not";
            string ifEndLabel = ifLabelPattern + "_end";

            string opJmpW = PBCI.inter_opJmpW.opCode;

            GetBcString_forExpressionChecking(sb, ib, "if", ifNotLabel, this);

            foreach (AOperatorNode op in ifOperators) {
                op.TranslateToBc(sb, ib);
            }

            if (elseOperators.Count > 0) {
                sb.Append($"    DEFB {opJmpW}\n");
                sb.Append($"    DEFW {ifEndLabel}\n");
            }

            sb.Append(ifNotLabel + ": \n");

            if (elseOperators.Count == 0) {
                return;
            }

            OperatorIf currSection = this;

            // ONE or NO "else" section at the end
            if (elseOperators.Count > 0) {
                sb.Append($"                    ; else \n");
                foreach (AOperatorNode op in elseOperators) {
                    op.TranslateToBc(sb, ib);
                }
            }

            sb.Append(ifEndLabel + ": \n");
        }

        public override void Init(FuncNode fn) {
            base.Init(fn);

            foreach (var on in ifOperators)
                on.Init(fn);

            foreach (var on in elseOperators)
                on.Init(fn);
        }

        public override void CheckOnValid() {
            CheckLoc();

            if (string.IsNullOrEmpty(aVar) && string.IsNullOrEmpty(aConst)) {
                throw new MessageForUserException($"{LogPrefix} doesn't has a first argument.");
            }
            if (string.IsNullOrEmpty(bVar) && string.IsNullOrEmpty(bConst)) {
                throw new MessageForUserException($"{LogPrefix} doesn't has a second argument.");
            }
            if (!string.IsNullOrEmpty(aVar) && !fn.IsVariableNameExists(aVar)) {
                throw new MessageForUserException($"{LogPrefix} first argument '{aVar}' is not defined.");
            }
            if (!string.IsNullOrEmpty(bVar) && !fn.IsVariableNameExists(bVar)) {
                throw new MessageForUserException($"{LogPrefix} second argument '{bVar}' is not defined.");
            }

            if (string.IsNullOrEmpty(comparisonOperator)) {
                throw new MessageForUserException($"{LogPrefix} doesn't has a logic operator.");
            } else {
                comparisonOperator = comparisonOperator.Replace('{', '<');
                comparisonOperator = comparisonOperator.Replace('}', '>');

                if (!posibleLogicOps.Contains(comparisonOperator)) {
                    throw new MessageForUserException($"{LogPrefix} has illegal logic operator '{comparisonOperator}'.");
                }
            }

            // check inside
            foreach (var op in ifOperators) {
                op.CheckOnValid();
            }

            // check next section
            foreach (var op in elseOperators) {
                op.CheckOnValid();
            }
        }

        public override void CreateVarsForConsts() {
            this.fn.pn.ExchangeConstToVar(ref aConst, ref aVar, this);
            this.fn.pn.ExchangeConstToVar(ref bConst, ref bVar, this);

            // create inside
            foreach (var op in ifOperators) {
                op.CreateVarsForConsts();
            }

            // create next section
            foreach (var op in elseOperators) {
                op.CreateVarsForConsts();
            }
        }
    }
}
