﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator.Translation {
    abstract class AOperatorNode : ANode {
        public string opName = null;

        protected FuncNode fn = null;

        public override string LogPrefix => $"{base.LogPrefix} '{opName}'";


        public virtual void Init(FuncNode fn) {
            this.fn = fn;
        }
    }
}
