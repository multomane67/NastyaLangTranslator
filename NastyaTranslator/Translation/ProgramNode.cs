﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

namespace NastyaTranslator.Translation {


    class ProgramNode : ANode {
        private const string mainFunctionName = "Main";

        public int maxNumberOfVars = 240; // (256-16) can be changed by attribute of namespace [MaxNumberOfVars(newNumber)]
        public const string ADDR_GV_BYTE_TABLE = "addr_gv_byte_table";
        public const string ADDR_GV_WORD_TABLE = "addr_gv_word_table";


        public List<Variable> globalVars = new List<Variable>();
        public List<FuncNode> functions = new List<FuncNode>();

        public virtual void Init() {
            foreach (Variable gv in globalVars)
                gv.Init(this, null);

            foreach (FuncNode fn in functions)
                fn.Init(this);
        }

        public Variable GetVariableWithName(string varName) {
            foreach (var var in globalVars) {
                if (var.name == varName) {
                    return var;
                }
            }

            return null;
        }

        public FuncNode GetFunctionWithName(string funcName) {
            foreach (var fn in functions) {
                if (fn.name == funcName)
                    return fn;
            }

            return null;
        }

        public bool IsFunctionNameExists(string funcName) {
            return GetFunctionWithName(funcName) != null;
        }

        public bool IsVariableNameExists(string funcName) {
            return GetVariableWithName(funcName) != null;
        }

        public override void CheckOnValid() {
            foreach (var var in globalVars) {
                var.CheckOnValid();
            }

            foreach (var func in functions) {
                func.CheckOnValid();
            }

            FuncNode main = GetFunctionWithName(mainFunctionName);
            if (main == null) {
                throw new MessageForUserException($"no '{mainFunctionName}' function");
            }
        }

        #region Translation into assembler as is

        // translate like node by node args
        public override void TranslateToAsm(StringBuilder sb) {
            sb.Append("    ORG 32768\n");

            foreach (var f in functions) {
                f.TranslateToAsm(sb);
            }

            FuncNode main = GetFunctionWithName(mainFunctionName);

            sb.Append("\n");

            sb.Append("global_vars:\n");

            int labMaxLen = 0;
            int valMaxLen = 0;

            foreach (var gv in globalVars) {
                gv.GetAlignLengths(out int labVarLen, out int valVarLen);
                labMaxLen = Math.Max(labMaxLen, labVarLen);
                valMaxLen = Math.Max(valMaxLen, valVarLen);
            }

            foreach (var gv in globalVars) {
                gv.GetAsmDefinisionStr(sb, labMaxLen, valMaxLen);
            }

            sb.Append("\n    end " + Utils.FuncNameToAsm(main.name) + "\n");
        }

        // run translation from this func
        public void TranslateToFile_Assembler(string fileName) {

            StringBuilder sb = new StringBuilder();

            TranslateToAsm(sb);

            File.WriteAllText(fileName, sb.ToString());
        }

        #endregion Translation into assembler as is


        #region Translation into ByteCode

        // translate like node by node args
        public override void TranslateToBc(StringBuilder sb, InterpretatorBuilder ib) {
            FuncNode main = GetFunctionWithName(mainFunctionName);

            sb.Append("curr_exec_addr:\n");
            sb.Append($"    DEFW {Utils.FuncNameToAsm(main.name)}\n");
            sb.Append("\n");
            sb.Append("bc_program:\n");
            OperatorCall.CallDict_PreProcess(ib);
            foreach (var f in functions) {
                f.TranslateToBc(sb, ib);
                sb.Append("\n");
            }
            OperatorCall.CallDict_PostProcess(sb, ib);
        }

        // run translation from this func
        public void TranslateToFile_ByteCode(string fileName) {



            // ========================================================
            // preparing vars and make id for each
            // ========================================================

            CreateVarsForConsts();

            // sort the vars for create table id's
            // Sort() is good variant but little bit - out of controll
            List<Variable> sorted = new List<Variable>(globalVars.Count);
            foreach (Variable gv in globalVars) {
                if (gv.type == ST.BYTE) {
                    sorted.Add(gv);
                }
            }
            foreach (Variable gv in globalVars) {
                if (gv.type == ST.WORD) {
                    sorted.Add(gv);
                }
            }
            globalVars = sorted;
            int varId = 0;
            foreach (Variable v in globalVars) {
                if (varId < maxNumberOfVars) {
                    v.idForByteCode = (byte)varId;
                    varId++;
                } else {
                    throw new Exception($"You use to many global vars: {globalVars.Count}, maximum is: {maxNumberOfVars}");
                }
            }

            // ========================================================
            // write to buffer program in byte code and make interpretator
            // ========================================================
            InterpretatorBuilder ib = new InterpretatorBuilder();
            StringBuilder sbProgram = new StringBuilder();
            TranslateToBc(sbProgram, ib);

            // ========================================================
            // write to buffer variable-table (bytes and words) + arrays
            // ========================================================
            int labMaxLen = 0;
            int valMaxLen = 0;

            foreach (Variable gv in globalVars) {
                gv.GetAlignLengths(out int labLen, out int valLen);
                labMaxLen = Math.Max(labMaxLen, labLen);
                valMaxLen = Math.Max(valMaxLen, valLen);
            }

            StringBuilder sbVars = new StringBuilder();
            StringBuilder sbArrays = new StringBuilder();
            sbVars.Append("\n");
            sbVars.Append($"{ADDR_GV_BYTE_TABLE}:\n");
            foreach (Variable gv in globalVars) {
                if (gv.type == ST.BYTE)
                    gv.GetBcDefinitionStr(sbVars, sbArrays, labMaxLen, valMaxLen);
            }
            sbVars.Append("\n");
            sbVars.Append($"{ADDR_GV_WORD_TABLE}:\n");
            foreach (Variable gv in globalVars) {
                if (gv.type == ST.WORD)
                    gv.GetBcDefinitionStr(sbVars, sbArrays, labMaxLen, valMaxLen);
            }


            // ========================================================
            // building program from buffers
            // ========================================================
            StringBuilder sb = new StringBuilder();
            sb.Append("    ORG 32768\n");
            sb.Append("\n");

            ib.WriteAllTextTo(sb);
            sb.Append("\n");

            sb.Append(sbProgram);
            sb.Append("\n");

            sb.Append(sbVars);
            sb.Append("\n");
            sb.Append(sbArrays);
            sb.Append("\n");

            sb.Append("bc_callstack_top:\n");
            sb.Append("DEFW bc_callstack\n");
            sb.Append("\n");
            sb.Append("bc_callstack:\n");

            sb.Append("\n    end " + PBCI.N_inter_main + "\n");
            File.WriteAllText(fileName, sb.ToString());
        }


        public override void CreateVarsForConsts() {
            // make consts in operator like a global vars
            foreach (FuncNode fn in functions) {
                foreach (AOperatorNode op in fn.operators) {
                    op.CreateVarsForConsts();
                }
            }
        }

        public void ExchangeConstToVar(ref string constVal, ref string varName, AOperatorNode currOperator) {
            if (!constVal.IsNullOrEmpty()) {

                string definedType = Literal.GetTypeOfCast(constVal); // can return null - in this case type will be selected by value for auto const

                string constValForParse = Literal.GetAsmValue(constVal);

                if (Int32.TryParse(constValForParse, out int decVal)) {
                    Variable autoConst = RegistrateAutoConst_ByDecimal(decVal, definedType);

                    constVal = null;

                    if (varName.IsNullOrEmpty()) {
                        varName = autoConst.name;
                    } else {
                        throw new Exception("varName is already not null or emty - this is strange, setting auto const was interrupted!");
                    }

                } else {
                    throw new Exception($"Can't parse literal in some '{currOperator.opName}' operator!");
                }
            }
        }

        private Variable RegistrateAutoConst_ByDecimal(int val, string useThisType = null) {
            string type = null;
            string typeSymbol = null;

            // minimal posible type
            string minType = ST.GetMinimumTypeForStorage(val);

            // force type or minimal posible
            if (useThisType.IsNullOrEmpty()) {
                type = minType;
            } else {
                bool usedTypeSmallerThanMinimal = useThisType == ST.BYTE && minType == ST.WORD;

                if (usedTypeSmallerThanMinimal) {
                    throw new Exception("Used  type smaller than minimal");
                } else {
                    type = useThisType;
                }
            }

            // selected type symbol for name
            if (type == ST.BYTE) {
                typeSymbol = "b";
            } else if (type == ST.WORD) {
                typeSymbol = "w";
            } else {
                throw new Exception("error");
            }


            string name = $"ac{typeSymbol}_{val}";

            Variable autoConst = globalVars.Find((gv) => gv.name == name);

            if (autoConst == null) {
                autoConst = new Variable() {
                    type = type,
                    name = name,
                    defValue = val.ToString()
                };

                globalVars.Add(autoConst);
            }

            return autoConst;
        }

        #endregion Translation into ByteCode


    }
}
