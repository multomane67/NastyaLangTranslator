﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator.Translation {
    abstract class ANode {
        public LocationInFile lif;

        public abstract void TranslateToAsm(StringBuilder sb);
        public abstract void TranslateToBc(StringBuilder sb, InterpretatorBuilder ib);

        public abstract void CheckOnValid();
        public abstract void CreateVarsForConsts();

        public virtual string LogPrefix => lif.ToString();

        protected void CheckLoc() {
            if (lif.IsHaveDefaultValue)
                throw new Exception($"Some '{GetType()}' don't have info about location in file.");
        }
    }
}
