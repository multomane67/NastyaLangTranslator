﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator.Translation {
    class OperatorCalcBinary : AOperatorNode {
        public OperatorCalcBinary(OperatorCalc ext) {
            this.opName = "calc_binary";

            string binOp = ext.operation + "=";

            if (ext.rVar == ext.aVar) {
                this.aVar = ext.aVar;
                this.operation = binOp;
                this.bVar = ext.bVar;
                this.bConst = ext.bConst;
            } else if (ext.rVar == ext.bVar) {
                this.aVar = ext.bVar;
                this.operation = binOp;
                this.bVar = ext.aVar;
                this.bConst = ext.aConst;
            }
            else 
            {
                throw new Exception("assembling error");
            }
        }

        public string aVar = null;

        public string operation = null;

        public string bVar = null;
        public string bConst = null;


        private int logicShiftNumber {
            get => OperatorCalc.logicShiftNumber;
            set => OperatorCalc.logicShiftNumber = value;
        }

        public override void TranslateToAsm(StringBuilder sb) {
            string aOperand = !string.IsNullOrEmpty(aVar) ? aVar : "undefined";
            string bOperand = !string.IsNullOrEmpty(bConst) ? bConst : !string.IsNullOrEmpty(bVar) ? bVar : "undefined";
            sb.Append($"                    ; {opName} {aOperand} {operation} {bOperand} \n");

            string vaType = !string.IsNullOrEmpty(aVar) ? fn.GetVariableWithName(aVar).type : null;
            string vbType = !string.IsNullOrEmpty(bVar) ? fn.GetVariableWithName(bVar).type : null;
            string cbType = !string.IsNullOrEmpty(bConst) ? Literal.GetTypeForConstStorage(bConst) : null;

            string aType = vaType;
            string bType = !vbType.IsNullOrEmpty() ? vbType : cbType;

            bool isByte = aType == ST.BYTE && bType == ST.BYTE;
            bool isWord = !isByte;

            bool isLogicShift = operation == "<<=" || operation == ">>=";

            if (isLogicShift) {
                string logicShiftLabel_repeat = $"logicShift{logicShiftNumber}_repeat";
                string logicShiftLabel_skipIfZero = $"logicShift{logicShiftNumber}_skipIfZero";
                logicShiftNumber++;

                // load cycle counter in B
                if (!bVar.IsNullOrEmpty()) {
                    // equal for both var types and B
                    if (vbType == ST.BYTE) {
                        sb.Append($"    LD A, ({Utils.VarNameToAsm(bVar)})\n");
                        sb.Append($"    LD B, A\n");
                    } else if (vbType == ST.WORD) {
                        sb.Append($"    LD A, ({Utils.VarNameToAsm(bVar)})\n");
                        sb.Append($"    LD B, A\n"); // will be truncated to lower byte, (lower is first, second will be ignored)
                    } else {
                        throw new Exception("error");
                    }
                } else if (!bConst.IsNullOrEmpty()) {
                    string cbAsmVal = Literal.GetAsmValue(bConst);

                    if (Int64.Parse(cbAsmVal) == 0f) {
                        Console.WriteLine($"WARNING: some calc try to make logic shift with zero const counter!");
                    }

                    if (cbType == ST.BYTE) {
                        sb.Append($"    LD B, {cbAsmVal}\n");
                    } else if (cbType == ST.WORD) {
                        sb.Append($"    LD HL, {cbAsmVal}\n");
                        sb.Append($"    LD B, L\n");
                    } else {
                        throw new Exception("error");
                    }
                } else {
                    throw new Exception("error");
                }

                // load operable in DE
                if (!aVar.IsNullOrEmpty()) {
                    if (vaType == ST.BYTE) {
                        sb.Append($"    LD DE, ({Utils.VarNameToAsm(aVar)})\n"); // load 2 bytes from one-byte-variable
                        sb.Append($"    LD D, 0\n"); // but high byte will be ignored
                    } else if (vaType == ST.WORD) {
                        sb.Append($"    LD DE, ({Utils.VarNameToAsm(aVar)})\n");
                    } else {
                        throw new Exception("error");
                    }
                } else {
                    throw new Exception("error");
                }

                sb.Append($"    LD A, 0\n");
                sb.Append($"    CP B\n");
                sb.Append($"    JR Z, {logicShiftLabel_skipIfZero}\n");

                sb.Append($"{logicShiftLabel_repeat}:\n");

                if (operation == "<<=") {
                    sb.Append($"    SLA E\n");
                    sb.Append($"    RL D\n");
                    sb.Append($"    DJNZ {logicShiftLabel_repeat}\n");
                } else if (operation == ">>=") {
                    sb.Append($"    SRL D\n");
                    sb.Append($"    RR E\n");
                    sb.Append($"    DJNZ {logicShiftLabel_repeat}\n");
                } else {
                    throw new Exception("operation select error");
                }

                sb.Append($"{logicShiftLabel_skipIfZero}:\n");

                // unload bits of result to result var
                if (aType == ST.BYTE) {
                    sb.Append($"    LD A, E\n");
                    sb.Append($"    LD ({Utils.VarNameToAsm(aVar)}), A\n");
                } else if (aType == ST.WORD) {
                    sb.Append($"    LD ({Utils.VarNameToAsm(aVar)}), DE\n");
                } else {
                    throw new Exception("error");
                }

            } else {
                if (isByte) {
                    // BYTEs
                    if (!string.IsNullOrEmpty(bConst)) {
                        sb.Append($"    LD A, {Literal.GetAsmValue(bConst)} \n");
                    } else if (!string.IsNullOrEmpty(bVar)) {
                        sb.Append($"    LD A, ({Utils.VarNameToAsm(bVar)}) \n");
                    } else {
                        throw new Exception("assembling  error");
                    }
                    sb.Append($"    LD B, A \n");

                    if (!string.IsNullOrEmpty(aVar)) {
                        sb.Append($"    LD A, ({Utils.VarNameToAsm(aVar)}) \n");
                    } else {
                        throw new Exception("assembling  error");
                    }

                    if (!string.IsNullOrEmpty(operation)) {
                        if (operation == "+=") {
                            sb.Append($"    ADD A, B \n");
                        } else if (operation == "-=") {
                            sb.Append($"    SUB B \n");
                        } else {
                            throw new Exception("assembling  error");
                        }
                    } else {
                        throw new Exception("assembling  error");
                    }


                    sb.Append($"    LD ({Utils.VarNameToAsm(aVar)}), A \n");

                } else if (isWord) {
                    // WORDs
                    if (!string.IsNullOrEmpty(aVar)) {
                        if (vaType == ST.BYTE) {
                            sb.Append($"    LD HL, ({Utils.VarNameToAsm(aVar)}) \n"); // load 2 bytes
                            sb.Append($"    LD H, 0 \n"); // but only lower will be used.
                        } else if (vaType == ST.WORD) {
                            sb.Append($"    LD HL, ({Utils.VarNameToAsm(aVar)}) \n");
                        } else {
                            throw new Exception("assembling  error");
                        }
                    } else {
                        throw new Exception("assembling  error");
                    }

                    if (!string.IsNullOrEmpty(bConst)) {
                        sb.Append($"    LD BC, {Literal.GetAsmValue(bConst)} \n");
                    } else if (!string.IsNullOrEmpty(bVar)) {
                        if (vbType == ST.BYTE) {
                            sb.Append($"    LD BC, ({Utils.VarNameToAsm(bVar)}) \n"); // load 2 bytes
                            sb.Append($"    LD B, 0 \n"); // but only lower will be used.
                        } else if (vbType == ST.WORD) {
                            sb.Append($"    LD BC, ({Utils.VarNameToAsm(bVar)}) \n");
                        } else {
                            throw new Exception("assembling  error");
                        }
                    } else {
                        throw new Exception("assembling  error");
                    }

                    if (!string.IsNullOrEmpty(operation)) {
                        if (operation == "+=") {
                            sb.Append($"    ADD HL, BC \n");
                        } else if (operation == "-=") {
                            sb.Append($"    AND A \n");
                            sb.Append($"    SBC HL, BC \n");
                        } else {
                            throw new Exception("assembling  error");
                        }
                    } else {
                        throw new Exception("assembling  error");
                    }


                    if (aType == ST.BYTE) {
                        sb.Append($"    LD A, L \n");
                        sb.Append($"    LD ({Utils.VarNameToAsm(aVar)}), A \n");
                    } else if (aType == ST.WORD) {
                        sb.Append($"    LD ({Utils.VarNameToAsm(aVar)}), HL \n");
                    } else {
                        throw new Exception("assembling  error");
                    }

                } else {
                    throw new Exception($"assembling  error");
                }
            }
        }

        public override void TranslateToBc(StringBuilder sb, InterpretatorBuilder ib) {
            string aOperand = !string.IsNullOrEmpty(aVar) ? aVar : "undefined";
            string bOperand = !string.IsNullOrEmpty(bConst) ? bConst : !string.IsNullOrEmpty(bVar) ? bVar : "undefined";
            sb.Append($"                    ; {opName} {aOperand} {operation} {bOperand} \n");

            string opCode = null;

            byte argA = fn.pn.GetVariableWithName(aVar).idForByteCode;
            byte argB = fn.pn.GetVariableWithName(bVar).idForByteCode;

            AsmProc asmProc = null;

            if (operation == "+=") {
                asmProc = PBCI.inter_opCalc_PlusEqual_AB;
            } else if (operation == "-=") {
                asmProc = PBCI.inter_opCalc_MinusEqual_AB;
            } else if (operation == "<<=") {
                asmProc = PBCI.inter_opCalc_SHL_AB;
            } else if (operation == ">>=") {
                asmProc = PBCI.inter_opCalc_SHR_AB;
            } else {
                throw new Exception("assembling  error");
            }

            ib.Registrate(asmProc);
            opCode = asmProc.opCode;

            sb.Append($"    DEFB {opCode}, {argA}, {argB}\n");
        }

        public override void CreateVarsForConsts() {
            this.fn.pn.ExchangeConstToVar(ref bConst, ref bVar, this);
        }

        public override void CheckOnValid() {
            throw new Exception("don't check OperatorCalcRAB!");
        }

    }
}
