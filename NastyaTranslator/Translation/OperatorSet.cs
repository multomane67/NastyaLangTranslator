﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator.Translation {
    class OperatorSet : AOperatorNode {
        public OperatorSet() {
            this.opName = "set";
        }

        public string aVar = null;
        public string bConst = null;
        public string bVar = null;

        public override void TranslateToAsm(StringBuilder sb) {
            string commentaryVal = !string.IsNullOrEmpty(bConst) ? bConst : !string.IsNullOrEmpty(bVar) ? bVar : "undefined";
            sb.Append($"                    ; set {aVar} = {commentaryVal}\n");

            Variable vaVar = fn.GetVariableWithName(aVar);

            if (vaVar.type == ST.BYTE) {
                if (!string.IsNullOrEmpty(bConst)) {
                        sb.Append($"    LD A, {Literal.GetAsmValue(bConst)} \n");
                } else if (!string.IsNullOrEmpty(bVar)) {
                    Variable vbVar = fn.GetVariableWithName(bVar);
                    if (vbVar.type == ST.BYTE) {
                        sb.Append($"    LD A, ({Utils.VarNameToAsm(bVar)}) \n");
                    } else if (vbVar.type == ST.WORD) {
                        sb.Append($"    LD A, ({Utils.VarNameToAsm(bVar)}) \n");
                        Console.Write($"Warning destination var '{aVar}' will take the cutted to byte value of '{bVar}' \n");
                    }
                } else {
                    throw new Exception("assembling  error");
                }
                        sb.Append($"    LD ({Utils.VarNameToAsm(aVar)}), A \n");

            } else if (vaVar.type == ST.WORD) {
                if (!string.IsNullOrEmpty(bConst)) {
                        sb.Append($"    LD BC, {Literal.GetAsmValue(bConst)} \n");
                } else if (!string.IsNullOrEmpty(bVar)) {
                    Variable vbVar = fn.GetVariableWithName(bVar);
                    if (vbVar.type == ST.BYTE) {
                        sb.Append($"    LD B, 0 \n");
                        sb.Append($"    LD A, ({Utils.VarNameToAsm(bVar)}) \n");
                        sb.Append($"    LD C, A \n");
                    } else if (vbVar.type == ST.WORD) {
                        sb.Append($"    LD BC, ({Utils.VarNameToAsm(bVar)}) \n");
                    }
                } else {
                    throw new Exception("assembling  error");
                }
                        sb.Append($"    LD ({Utils.VarNameToAsm(aVar)}), BC \n");
            }
        }

        public override void TranslateToBc(StringBuilder sb, InterpretatorBuilder ib) {
            string commentaryVal = !string.IsNullOrEmpty(bConst) ? bConst : !string.IsNullOrEmpty(bVar) ? bVar : "undefined";
            sb.Append($"                    ; set {aVar} = {commentaryVal}\n");

            ib.Registrate(PBCI.inter_opSet_AR);
            string opCode = PBCI.inter_opSet_AR.opCode;


            sb.Append($"    DEFB {opCode}, {fn.pn.GetVariableWithName(bVar).idForByteCode}, {fn.pn.GetVariableWithName(aVar).idForByteCode}\n");
        }





        public override void CheckOnValid() {
            CheckLoc();

            if (string.IsNullOrEmpty(aVar)) {
                throw new MessageForUserException($"{LogPrefix} doesn't has a name of destination var.");
            } else if (!fn.IsVariableNameExists(aVar)) {
                throw new MessageForUserException($"{LogPrefix} has undefined variable '{aVar}'.");
            }

            if (string.IsNullOrEmpty(bConst) && string.IsNullOrEmpty(bVar)) {
                throw new MessageForUserException($"{LogPrefix} doesn't has a source var or const.");
            }

            if (!string.IsNullOrEmpty(bVar) && !fn.IsVariableNameExists(bVar)) {
                throw new MessageForUserException($"{LogPrefix} has undefined variable '{bVar}'.");
            }
        }

        public override void CreateVarsForConsts() {
            this.fn.pn.ExchangeConstToVar(ref bConst, ref bVar, this);
        }
    }
}