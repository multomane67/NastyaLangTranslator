﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator.Translation {
    public class AsmProc {
        public string opCode = null;
        public string name = null;
        public List<string> dependencies = null;
        public string text = null;
    }
}
