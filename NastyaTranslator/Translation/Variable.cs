﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator.Translation {
    class Variable : ANode {
        public enum Visibility {
            Unknown = 0,
            Global, // in all functions in the module
            Local, // in one function
        }

        public override string LogPrefix => $"{base.LogPrefix} 'variable'";

        protected ProgramNode pn = null;
        protected FuncNode fn = null; // will be used for local vars

        public Visibility visibility = Visibility.Unknown; // maybe i can use parent not fn and pn and maybe i don't need visibility!
        public string type;
        public string name;
        public byte idForByteCode;

        public string defValue;

        public StringRepresentation arrChars;
        public string arrValues;
        public string arrType;

        public string arrLen;

        //private static int defLastNum = -1;

        public virtual void Init(ProgramNode pn, FuncNode fn) {
            this.pn = pn;
            this.fn = fn;
        }

        public void GetAsmDefinisionStr(StringBuilder sb, int labelAlign, int valueAlign) {
            StringBuilder arrSb = new StringBuilder();
            GetDefinitionStr(sb, arrSb, labelAlign, valueAlign, false);
            sb.Append(arrSb.ToString());
        }

        public void GetBcDefinitionStr(StringBuilder sb, StringBuilder arrSb, int labelAlign, int valueAlign) {
            GetDefinitionStr(sb, arrSb, labelAlign, valueAlign, true);
        }

        public void GetAlignLengths(out int labLen, out int valLen) {
            labLen = $"{Utils.VarNameToAsm(name)}: ".Length;
            valLen = 0;

            if (!defValue.IsNullOrEmpty()) {
                valLen = defValue.Length;
            } else if (arrChars != null || !arrType.IsNullOrEmpty()) {
                valLen = $"_defArrS_{name}_".Length;
            } else {
                valLen = 1; // hard code for symbol '0' in get asm definition
            }
        }

        public void GetDefinitionStr(StringBuilder sb, StringBuilder arrSb, int labelAlign, int valueAlign, bool writeId) {
            string strLabel = Utils.VarNameToAsm(name) + ": ";
            strLabel = strLabel.AlignBySpacesLeft(labelAlign);
            sb.Append(strLabel);

            string strId = writeId ? $"; id {idForByteCode}" : string.Empty;

            if (type == ST.BYTE) {
                if (!defValue.IsNullOrEmpty()) {
                    string alignedVal = defValue.AlignBySpacesLeft(valueAlign);
                    sb.Append($" DEFB {alignedVal} {strId}\n");
                } else {
                    string alignedZero = "0".AlignBySpacesLeft(valueAlign);
                    sb.Append($" DEFB {alignedZero} {strId}\n");
                }
            } else if (type == ST.WORD) {
                if (!defValue.IsNullOrEmpty()) {
                    string alignedVal = defValue.AlignBySpacesLeft(valueAlign);
                    sb.Append($" DEFW {alignedVal} {strId}\n");
                } else if (arrChars != null) {
                    int buildLen = !arrLen.IsNullOrEmpty() ? int.Parse(arrLen) : arrChars.Length;

                    StringRepresentation tmpArrChars = arrChars.Copy();
                    int needToAdd = buildLen - tmpArrChars.Length;
                    if (needToAdd < 0) throw new Exception("assembling error");
                    for (int i = 0; i < needToAdd; i++) {
                        tmpArrChars.AddCode(0);
                    }
                    //string tmpArrChars = AddZeros(arrChars, buildLen, true);

                    string asmStrDefinition = tmpArrChars.BuildAsmStringDefinition();
                    //tmpArrChars = EscapeSequences.ProcessIntoString(tmpArrChars);
                    //string asmStrDefinition = BuildAsmStringDefinition(tmpArrChars);

                    string alignedVal = $"_defArrS_{name}_".AlignBySpacesLeft(valueAlign);
                    sb.Append($" DEFW {alignedVal} {strId}\n");
                    arrSb.Append($"_defArrS_{name}_: \n");
                    arrSb.Append(asmStrDefinition);
                    arrSb.Append($"_defArrE_{name}_: \n");
                } else if (!arrType.IsNullOrEmpty()) {
                    int buildLen = !arrLen.IsNullOrEmpty() ? int.Parse(arrLen) : GetArrayLen(arrValues);
                    string arrString = AddZeros(arrValues, buildLen, false);

                    string alignedVal = $"_defArrS_{name}_".AlignBySpacesLeft(valueAlign);
                    sb.Append($" DEFW {alignedVal} {strId}\n");
                    arrSb.Append($"_defArrS_{name}_: \n");
                    if (arrType == ST.BYTE) {
                        arrSb.Append($" DEFB {arrString}\n");
                    } else if (arrType == ST.WORD) {
                        arrSb.Append($" DEFW {arrString}\n");
                    }
                    arrSb.Append($"_defArrE_{name}_: \n");
                } else {
                    string alignedZero = "0".AlignBySpacesLeft(valueAlign);
                    sb.Append($" DEFW {alignedZero} {strId}\n");
                }
            }
        }

        private int GetArrayLen(string arr) {
            arr = arr.IsNullOrEmpty() ? "" : arr;

            int len = 0;

            len = 0;
            foreach (char c in arr) {
                if (c == ',')
                    len++;
            }
                
            if (!arr.IsNullOrWhiteSpace())
                len += 1;

            return len;
        }

        private string AddZeros(string arr, int len, bool isString) {
            arr = arr.IsNullOrEmpty() ? "" : arr;

            int currLen = GetArrayLen(arr);

            int needToAdd = len - currLen;

            bool firstWithoutComma = currLen == 0;

            if (needToAdd > 0) {
                StringBuilder sb = new StringBuilder();

                if (!arr.IsNullOrWhiteSpace())
                    sb.Append(arr);

                while (needToAdd > 0) {
                    string addElement = null;
                    if (isString) {
                        addElement = "\\0";
                    } else {
                        if (firstWithoutComma) {
                            firstWithoutComma = false;
                            addElement = "0";
                        } else {
                            addElement = ", 0";
                        }
                    }
                    sb.Append(addElement);
                    needToAdd -= 1;
                }
                arr = sb.ToString();
            } else if (needToAdd == 0) {
                // return arr string as is
            } else {
                throw new Exception("error");
            }

            return arr;
        }



        private string BuildAsmStringDefinition(string symbols) 
        {
            StringBuilder sb = new StringBuilder();
            List<string> list = new List<string>();
            foreach (char c in symbols) 
            {
                if (EscapeSequences.Codes2Sequences.ContainsKey(c)) {
                    if (sb.Length > 0) {
                        list.Add(sb.ToString());
                        sb.Clear();
                    }
                    list.Add(c.ToString());
                } else {
                    sb.Append(c.ToString());
                }
            }

            // add last element
            if (sb.Length > 0) {
                list.Add(sb.ToString());
                sb.Clear();
            }

            foreach (string str in list) {
                if (str.Length == 1 && EscapeSequences.Codes2Sequences.ContainsKey(str[0])) {
                    sb.Append($"DEFB {(int)str[0]}\n");
                } else {
                    sb.Append($"DEFM \"{str}\"\n");
                }
            }

            return sb.ToString();
        }

        // added for inheritance support only
        public override void TranslateToAsm(StringBuilder sb) {
            throw new NotImplementedException();
        }

        public override void TranslateToBc(StringBuilder sb, InterpretatorBuilder ib) {
            throw new NotImplementedException();
        }

        public override void CheckOnValid() {
            CheckLoc();

            if (string.IsNullOrEmpty(type))
                throw new MessageForUserException($"{LogPrefix} doesn't has type.");
            if (string.IsNullOrEmpty(name))
                throw new MessageForUserException($"{LogPrefix} doesn't has name.");

            if (type != ST.BYTE
            && type != ST.WORD)
            {
                throw new MessageForUserException($"{LogPrefix} var '{name}' has illegal type '{type}'.");
            }

            StringBuilder sbUsageLocations = null;

            if (!IsOneInTheList(pn.globalVars, out sbUsageLocations)) {
                throw new MessageForUserException($"Var name '{name}' has multiple declarations! Places:\n{sbUsageLocations.ToString()}");
            }

            if (visibility == Visibility.Local)
            {
                if (!IsOneInTheList(fn.localVars, out sbUsageLocations)) {
                    throw new MessageForUserException($"Var name '{name}' has multiple declarations! Places:\n{sbUsageLocations.ToString()}");
                }
            }

            bool isHaveArrayDef = !arrChars.IsNullOrEmpty() || !arrType.IsNullOrEmpty() || !arrValues.IsNullOrEmpty();
            if (isHaveArrayDef && type != ST.WORD)
            {
                throw new MessageForUserException($"{LogPrefix} error in var '{name}'. Params like: 'arrChars', 'arrType', 'arrValues' can be used with type 'word' only.");
            }

            int defsCnt = 0;

            defsCnt += !defValue.IsNullOrEmpty() ? 1 : 0;
            defsCnt += !arrChars.IsNullOrEmpty() ? 1 : 0;
            defsCnt += !arrValues.IsNullOrEmpty() ? 1 : 0;

            if (defsCnt > 1)
            {
                throw new MessageForUserException($"{LogPrefix} error in var '{name}'. You can use only one of this params: 'defValue', 'arrChars', 'arrValues'.");
            }

            if (isHaveArrayDef && !arrLen.IsNullOrEmpty())
            {
                if (int.TryParse(arrLen, out int arrLenValue) == false)
                {
                    throw new MessageForUserException($"{LogPrefix} error in var '{name}'. Illegal value of arrLen='{arrLen}' can't be parsed like int.");
                }

                int currLen = Math.Max(
                    arrChars != null ? arrChars.Length : -1,
                    arrValues != null ? GetArrayLen(arrValues) : -1
                );

                if (arrLenValue < currLen)
                {
                    throw new MessageForUserException($"{LogPrefix} error in var '{name}'.  arrLenValue is less then current defined array");
                }
            }

            if (isHaveArrayDef && arrLen.IsNullOrEmpty() && arrChars.IsNullOrEmpty() && arrValues.IsNullOrEmpty()) {
                throw new MessageForUserException($"{LogPrefix} error in var '{name}'. Defined like array but with no size and array values.");
            }

            if (!arrChars.IsNullOrEmpty() && arrType != ST.BYTE) {
                throw new MessageForUserException($"{LogPrefix} error in var '{name}'. You can't use arrChars with '{arrType}'. Just '{ST.BYTE}' only.");
            }

            if (isHaveArrayDef && !arrValues.IsNullOrEmpty() && arrType.IsNullOrEmpty()) {
                throw new MessageForUserException($"{LogPrefix} error in var '{name}'. Have no 'arrType'.");
            }

            if (isHaveArrayDef && !arrValues.IsNullOrEmpty() && !arrType.IsNullOrEmpty()) {
                
                ST.Definition td;

                if (!ST.Name2Def.TryGetValue(arrType, out td)) {
                    throw new MessageForUserException($"{LogPrefix} illegal behaviour with determine bounds of values for type '{arrType}'");
                }

                string arr = arrValues.Replace(" ", "");
                string[] arrNumbers = arr.Split(',');

                List<int> indexesWithOutOfBounds = new List<int>();

                for (int i = 0; i < arrNumbers.Length; i++) {
                    string numStr = arrNumbers[i];
                    int numInt = int.Parse(numStr);
                    if (numInt < td.min || numInt > td.max) {
                        indexesWithOutOfBounds.Add(i);
                    }
                }

                if (indexesWithOutOfBounds.Count > 0) {
                    string strListOfInexes = indexesWithOutOfBounds.ToStrList();
                    throw new MessageForUserException($"{LogPrefix} error in var '{name}'. Some elements in array is out of bounds for type '{arrType}' [{td.min}..{td.max}].\n Indexes: {strListOfInexes}.");
                }
            }
        }

        private bool IsOneInTheList(List<Variable> list, out StringBuilder sbUsageLocations) {
            sbUsageLocations = null;
            List<LocationInFile> findedLocations = null;

            findedLocations = (from gv in pn.globalVars
                              where gv.name == name
                              select gv.lif).ToList();

            if (findedLocations.Count > 1) {
                sbUsageLocations = new StringBuilder();

                foreach (LocationInFile lif in findedLocations) {
                    sbUsageLocations.Append($"{lif}\n");
                }

                return false;
            }

            return true;
        }

        public override void CreateVarsForConsts() {
            throw new NotImplementedException();
        }
    }
}
