﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator.Translation {
    class OperatorCall : AOperatorNode {

        public static Dictionary<string, List<int>> callDict = new Dictionary<string, List<int>>();

        public class Call {
            public int indx = 0;
            public string funcName = "";
            public bool isShort = false;
        }

        public static void CallDict_PreProcess(InterpretatorBuilder ib) {
            callDict.Clear();
            ib.SetShortCalls(null);
        }

        public static void CallDict_PostProcess(StringBuilder sb, InterpretatorBuilder ib) {
            if (callDict.Count > 0) {

                int freeCommands = ib.GetFreeCodesCount();

                List<KeyValuePair<string, List<int>>> orderedCallGroups = (
                    from kvp in callDict orderby kvp.Value.Count descending select kvp
                ).ToList();

                List<Call> callList = new List<Call>();
                List<string> shortCallNames = new List<string>();

                int i_group = 0;
                foreach (var kvp in orderedCallGroups) {
                    string funcName = kvp.Key;
                    List<int> occurs = kvp.Value;
                    bool isShort = i_group++ < freeCommands;

                    if (isShort) {
                        shortCallNames.Add(kvp.Key);
                    }

                    foreach (int indx in occurs) {
                        Call p = new Call() {
                            indx = indx,
                            funcName = funcName,
                            isShort = isShort
                        };

                        callList.Add(p);
                    }
                }

                ib.SetShortCalls(shortCallNames);

                callList.Sort((a, b) => a.indx - b.indx); // need 1, 2, 3

                int addedLength = 0;
                foreach (Call call in callList) {
                    var insertSB = new StringBuilder();
                    if (call.isShort) {
                        string opShortCall = ib.GetOpCode_forShortCall(call.funcName);
                        insertSB.Append($"    DEFB {opShortCall}\n");
                    } else {
                        ib.Registrate(PBCI.inter_opCall);
                        insertSB.Append($"    DEFB {PBCI.inter_opCall.opCode}\n");
                        insertSB.Append($"    DEFW {Utils.FuncNameToAsm(call.funcName)}\n");
                    }
                    sb.Insert(call.indx + addedLength, insertSB);
                    addedLength += insertSB.Length;
                }
            }
        }

        public OperatorCall() {
            this.opName = "call";
        }

        public string callFuncName = null;

        public override void CheckOnValid() {
            CheckLoc();

            if (string.IsNullOrEmpty(callFuncName)) {
                throw new MessageForUserException($"{LogPrefix} no function name for call.");
            } else if (!fn.pn.IsFunctionNameExists(callFuncName)) {
                throw new MessageForUserException($"{LogPrefix} function name '{callFuncName}' is not defined.");
            }
        }

        public override void TranslateToAsm(StringBuilder sb) {
            sb.Append($"                    ; call {callFuncName}()\n");
            sb.Append($"    call {Utils.FuncNameToAsm(callFuncName)}\n");
        }

        public override void TranslateToBc(StringBuilder sb, InterpretatorBuilder ib) {
            sb.Append($"                    ; call {callFuncName}()\n");

            int ind = sb.Length;
            sb.Append("\n");

            List<int> occurs = null;
            if (!callDict.TryGetValue(callFuncName, out occurs)) {
                occurs = new List<int>();
                callDict.Add(callFuncName, occurs);
            }

            occurs.Add(ind);
        }

        public override void CreateVarsForConsts() {

        }


    }
}
