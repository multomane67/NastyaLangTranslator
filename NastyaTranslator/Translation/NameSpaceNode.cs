﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator.Translation {
    class NameSpaceNode : ANode {
        public string name;
        public List<AttributeNode> attributes;
        public List<Variable> variables;
        public List<FuncNode> functions;

        // other methods are not implemented now
        public override void CheckOnValid() {
            throw new NotImplementedException();
        }

        public override void CreateVarsForConsts() {
            throw new NotImplementedException();
        }

        public override void TranslateToAsm(StringBuilder sb) {
            throw new NotImplementedException();
        }

        public override void TranslateToBc(StringBuilder sb, InterpretatorBuilder ib) {
            throw new NotImplementedException();
        }
    }
}
