﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator.Translation {
    // maked for support Parsing.Classes.Many just like a container
    // but other methods was realized too
    class ListOfNodes : ANode {

        public List<ANode> nodes = null;

        public virtual void Init(FuncNode fn) {
            Console.WriteLine("Check myself, maybe i am doing something wrong! because this class uses like aggregation container earlier.");
            foreach (ANode node in nodes) {
                var on = node as AOperatorNode;
                on?.Init(fn);
            }
        }

        public override void CheckOnValid() {
            CheckLoc();

            foreach (var node in nodes)
                node.CheckOnValid();
        }

        public override void CreateVarsForConsts() {
            foreach (var node in nodes)
                node.CreateVarsForConsts();
        }

        public override void TranslateToAsm(StringBuilder sb) {
            foreach (var node in nodes)
                node.TranslateToAsm(sb);
        }

        public override void TranslateToBc(StringBuilder sb, InterpretatorBuilder ib) {
            foreach (var node in nodes)
                node.TranslateToBc(sb, ib);
        }
    }
}
