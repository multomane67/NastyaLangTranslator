﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator.Translation {
    class OperatorContinue : AOperatorNode {
        public OperatorContinue() {
            this.opName = "continue";
        }

        public override void CheckOnValid() {
            CheckLoc();

            if (AOperatorCycle.currCycle == null) {
                throw new MessageForUserException($"{LogPrefix} is outside the cycle.");
            }
        }

        public override void TranslateToAsm(StringBuilder sb) {
            sb.Append($"                    ; continue \n");

            sb.Append($"    JP {AOperatorCycle.currContinueLab}\n");
        }

        public override void TranslateToBc(StringBuilder sb, InterpretatorBuilder ib) {
            sb.Append($"                    ; continue \n");

            ib.Registrate(PBCI.inter_opJmpW);
            string opCode = PBCI.inter_opJmpW.opCode;

            sb.Append($"DEFB {opCode}\n");
            sb.Append($"DEFW {AOperatorCycle.currContinueLab}\n");
        }

        public override void CreateVarsForConsts() {

        }
    }
}
