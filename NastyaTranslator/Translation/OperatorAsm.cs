﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator.Translation {
    class OperatorAsm : AOperatorNode {
        // TODO think about verbose for all operator
        public bool verbose = false;

        public OperatorAsm() {
            this.opName = "asm";

            this.startLabel = $"asm{asmNumber}_begin";
            this.endLabel   = $"asm{asmNumber}_end";

            asmNumber++;
        }

        private string startLabel = null;
        private string endLabel = null;

        public static int asmNumber = 0;

        public string asmText = null;

        public override void TranslateToAsm(StringBuilder sb) {
            // TODO: formating asm code - remove old spaces before commands and add new spaces before comands
            if (verbose)
                sb.Append("; ------ asm { ----------------------------------------- \n");

                sb.Append(asmText + "\n");

            if (verbose)
                sb.Append("; ------ } -------------------------------------- END -- \n");
        }

        public override void TranslateToBc(StringBuilder sb, InterpretatorBuilder ib) {
            // TODO: formating asm code - remove old spaces before commands and add new spaces before comands

            sb.Append($"                    ; asm code\n");

            ib.Registrate(PBCI.inter_opAsmW);
            string opCode = PBCI.inter_opAsmW.opCode;

            sb.Append($"    DEFB {opCode}\n");
            sb.Append($"    DEFW {endLabel}\n");

            sb.Append($"{startLabel}:\n"); // is unused now

            if (verbose)
                sb.Append("; ------ asm { ----------------------------------------- \n");

            sb.Append(asmText + "\n");

            if (verbose)
                sb.Append("; ------ } -------------------------------------- END -- \n");

            sb.Append($"    ret\n");

            sb.Append($"{endLabel}:\n");
        }



        public override void CheckOnValid() {
            CheckLoc();
            // TODO: probably - verifing the used vars inside the asm code
        }

        public override void CreateVarsForConsts() {
            // not needed now, consts are not needed here now
        }
    }
}
