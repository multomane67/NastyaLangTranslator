﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Reflection;

namespace NastyaTranslator.Translation {
    // procedures of byte code interpretator - PBCI
    static class PBCI {
        private static Dictionary<string, AsmProc> all = null;

        public static void InitAll() {
            all = new Dictionary<string, AsmProc>();

            AddToList(inter_main);
            AddToList(proc_LD_DE_from_word_table_BC_by_index_A);
            AddToList(proc_LD_A_next_byte_of_prog);
            AddToList(proc_LD_BC_next_word_of_prog);
            AddToList(proc_CALL_asm_at_DE);

            AddToList(proc_CALL_ByteCode_at_DE);

            AddToList(proc_LD_BC_varA);
            AddToList(proc_LD_varA_BC);
            AddToList(proc_LD_HL_addrOf_varA);

            AddToList(proc_CALL_DE_for_ABR);
            AddToList(proc_CALL_DE_for_AB);
            AddToList(proc_CALL_DE_for_A);
            AddToList(proc_ADD_HL_BC);
            AddToList(proc_SUB_HL_BC);
            AddToList(proc_SHL_HL_BC);
            AddToList(proc_SHR_HL_BC);

            AddToList(proc_opIf_Compare_AB);

            AddToList(proc_opPtr_GetPut_PV);
            AddToList(proc_opPtr_GetPut_PIV);


            AddToList(inter_opRet);
            AddToList(inter_opJmpB);
            AddToList(inter_opJmpW);

            AddToList(inter_opCall);

            AddToList(inter_opAsmW);

            AddToList(inter_opSet_AR);

            AddToList(inter_opCalc_Plus_ABR);
            AddToList(inter_opCalc_Minus_ABR);
            AddToList(inter_opCalc_SHL_ABR);
            AddToList(inter_opCalc_SHR_ABR);
            AddToList(inter_opCalc_PlusEqual_AB);
            AddToList(inter_opCalc_MinusEqual_AB);
            AddToList(inter_opCalc_SHL_AB);
            AddToList(inter_opCalc_SHR_AB);
            AddToList(inter_opCalc_PlusPlus_A);
            AddToList(inter_opCalc_MinusMinus_A);
            AddToList(inter_opCalc_SHL_A);
            AddToList(inter_opCalc_SHR_A);

            AddToList(inter_opIf_Equal_AB);
            AddToList(inter_opIf_NotEqual_AB);
            AddToList(inter_opIf_Less_AB);
            AddToList(inter_opIf_GreaterEqual_AB);

            AddToList(inter_opPtr_Put_PV);
            AddToList(inter_opPtr_Get_PV);
            AddToList(inter_opPtr_Put_PIV);
            AddToList(inter_opPtr_Get_PIV);
        }

        private static void AddToList(AsmProc asmProc) {
            all.Add(asmProc.name, asmProc);
        }

        public static List<AsmProc> GetAllDependenciesFor(AsmProc asmProc) {

            Dictionary<string, AsmProc> totalDeps = new Dictionary<string, AsmProc>();

            List<string> depNames =
                asmProc.dependencies != null
                    ? new List<string>(asmProc.dependencies)
                    : new List<string>();

            bool needContinue = false;

            do {
                List<string> newDepNames = new List<string>();

                List<AsmProc> depProcs = new List<AsmProc>(depNames.Count);

                // convert names into AsmProc's
                foreach (string depName in depNames) {
                    if (all.TryGetValue(depName, out AsmProc proc)) {
                        depProcs.Add(proc);
                    } else {
                        throw new Exception($"Can't find proc with name '{depName}' in all.");
                    }
                }

                // add procedures to total
                foreach (AsmProc proc in depProcs) {
                    if (!totalDeps.ContainsKey(proc.name)) {
                        totalDeps.Add(proc.name, proc);
                    }
                }

                foreach (AsmProc proc in depProcs) {
                    List<string> currProcDeps = proc.dependencies != null ? proc.dependencies : new List<string>();
                    // remove already added
                    currProcDeps.RemoveAll((name) => totalDeps.ContainsKey(name));

                    newDepNames.AddRange(currProcDeps);
                }

                depNames = newDepNames;

                // check a reson for continue
                needContinue = newDepNames.Count > 0;

            } while (needContinue);

            List<AsmProc> result = new List<AsmProc>();
            foreach (KeyValuePair<string, AsmProc> kvp in totalDeps) {
                result.Add(kvp.Value);
            }

            return result;
        }

        #region USUAL COMMON PROCEDURES

        //// * -------------------------------------------------------------------------------------- *
        //// * maked for main cycle                                                                   *
        //// * -------------------------------------------------------------------------------------- *

        public const string N_inter_main = "inter_main";
        public static AsmProc inter_main = new AsmProc() {
            name = N_inter_main,
            dependencies = new List<string>() {
                N_proc_LD_A_next_byte_of_prog,
                N_proc_CALL_asm_at_DE,
                N_proc_CALL_ByteCode_at_DE,
                N_proc_LD_DE_from_word_table_BC_by_index_A,
            },
            text =
                $"{N_inter_main}:\n" +
                $"    \n" +
                $"{N_inter_main}__repeat:\n" +
                $"    call {N_proc_LD_A_next_byte_of_prog}\n" +
                $"    LD B, OP_CODES_COUNT\n" +
                $"    LD C, A\n" +
                $"    SUB B\n" +
                $"    JR NC, {N_inter_main}__short_call\n" + // if A >= B jump to __short_call
                $"    \n" +
                $"{N_inter_main}__inter_call:\n" +
                $"    LD A, C\n" +
                $"    LD BC, inter_table\n" +
                $"    call {N_proc_LD_DE_from_word_table_BC_by_index_A}\n" +
                $"    call {N_proc_CALL_asm_at_DE}\n" +
                $"    JR {N_inter_main}__jump_to_repeat\n" +
                $"    \n" +
                $"{N_inter_main}__short_call:\n" +
                $"    LD BC, module_short_calls\n" +
                $"    call {N_proc_LD_DE_from_word_table_BC_by_index_A}\n" +
                $"    call {N_proc_CALL_ByteCode_at_DE}\n" +
                $"    \n" +
                $"{N_inter_main}__jump_to_repeat:\n" +
                $"{N_inter_main}__erase_for_halt:\n" +
                $"    JR {N_inter_main}__repeat\n" +
                $"    \n" +
                $"    ret\n"
        };

        public const string N_proc_LD_DE_from_word_table_BC_by_index_A = "N_proc_LD_DE_from_word_table_BC_by_index_A";
        public static AsmProc proc_LD_DE_from_word_table_BC_by_index_A = new AsmProc() {
            name = N_proc_LD_DE_from_word_table_BC_by_index_A,
            text =
               $"{N_proc_LD_DE_from_word_table_BC_by_index_A}:\n" +
                "    \n" +
                "    PUSH HL\n" +
                "    \n" +
                "    LD H, 0\n" +
                "    LD L, A\n" +
                "    SLA L\n" +
                "    RL H\n" +
                "    ADD HL, BC\n" +
                "    LD E, (HL)\n" +
                "    INC HL\n" +
                "    LD D, (HL)\n" +
                "    \n" +
                "    POP HL\n" +
                "    \n" +
                "    ret\n"
        };

        public const string N_proc_LD_A_next_byte_of_prog = "proc_LD_A_next_byte_of_prog";
        public static AsmProc proc_LD_A_next_byte_of_prog = new AsmProc() {
            name = N_proc_LD_A_next_byte_of_prog,
            text =
               $"{N_proc_LD_A_next_byte_of_prog}:\n" +
                "    \n" +
                "    PUSH HL\n" +
                "    \n" +
                "    LD HL, (curr_exec_addr)\n" +
                "    LD A, (HL)\n" +
                "    INC HL\n" +
                "    LD (curr_exec_addr), HL\n" +
                "    \n" +
                "    POP HL\n" +
                "    \n" +
                "    ret\n"
        };

        public const string N_proc_LD_BC_next_word_of_prog = "proc_LD_BC_next_word_of_prog";
        public static AsmProc proc_LD_BC_next_word_of_prog = new AsmProc() {
            name = N_proc_LD_BC_next_word_of_prog,
            text =
               $"{N_proc_LD_BC_next_word_of_prog}:\n" +
                "    \n" +
                "    PUSH HL\n" +
                "    \n" +
                "    LD HL, (curr_exec_addr)\n" +
                "    LD C, (HL)\n" +
                "    INC HL\n" +
                "    LD B, (HL)\n" +
                "    INC HL\n" +
                "    LD (curr_exec_addr), HL\n" +
                "    \n" +
                "    POP HL\n" +
                "    \n" +
                "    ret\n"
        };

        public const string N_proc_CALL_asm_at_DE = "proc_CALL_asm_at_DE";
        public static AsmProc proc_CALL_asm_at_DE = new AsmProc() {
            name = N_proc_CALL_asm_at_DE,
            text =
               $"{N_proc_CALL_asm_at_DE}:\n" +
                "    \n" +
               $"    LD ({N_proc_CALL_asm_at_DE}__call+1), DE\n" +
               $"{N_proc_CALL_asm_at_DE}__call:\n" +
                "    call 0000\n" +
                "    \n" +
                "    ret\n"
        };

        public const string N_proc_CALL_ByteCode_at_DE = "proc_CALL_ByteCode_at_DE";
        public static AsmProc proc_CALL_ByteCode_at_DE = new AsmProc() {
            name = N_proc_CALL_ByteCode_at_DE,
            text =
               $"{N_proc_CALL_ByteCode_at_DE}:\n" +
                "    \n" +
                "    PUSH AF\n" +
                "    PUSH BC\n" +
                "    PUSH HL\n" +
                "    \n" +
            // load the callstack top into HL and callstack maximum into BC
                "    LD BC, 65534\n" +
                "    LD HL, (bc_callstack_top)\n" +
            // check the callstack_top on overflow
                "    PUSH HL\n" +
                "    AND A\n" +
                "    SBC HL, BC\n" +
                "    POP HL\n" +
               $"    JR NC, {N_proc_CALL_ByteCode_at_DE}__stack_overflow\n" +
            // load return addr into BC
                "    LD BC, (curr_exec_addr)\n" +
            // save return addr (BC) by call stack top addr (HL)
                "    LD (HL), C\n" +
                "    INC HL\n" +
                "    LD (HL), B\n" +
                "    INC HL\n" +
            // coхранить HL в колстек топ 
                "    LD (bc_callstack_top), HL\n" +
            // прыгнуть
                "    LD (curr_exec_addr), DE\n" +
                "    \n" +
               $"    JR {N_proc_CALL_ByteCode_at_DE}__exit\n" +
               $"{N_proc_CALL_ByteCode_at_DE}__stack_overflow:\n" +
                "    \n" +
                "    LD A, 2\n" +
                "    call 5633\n" +
                "    LD A, '#'\n" +
                "    rst 16\n" +
                "    LD A, '1'\n" +
                "    rst 16\n" +
                "    \n" +
                "    LD BC, 0000\n" +
               $"    LD ({N_inter_main}__erase_for_halt), BC\n" +
                "    \n" +
               $"{N_proc_CALL_ByteCode_at_DE}__exit:\n" +
                "    POP HL\n" +
                "    POP BC\n" +
                "    POP AF\n" +
                "    \n" +
                "    ret\n"
        };

        // * -------------------------------------------------------------------------------------- *
        // * maked for OperatorSet                                                                  *
        // * -------------------------------------------------------------------------------------- *

        public const string N_proc_LD_BC_varA = "proc_LD_BC_varA";
        public static AsmProc proc_LD_BC_varA = new AsmProc() {
            name = N_proc_LD_BC_varA,
            dependencies = new List<string>() {
                N_proc_LD_HL_addrOf_varA,
            },
            text =
               $"{N_proc_LD_BC_varA}:\n" +
                "    \n" +
                "    PUSH HL\n" +
                "    \n" +
               $"    call {N_proc_LD_HL_addrOf_varA}\n" +
                "    LD C, (HL)\n" +
               $"    CP {ProgramNode.ADDR_GV_WORD_TABLE} - {ProgramNode.ADDR_GV_BYTE_TABLE}\n" +
               $"    JR NC, {N_proc_LD_BC_varA}__word  ;jump if A >= count\n" +
                ";load 1 byte by addr\n" +
                "    LD B, 0\n" +
               $"    JR {N_proc_LD_BC_varA}__exit" +
                "    \n" +
               $"{N_proc_LD_BC_varA}__word:" +
                ";load 2 bytes by addr\n" +
                "    INC HL\n" +
                "    LD B, (HL)\n" +
                "    \n" +
               $"{N_proc_LD_BC_varA}__exit:\n" +
                "    \n" +
                "    POP HL\n" +
                "    \n" +
                "    ret\n"
        };

        public const string N_proc_LD_varA_BC = "proc_LD_varA_BC";
        public static AsmProc proc_LD_varA_BC = new AsmProc() {
            name = N_proc_LD_varA_BC,
            dependencies = new List<string>() {
                N_proc_LD_HL_addrOf_varA,
            },
            text =
               $"{N_proc_LD_varA_BC}:\n" +
                "    \n" +
                "    PUSH HL\n" +
                "    \n" +
               $"    call {N_proc_LD_HL_addrOf_varA}\n" +
                "    LD (HL), C\n" +
               $"    CP {ProgramNode.ADDR_GV_WORD_TABLE} - {ProgramNode.ADDR_GV_BYTE_TABLE}\n" +
               $"    JR C, {N_proc_LD_varA_BC}__exit  ;jump if A < count\n" +
                "    INC HL\n" +
                "    LD (HL), B\n" +
                "    \n" +
               $"{N_proc_LD_varA_BC}__exit:\n" +
                "    \n" +
                "    POP HL\n" +
                "    \n" +
                "    ret\n"
        };

        public const string N_proc_LD_HL_addrOf_varA = "proc_LD_HL_addrOf_varA";
        public static AsmProc proc_LD_HL_addrOf_varA = new AsmProc() {
            name = N_proc_LD_HL_addrOf_varA,
            text =
               $"{N_proc_LD_HL_addrOf_varA}:\n" +
                "    \n" +
                "    PUSH BC\n" +
                "    PUSH AF\n" +
                "    \n" +
                "    LD H, 0\n" +
                "    \n" +
               $"    LD C, {ProgramNode.ADDR_GV_WORD_TABLE} - {ProgramNode.ADDR_GV_BYTE_TABLE}  ;byte vars count\n" +
                "    CP C\n" +
               $"    JR NC, {N_proc_LD_HL_addrOf_varA}__word_table  ;jump if A >= C\n" +
                ";calc like byte addr\n" +
                "    LD L, A\n" +
               $"    LD BC, {ProgramNode.ADDR_GV_BYTE_TABLE}\n" +
                "    ADD HL, BC\n" +
               $"    JR {N_proc_LD_HL_addrOf_varA}__exit" +
                "    \n" +
               $"{N_proc_LD_HL_addrOf_varA}__word_table:\n" +
                ";calc like word addr\n" +
                "    SUB C\n" +
                "    LD L, A\n" +
                "    SLA L\n" +
                "    RL H\n" +
               $"    LD BC, {ProgramNode.ADDR_GV_WORD_TABLE}\n" +
                "    ADD HL, BC\n" +
               $"{N_proc_LD_HL_addrOf_varA}__exit:\n" +
                "    POP AF\n" +
                "    POP BC\n" +
                "    \n" +
                "    ret\n"
        };

        // * -------------------------------------------------------------------------------------- *
        // * maked for OperatorCalc                                                                 *
        // * -------------------------------------------------------------------------------------- *

        public const string N_proc_CALL_DE_for_ABR = "proc_CALL_DE_for_ABR";
        public static AsmProc proc_CALL_DE_for_ABR = new AsmProc() {
            name = N_proc_CALL_DE_for_ABR,
            dependencies = new List<string>() {
                N_proc_LD_A_next_byte_of_prog,
                N_proc_LD_BC_varA,
                N_proc_LD_varA_BC,
                N_proc_CALL_asm_at_DE,
            },
            text =
               $"{N_proc_CALL_DE_for_ABR}:\n" +
                "    \n" +
                "    PUSH AF\n" +
                "    PUSH BC\n" +
                "    PUSH HL\n" +
                "    \n" +
               $"    call {N_proc_LD_A_next_byte_of_prog}\n" + // A
               $"    call {N_proc_LD_BC_varA}\n" +
                "    \n" +
                "    PUSH BC\n" +
                "    POP HL\n" +
                "    \n" +
               $"    call {N_proc_LD_A_next_byte_of_prog}\n" + // B
               $"    call {N_proc_LD_BC_varA}\n" +
                "    \n" +
               $"    call {N_proc_CALL_asm_at_DE}\n" +
                "    \n" +
                "    PUSH HL\n" +
                "    POP BC\n" +
                "    \n" +
               $"    call {N_proc_LD_A_next_byte_of_prog}\n" + // R
               $"    call {N_proc_LD_varA_BC}\n" +
                "    \n" +
                "    POP HL\n" +
                "    POP BC\n" +
                "    POP AF\n" +
                "    \n" +
                "    ret\n"
        };

        public const string N_proc_CALL_DE_for_AB = "proc_CALL_DE_for_AB";
        public static AsmProc proc_CALL_DE_for_AB = new AsmProc() {
            name = N_proc_CALL_DE_for_AB,
            dependencies = new List<string>() {
                N_proc_LD_A_next_byte_of_prog,
                N_proc_LD_BC_varA,
                N_proc_LD_varA_BC,
                N_proc_CALL_asm_at_DE,
            },
            text =
               $"{N_proc_CALL_DE_for_AB}:\n" +
                "    \n" +
                "    PUSH AF\n" +
                "    PUSH BC\n" +
                "    PUSH HL\n" +
                "    \n" +
               $"    call {N_proc_LD_A_next_byte_of_prog}\n" + // A
                "    PUSH AF\n" +
               $"    call {N_proc_LD_BC_varA}\n" +
                "    \n" +
                "    PUSH BC\n" +
                "    POP HL\n" +
                "    \n" +
               $"    call {N_proc_LD_A_next_byte_of_prog}\n" + // B
               $"    call {N_proc_LD_BC_varA}\n" +
                "    \n" +
               $"    call {N_proc_CALL_asm_at_DE}\n" +
                "    \n" +
                "    PUSH HL\n" +
                "    POP BC\n" +
                "    \n" +
                "    POP AF\n" +
               $"    call {N_proc_LD_varA_BC}\n" +
                "    \n" +
                "    POP HL\n" +
                "    POP BC\n" +
                "    POP AF\n" +
                "    \n" +
                "    ret\n"
        };

        public const string N_proc_CALL_DE_for_A = "proc_CALL_DE_for_A";
        public static AsmProc proc_CALL_DE_for_A = new AsmProc() {
            name = N_proc_CALL_DE_for_A,
            dependencies = new List<string>() {
                N_proc_LD_A_next_byte_of_prog,
                N_proc_LD_BC_varA,
                N_proc_LD_varA_BC,
                N_proc_CALL_asm_at_DE,
            },
            text =
               $"{N_proc_CALL_DE_for_A}:\n" +
                "    \n" +
                "    PUSH AF\n" +
                "    PUSH BC\n" +
                "    PUSH HL\n" +
                "    \n" +
               $"    call {N_proc_LD_A_next_byte_of_prog}\n" + // A
               $"    call {N_proc_LD_BC_varA}\n" +
                "    \n" +
                "    PUSH BC\n" +
                "    POP HL\n" +
                "    \n" +
               $"    LD BC, 1\n" +
                "    \n" +
               $"    call {N_proc_CALL_asm_at_DE}\n" +
                "    \n" +
                "    PUSH HL\n" +
                "    POP BC\n" +
                "    \n" +
               $"    call {N_proc_LD_varA_BC}\n" +
                "    \n" +
                "    POP HL\n" +
                "    POP BC\n" +
                "    POP AF\n" +
                "    \n" +
                "    ret\n"
        };

        public const string N_proc_ADD_HL_BC = "proc_ADD_HL_BC";
        public static AsmProc proc_ADD_HL_BC = new AsmProc() {
            name = N_proc_ADD_HL_BC,
            text =
               $"{N_proc_ADD_HL_BC}:\n" +
                "    \n" +
                "    ADD HL, BC\n" +
                "    \n" +
                "    ret\n"
        };

        public const string N_proc_SUB_HL_BC = "proc_SUB_HL_BC";
        public static AsmProc proc_SUB_HL_BC = new AsmProc() {
            name = N_proc_SUB_HL_BC,
            text =
               $"{N_proc_SUB_HL_BC}:\n" +
                "    \n" +
                "    PUSH AF\n" +
                "    \n" +
                "    AND A \n" +
                "    SBC HL, BC \n" +
                "    \n" +
                "    POP AF\n" +
                "    \n" +
                "    ret\n"
        };

        public const string N_proc_SHL_HL_BC = "proc_SHL_HL_BC";
        public static AsmProc proc_SHL_HL_BC = new AsmProc() {
            name = N_proc_SHL_HL_BC,
            text =
               $"{N_proc_SHL_HL_BC}:\n" +
                "    \n" +
                "    PUSH AF\n" +
                "    PUSH BC\n" +
                "    \n" +
                "    LD B, C  ; DJNZ works with B only. higher byte stored in B is not needed for counting only lower C.\n" +
                "    LD A, 0\n" +
                "    CP B\n" +
               $"    JR Z, {N_proc_SHL_HL_BC}__skipIfZero\n" +
                "    \n" +
               $"{N_proc_SHL_HL_BC}__repeat:\n" +
                "    SLA L\n" +
                "    RL H\n" +
               $"    DJNZ {N_proc_SHL_HL_BC}__repeat\n" +
                "    \n" +
               $"{N_proc_SHL_HL_BC}__skipIfZero:\n" +
                "    \n" +
                "    POP BC\n" +
                "    POP AF\n" +
                "    \n" +
                "    ret\n"
        };

        public const string N_proc_SHR_HL_BC = "proc_SHR_HL_BC";
        public static AsmProc proc_SHR_HL_BC = new AsmProc() {
            name = N_proc_SHR_HL_BC,
            text =
               $"{N_proc_SHR_HL_BC}:\n" +
                "    \n" +
                "    PUSH AF\n" +
                "    PUSH BC\n" +
                "    \n" +
                "    LD B, C  ; DJNZ works with B only. higher byte stored in B is not needed for counting only lower C.\n" +
                "    LD A, 0\n" +
                "    CP B\n" +
               $"    JR Z, {N_proc_SHR_HL_BC}__skipIfZero\n" +
                "    \n" +
               $"{N_proc_SHR_HL_BC}__repeat:\n" +
                "    SRL H\n" +
                "    RR L\n" +
               $"    DJNZ {N_proc_SHR_HL_BC}__repeat\n" +
                "    \n" +
               $"{N_proc_SHR_HL_BC}__skipIfZero:\n" +
                "    \n" +
                "    POP BC\n" +
                "    POP AF\n" +
                "    \n" +
                "    ret\n"
        };

        // * -------------------------------------------------------------------------------------- *
        // * maked for OperatorIf                                                                   *
        // * -------------------------------------------------------------------------------------- *

        public const string N_proc_opIf_Compare_AB = "proc_opIf_Compare_AB";
        public static AsmProc proc_opIf_Compare_AB = new AsmProc() {
            name = N_proc_opIf_Compare_AB,
            dependencies = new List<string>() {
                N_proc_LD_A_next_byte_of_prog,
                N_proc_LD_BC_varA,
                N_proc_LD_BC_next_word_of_prog,
            },
            text =
               $"{N_proc_opIf_Compare_AB}:\n" +
                "    \n" +
               $"    call {N_proc_LD_A_next_byte_of_prog}\n" +
               $"    call {N_proc_LD_BC_varA}\n" +
                "    PUSH BC\n" +
               $"    call {N_proc_LD_A_next_byte_of_prog}\n" +
               $"    call {N_proc_LD_BC_varA}\n" +
                "    PUSH BC\n" +
               $"    call {N_proc_LD_BC_next_word_of_prog}\n" +
                "    PUSH BC\n" +
                "    \n" +
                "    POP DE\n" +
                "    POP BC\n" +
                "    POP HL\n" +
                "    \n" +
                "    AND A\n" +
                "    SBC HL, BC\n" +
               $"{N_proc_opIf_Compare_AB}__jmp_op:\n" +
               $"    JR Z, {N_proc_opIf_Compare_AB}__exit\n" +
                "    LD (curr_exec_addr), DE\n" +
                "    \n" +
               $"{N_proc_opIf_Compare_AB}__exit:\n" +
                "    ret\n"
        };

        // * -------------------------------------------------------------------------------------- *
        // * maked for OperatorPtr                                                                  *
        // * -------------------------------------------------------------------------------------- *

        // this func is universal for put and get
        // and is unsafe by registers. it means this proc use and change many registers
        public const string N_proc_opPtr_GetPut_PV = "proc_opPtr_GetPut_PV";
        public static AsmProc proc_opPtr_GetPut_PV = new AsmProc() {
            name = N_proc_opPtr_GetPut_PV,
            dependencies = new List<string>() {
                N_proc_LD_A_next_byte_of_prog,
                N_proc_LD_BC_varA,
                N_proc_LD_varA_BC,
            },
            text =
               $"{N_proc_opPtr_GetPut_PV}:\n" +
                "    \n" +
               $"    call {N_proc_LD_A_next_byte_of_prog}\n" +
               $"    call {N_proc_LD_BC_varA}\n" +
                "    PUSH BC\n" +
                "    POP HL\n" +
                "    \n" +
               $"    call {N_proc_LD_A_next_byte_of_prog}\n" +
               $"    CP {ProgramNode.ADDR_GV_WORD_TABLE} - {ProgramNode.ADDR_GV_BYTE_TABLE}\n" +
               $"    JR NC, {N_proc_opPtr_GetPut_PV}__word\n" + // if A >= byte vars count then jump to __word
                "    \n" +
               $"{N_proc_opPtr_GetPut_PV}__byte:\n" +
               $"    JP {N_proc_opPtr_GetPut_PV}__put_byte\n" +
               $"{N_proc_opPtr_GetPut_PV}__put_byte:\n" +
               $"    call {N_proc_LD_BC_varA}\n" +
                "    LD (HL), C\n" +
               $"    JR {N_proc_opPtr_GetPut_PV}__exit\n" +
               $"{N_proc_opPtr_GetPut_PV}__get_byte:\n" +
                "    LD C, (HL)\n" +
               $"    call {N_proc_LD_varA_BC}\n" +
               $"    JR {N_proc_opPtr_GetPut_PV}__exit\n" +
                "    \n" +
               $"{N_proc_opPtr_GetPut_PV}__word:\n" +
               $"    JP {N_proc_opPtr_GetPut_PV}__put_word\n" +
               $"{N_proc_opPtr_GetPut_PV}__put_word:\n" +
               $"    call {N_proc_LD_BC_varA}\n" +
                "    LD (HL), C\n" +
                "    INC HL\n" +
                "    LD (HL), B\n" +
               $"    JR {N_proc_opPtr_GetPut_PV}__exit\n" +
               $"{N_proc_opPtr_GetPut_PV}__get_word:\n" +
                "    LD C, (HL)\n" +
                "    INC HL\n" +
                "    LD B, (HL)\n" +
               $"    call {N_proc_LD_varA_BC}\n" +
                "    \n" +
               $"{N_proc_opPtr_GetPut_PV}__exit:\n" +
                "    \n" +
                "    ret\n"
        };

        // this func is universal for put and get
        // and is unsafe by registers. it means this proc use and change many registers
        public const string N_proc_opPtr_GetPut_PIV = "proc_opPtr_GetPut_PIV";
        public static AsmProc proc_opPtr_GetPut_PIV = new AsmProc() {
            name = N_proc_opPtr_GetPut_PIV,
            dependencies = new List<string>() {
                N_proc_LD_A_next_byte_of_prog,
                N_proc_LD_BC_varA,
                N_proc_LD_varA_BC,
            },
            text =
               $"{N_proc_opPtr_GetPut_PIV}:\n" +
                "    \n" +
               $"    call {N_proc_LD_A_next_byte_of_prog}\n" +
               $"    call {N_proc_LD_BC_varA}\n" +
                "    PUSH BC\n" +
                "    POP HL\n" +
                "    \n" +
               $"    call {N_proc_LD_A_next_byte_of_prog}\n" +
               $"    call {N_proc_LD_BC_varA}\n" +
                "    PUSH BC\n" +
                "    POP DE\n" +
                "    \n" +
               $"    call {N_proc_LD_A_next_byte_of_prog}\n" +
               //$"    call {N_proc_LD_BC_varA}\n" +
               $"    CP {ProgramNode.ADDR_GV_WORD_TABLE} - {ProgramNode.ADDR_GV_BYTE_TABLE}\n" +
               $"    JR NC, {N_proc_opPtr_GetPut_PIV}__word\n" + // if A >= byte vars count then jump to __word
                "    \n" +
               $"{N_proc_opPtr_GetPut_PIV}__byte:\n" +
               $"    JP {N_proc_opPtr_GetPut_PIV}__put_byte\n" +
               $"{N_proc_opPtr_GetPut_PIV}__put_byte:\n" +
               $"    call {N_proc_LD_BC_varA}\n" +
                "    ADD HL, DE\n" +
                "    LD (HL), C\n" +
               $"    JR {N_proc_opPtr_GetPut_PIV}__exit\n" +
               $"{N_proc_opPtr_GetPut_PIV}__get_byte:\n" +
                "    ADD HL, DE\n" +
                "    LD C, (HL)\n" +
               $"    call {N_proc_LD_varA_BC}\n" +
               $"    JR {N_proc_opPtr_GetPut_PIV}__exit\n" +
                "    \n" +
               $"{N_proc_opPtr_GetPut_PIV}__word:\n" +
               $"    JP {N_proc_opPtr_GetPut_PIV}__put_word\n" +
               $"{N_proc_opPtr_GetPut_PIV}__put_word:\n" +
               $"    call {N_proc_LD_BC_varA}\n" +
                "    SLA E\n" +
                "    RL D\n" +
                "    ADD HL, DE\n" +
                "    LD (HL), C\n" +
                "    INC HL\n" +
                "    LD (HL), B\n" +
               $"    JR {N_proc_opPtr_GetPut_PIV}__exit\n" +
               $"{N_proc_opPtr_GetPut_PIV}__get_word:\n" +
                "    SLA E\n" +
                "    RL D\n" +
                "    ADD HL, DE\n" +
                "    LD C, (HL)\n" +
                "    INC HL\n" +
                "    LD B, (HL)\n" +
               $"    call {N_proc_LD_varA_BC}\n" +
                "    \n" +
               $"{N_proc_opPtr_GetPut_PIV}__exit:\n" +
                "    \n" +
                "    ret\n"
        };

        #endregion USUAL COMMON PROCEDURES


        #region INTERPRETATION PROCEDURES

        // * -------------------------------------------------------------------------------------- *
        // * this is build-in procedures always in any program                                      *
        // * -------------------------------------------------------------------------------------- *

        public const string N_inter_opRet = "inter_opRet";
        public static AsmProc inter_opRet = new AsmProc() {
            opCode = "opRet",
            name = N_inter_opRet,
            text =
               $"{N_inter_opRet}:\n" +
                "    \n" +
                "    LD HL, (bc_callstack_top)\n" + // read the top addr to BC
                "    DEC HL\n" + // decreaste on 2
                "    DEC HL\n" +
                "    LD (bc_callstack_top), HL\n" +
                "    \n" +
                "    LD BC, bc_callstack\n" +
                "    PUSH HL\n" +
                "    AND A\n" +
                "    SBC HL, BC\n" +
                "    POP HL\n" +
               $"    JR C, {N_inter_opRet}__exit_program\n" +
                "    \n" +
                "    LD C, (HL)\n" +
                "    INC HL\n" +
                "    LD B, (HL)\n" +
                "    \n" +
                "    LD (curr_exec_addr), BC\n" +
                "    \n" +
                "    ret\n" +
               $"{N_inter_opRet}__exit_program:\n" +
                "    LD BC, 0\n" +
               $"    LD ({N_inter_main}__erase_for_halt), BC\n" +
                "    ret\n"

        };

        // will be used in future when we can be able to calculate bytes for needed jump
        public static AsmProc inter_opJmpB = new AsmProc() {
            opCode = "opJmpB",
            name = "",
            text = ""
        };

        public const string N_inter_opJmpW = "inter_opJmpW";
        public static AsmProc inter_opJmpW = new AsmProc() {
            opCode = "opJmpW",
            name = N_inter_opJmpW,
            dependencies = new List<string>() {
                N_proc_LD_BC_next_word_of_prog,
            },
            text =
               $"{N_inter_opJmpW}:\n" +
               $"    call {N_proc_LD_BC_next_word_of_prog}\n" +
                "    LD (curr_exec_addr), BC\n" +
                "    \n" +
                "    ret\n"
        };

        // * -------------------------------------------------------------------------------------- *
        // * maked for OperatorCall                                                                 *
        // * -------------------------------------------------------------------------------------- *

        public const string N_inter_opCall = "inter_opCall";
        public static AsmProc inter_opCall = new AsmProc() {
            opCode = "opCall",
            name = N_inter_opCall,
            dependencies = new List<string>() {
                N_proc_LD_BC_next_word_of_prog,
                N_proc_CALL_ByteCode_at_DE,
            },
            text =
               $"{N_inter_opCall}:\n" +
               $"    call {N_proc_LD_BC_next_word_of_prog}\n" +
                "    PUSH BC\n" +
                "    POP DE\n" +
               $"    call {N_proc_CALL_ByteCode_at_DE}\n" +
                "    ret\n"
        };

        // * -------------------------------------------------------------------------------------- *
        // * maked for OperatorAsm                                                                  *
        // * -------------------------------------------------------------------------------------- *

        public const string N_inter_opAsmW = "inter_opAsmW";
        public static AsmProc inter_opAsmW = new AsmProc() {
            opCode = "opAsmW",
            name = N_inter_opAsmW,
            dependencies = new List<string>() {
                N_proc_LD_BC_next_word_of_prog,
                N_proc_CALL_asm_at_DE
            },
            text =
               $"{N_inter_opAsmW}:\n" +
               $"    call {N_proc_LD_BC_next_word_of_prog}\n" +
                "    PUSH BC\n" +
                "    LD DE, (curr_exec_addr)\n" +
               $"    call {N_proc_CALL_asm_at_DE}\n" +
                "    POP DE\n" +
                "    LD (curr_exec_addr), DE\n" +
                "    \n" +
                "    ret\n"
        };

        // * -------------------------------------------------------------------------------------- *
        // * OperatorSet                                                                            *
        // * -------------------------------------------------------------------------------------- *

        public const string N_inter_opSet_AR = "inter_opSet_AR";
        public static AsmProc inter_opSet_AR = new AsmProc() {
            opCode = "opSet_AR",
            name = N_inter_opSet_AR,
            dependencies = new List<string>() {
                N_proc_LD_A_next_byte_of_prog,
                N_proc_LD_BC_varA,
                N_proc_LD_varA_BC,
            },
            text =
               $"{N_inter_opSet_AR}:\n" +
                "    \n" +
               $"    call {N_proc_LD_A_next_byte_of_prog}\n" +
               $"    call {N_proc_LD_BC_varA}\n" +
               $"    call {N_proc_LD_A_next_byte_of_prog}\n" +
               $"    call {N_proc_LD_varA_BC}\n" +
                "    \n" +
                "    ret\n"
        };

        // * -------------------------------------------------------------------------------------- *
        // * OperatorCalc                                                                           *
        // * -------------------------------------------------------------------------------------- *

        // RAB
        public const string N_inter_opCalc_Plus_ABR = "inter_opCalc_Plus_ABR";
        public static AsmProc inter_opCalc_Plus_ABR = new AsmProc() {
            opCode = "opCalc_Plus_ABR",
            name = N_inter_opCalc_Plus_ABR,
            dependencies = new List<string>() {
                N_proc_CALL_DE_for_ABR,
                N_proc_ADD_HL_BC,
            },
            text =
               $"{N_inter_opCalc_Plus_ABR}:\n" +
                "    \n" +
               $"    LD DE, {N_proc_ADD_HL_BC}\n" +
               $"    call {N_proc_CALL_DE_for_ABR}\n" +
                "    \n" +
                "    ret\n"
        };

        public const string N_inter_opCalc_Minus_ABR = "inter_opCalc_Minus_ABR";
        public static AsmProc inter_opCalc_Minus_ABR = new AsmProc() {
            opCode = "opCalc_Minus_ABR",
            name = N_inter_opCalc_Minus_ABR,
            dependencies = new List<string>() {
                N_proc_CALL_DE_for_ABR,
                N_proc_SUB_HL_BC,
            },
            text =
               $"{N_inter_opCalc_Minus_ABR}:\n" +
                "    \n" +
               $"    LD DE, {N_proc_SUB_HL_BC}\n" +
               $"    call {N_proc_CALL_DE_for_ABR}\n" +
                "    \n" +
                "    ret\n"
        };

        public const string N_inter_opCalc_SHL_ABR = "inter_opCalc_SHL_ABR";
        public static AsmProc inter_opCalc_SHL_ABR = new AsmProc() {
            opCode = "opCalc_SHL_ABR",
            name = N_inter_opCalc_SHL_ABR,
            dependencies = new List<string>() {
                N_proc_CALL_DE_for_ABR,
                N_proc_SHL_HL_BC,
            },
            text =
               $"{N_inter_opCalc_SHL_ABR}:\n" +
                "    \n" +
               $"    LD DE, {N_proc_SHL_HL_BC}\n" +
               $"    call {N_proc_CALL_DE_for_ABR}\n" +
                "    \n" +
                "    ret\n"
        };

        public const string N_inter_opCalc_SHR_ABR = "inter_opCalc_SHR_ABR";
        public static AsmProc inter_opCalc_SHR_ABR = new AsmProc() {
            opCode = "opCalc_SHR_ABR",
            name = N_inter_opCalc_SHR_ABR,
            dependencies = new List<string>() {
                N_proc_CALL_DE_for_ABR,
                N_proc_SHR_HL_BC,
            },
            text =
               $"{N_inter_opCalc_SHR_ABR}:\n" +
                "    \n" +
               $"    LD DE, {N_proc_SHR_HL_BC}\n" +
               $"    call {N_proc_CALL_DE_for_ABR}\n" +
                "    \n" +
                "    ret\n"
        };

        // Binary
        public const string N_inter_opCalc_PlusEqual_AB = "inter_opCalc_PlusEqual_AB";
        public static AsmProc inter_opCalc_PlusEqual_AB = new AsmProc() {
            opCode = "opCalc_PlusEqual_AB",
            name = N_inter_opCalc_PlusEqual_AB,
            dependencies = new List<string>() {
                N_proc_CALL_DE_for_AB,
                N_proc_ADD_HL_BC,
            },
            text =
               $"{N_inter_opCalc_PlusEqual_AB}:\n" +
                "    \n" +
               $"    LD DE, {N_proc_ADD_HL_BC}\n" +
               $"    call {N_proc_CALL_DE_for_AB}\n" +
                "    \n" +
                "    ret\n"
        };

        public const string N_inter_opCalc_MinusEqual_AB = "inter_opCalc_MinusEqual_AB";
        public static AsmProc inter_opCalc_MinusEqual_AB = new AsmProc() {
            opCode = "opCalc_MinusEqual_AB",
            name = N_inter_opCalc_MinusEqual_AB,
            dependencies = new List<string>() {
                N_proc_CALL_DE_for_AB,
                N_proc_SUB_HL_BC,
            },
            text =
               $"{N_inter_opCalc_MinusEqual_AB}:\n" +
                "    \n" +
               $"    LD DE, {N_proc_SUB_HL_BC}\n" +
               $"    call {N_proc_CALL_DE_for_AB}\n" +
                "    \n" +
                "    ret\n"
        };

        public const string N_inter_opCalc_SHL_AB = "inter_opCalc_SHL_AB";
        public static AsmProc inter_opCalc_SHL_AB = new AsmProc() {
            opCode = "opCalc_SHL_AB",
            name = N_inter_opCalc_SHL_AB,
            dependencies = new List<string>() {
                N_proc_CALL_DE_for_AB,
                N_proc_SHL_HL_BC,
            },
            text =
               $"{N_inter_opCalc_SHL_AB}:\n" +
                "    \n" +
               $"    LD DE, {N_proc_SHL_HL_BC}\n" +
               $"    call {N_proc_CALL_DE_for_AB}\n" +
                "    \n" +
                "    ret\n"
        };

        public const string N_inter_opCalc_SHR_AB = "inter_opCalc_SHR_AB";
        public static AsmProc inter_opCalc_SHR_AB = new AsmProc() {
            opCode = "opCalc_SHR_AB",
            name = N_inter_opCalc_SHR_AB,
            dependencies = new List<string>() {
                N_proc_CALL_DE_for_AB,
                N_proc_SHR_HL_BC,
            },
            text =
               $"{N_inter_opCalc_SHR_AB}:\n" +
                "    \n" +
               $"    LD DE, {N_proc_SHR_HL_BC}\n" +
               $"    call {N_proc_CALL_DE_for_AB}\n" +
                "    \n" +
                "    ret\n"
        };

        // Unary
        public const string N_inter_opCalc_PlusPlus_A = "inter_opCalc_PlusPlus_A";
        public static AsmProc inter_opCalc_PlusPlus_A = new AsmProc() {
            opCode = "opCalc_PlusPlus_A",
            name = N_inter_opCalc_PlusPlus_A,
            dependencies = new List<string>() {
                N_proc_CALL_DE_for_A,
                N_proc_ADD_HL_BC,
            },
            text =
               $"{N_inter_opCalc_PlusPlus_A}:\n" +
                "    \n" +
               $"    LD DE, {N_proc_ADD_HL_BC}\n" +
               $"    call {N_proc_CALL_DE_for_A}\n" +
                "    \n" +
                "    ret\n"
        };

        public const string N_inter_opCalc_MinusMinus_A = "inter_opCalc_MinusMinus_A";
        public static AsmProc inter_opCalc_MinusMinus_A = new AsmProc() {
            opCode = "opCalc_MinusMinus_A",
            name = N_inter_opCalc_MinusMinus_A,
            dependencies = new List<string>() {
                N_proc_CALL_DE_for_A,
                N_proc_SUB_HL_BC,
            },
            text =
               $"{N_inter_opCalc_MinusMinus_A}:\n" +
                "    \n" +
               $"    LD DE, {N_proc_SUB_HL_BC}\n" +
               $"    call {N_proc_CALL_DE_for_A}\n" +
                "    \n" +
                "    ret\n"
        };

        public const string N_inter_opCalc_SHL_A = "inter_opCalc_SHL_A";
        public static AsmProc inter_opCalc_SHL_A = new AsmProc() {
            opCode = "opCalc_SHL_A",
            name = N_inter_opCalc_SHL_A,
            dependencies = new List<string>() {
                N_proc_CALL_DE_for_A,
                N_proc_SHL_HL_BC,
            },
            text =
               $"{N_inter_opCalc_SHL_A}:\n" +
                "    \n" +
               $"    LD DE, {N_proc_SHL_HL_BC}\n" +
               $"    call {N_proc_CALL_DE_for_A}\n" +
                "    \n" +
                "    ret\n"
        };

        public const string N_inter_opCalc_SHR_A = "inter_opCalc_SHR_A";
        public static AsmProc inter_opCalc_SHR_A = new AsmProc() {
            opCode = "opCalc_SHR_A",
            name = N_inter_opCalc_SHR_A,
            dependencies = new List<string>() {
                N_proc_CALL_DE_for_A,
                N_proc_SHR_HL_BC,
            },
            text =
               $"{N_inter_opCalc_SHR_A}:\n" +
                "    \n" +
               $"    LD DE, {N_proc_SHR_HL_BC}\n" +
               $"    call {N_proc_CALL_DE_for_A}\n" +
                "    \n" +
                "    ret\n"
        };

        // * -------------------------------------------------------------------------------------- *
        // * maked for OperatorIf                                                                   *
        // * -------------------------------------------------------------------------------------- *

        public const string N_inter_opIf_Equal_AB = "inter_opIf_Equal_AB";
        public static AsmProc inter_opIf_Equal_AB = new AsmProc() {
            opCode = "opIf_Equal_AB",
            name = N_inter_opIf_Equal_AB,
            dependencies = new List<string>() {
                N_proc_opIf_Compare_AB
            },
            text =
               $"{N_inter_opIf_Equal_AB}:\n" +
                "    \n" +
                "    LD A, #28\n" + // JR, Z is 28XX
               $"    LD ({N_proc_opIf_Compare_AB}__jmp_op), A\n" +
               $"    call {N_proc_opIf_Compare_AB}\n" +
                "    ret\n"
        };

        public const string N_inter_opIf_NotEqual_AB = "inter_opIf_NotEqual_AB";
        public static AsmProc inter_opIf_NotEqual_AB = new AsmProc() {
            opCode = "opIf_NotEqual_AB",
            name = N_inter_opIf_NotEqual_AB,
            dependencies = new List<string>() {
                N_proc_opIf_Compare_AB
            },
            text =
               $"{N_inter_opIf_NotEqual_AB}:\n" +
                "    \n" +
                "    LD A, #20\n" + // JR, NZ is 20XX
               $"    LD ({N_proc_opIf_Compare_AB}__jmp_op), A\n" +
               $"    call {N_proc_opIf_Compare_AB}\n" +
                "    ret\n"
        };

        public const string N_inter_opIf_Less_AB = "inter_opIf_Less_AB";
        public static AsmProc inter_opIf_Less_AB = new AsmProc() {
            opCode = "opIf_Less_AB",
            name = N_inter_opIf_Less_AB,
            dependencies = new List<string>() {
                N_proc_opIf_Compare_AB
            },
            text =
               $"{N_inter_opIf_Less_AB}:\n" +
                "    \n" +
                "    LD A, #38\n" + // JR, C is 38XX
               $"    LD ({N_proc_opIf_Compare_AB}__jmp_op), A\n" +
               $"    call {N_proc_opIf_Compare_AB}\n" +
                "    ret\n"
        };

        public const string N_inter_opIf_GreaterEqual_AB = "inter_opIf_GreaterEqual_AB";
        public static AsmProc inter_opIf_GreaterEqual_AB = new AsmProc() {
            opCode = "opIf_GreaterEqual_AB",
            name = N_inter_opIf_GreaterEqual_AB,
            dependencies = new List<string>() {
                N_proc_opIf_Compare_AB
            },
            text =
               $"{N_inter_opIf_GreaterEqual_AB}:\n" +
                "    \n" +
                "    LD A, #30\n" + // JR, NC is 30XX
               $"    LD ({N_proc_opIf_Compare_AB}__jmp_op), A\n" +
               $"    call {N_proc_opIf_Compare_AB}\n" +
                "    ret\n"
        };

        // * -------------------------------------------------------------------------------------- *
        // * maked for OperatorPtr                                                                  *
        // * -------------------------------------------------------------------------------------- *

        public const string N_inter_opPtr_Put_PV = "inter_opPtr_Put_PV";
        public static AsmProc inter_opPtr_Put_PV = new AsmProc() {
            opCode = "opPtr_Put_PV",
            name = N_inter_opPtr_Put_PV,
            dependencies = new List<string>() {
                N_proc_opPtr_GetPut_PV
            },
            text =
               $"{N_inter_opPtr_Put_PV}:\n" +
                "    \n" +
               $"    LD BC, {N_proc_opPtr_GetPut_PV}__put_byte\n" +
               $"    LD DE, {N_proc_opPtr_GetPut_PV}__put_word\n" +
               $"    LD ({N_proc_opPtr_GetPut_PV}__byte+1), BC\n" +
               $"    LD ({N_proc_opPtr_GetPut_PV}__word+1), DE\n" +
                "    \n" +
               $"    call {N_proc_opPtr_GetPut_PV}\n" +
                "    \n" +
                "    ret\n"
        };

        public const string N_inter_opPtr_Get_PV = "inter_opPtr_Get_PV";
        public static AsmProc inter_opPtr_Get_PV = new AsmProc() {
            opCode = "opPtr_Get_PV",
            name = N_inter_opPtr_Get_PV,
            dependencies = new List<string>() {
                N_proc_opPtr_GetPut_PV
            },
            text =
               $"{N_inter_opPtr_Get_PV}:\n" +
                "    \n" +
               $"    LD BC, {N_proc_opPtr_GetPut_PV}__get_byte\n" +
               $"    LD DE, {N_proc_opPtr_GetPut_PV}__get_word\n" +
               $"    LD ({N_proc_opPtr_GetPut_PV}__byte+1), BC\n" +
               $"    LD ({N_proc_opPtr_GetPut_PV}__word+1), DE\n" +
                "    \n" +
               $"    call {N_proc_opPtr_GetPut_PV}\n" +
                "    \n" +
                "    ret\n"
        };

        public const string N_inter_opPtr_Put_PIV = "inter_opPtr_Put_PIV";
        public static AsmProc inter_opPtr_Put_PIV = new AsmProc() {
            opCode = "opPtr_Put_PIV",
            name = N_inter_opPtr_Put_PIV,
            dependencies = new List<string>() {
                N_proc_opPtr_GetPut_PIV,
            },
            text =
               $"{N_inter_opPtr_Put_PIV}:\n" +
                "    \n" +
               $"    LD BC, {N_proc_opPtr_GetPut_PIV}__put_byte\n" +
               $"    LD DE, {N_proc_opPtr_GetPut_PIV}__put_word\n" +
               $"    LD ({N_proc_opPtr_GetPut_PIV}__byte+1), BC\n" +
               $"    LD ({N_proc_opPtr_GetPut_PIV}__word+1), DE\n" +
                "    \n" +
               $"    call {N_proc_opPtr_GetPut_PIV}\n" +
                "    \n" +
                "    ret\n"
        };

        public const string N_inter_opPtr_Get_PIV = "inter_opPtr_Get_PIV";
        public static AsmProc inter_opPtr_Get_PIV = new AsmProc() {
            opCode = "opPtr_Get_PIV",
            name = N_inter_opPtr_Get_PIV,
            dependencies = new List<string>() {
                N_proc_opPtr_GetPut_PIV,
            },
            text =
               $"{N_inter_opPtr_Get_PIV}:\n" +
                "    \n" +
               $"    LD BC, {N_proc_opPtr_GetPut_PIV}__get_byte\n" +
               $"    LD DE, {N_proc_opPtr_GetPut_PIV}__get_word\n" +
               $"    LD ({N_proc_opPtr_GetPut_PIV}__byte+1), BC\n" +
               $"    LD ({N_proc_opPtr_GetPut_PIV}__word+1), DE\n" +
                "    \n" +
               $"    call {N_proc_opPtr_GetPut_PIV}\n" +
                "    \n" +
                "    ret\n"
        };

        public const string N_inter_opPtr_AddrOf_VP = "inter_opPtr_AddrOf_VP";
        public static AsmProc inter_opPtr_AddrOf_VP = new AsmProc() {
            opCode = "opPtr_AddrOf_VP",
            name = N_inter_opPtr_AddrOf_VP,
            dependencies = new List<string>() {
                N_proc_LD_A_next_byte_of_prog,
                N_proc_LD_HL_addrOf_varA,
                N_proc_LD_varA_BC,
            },
            text =
               $"{N_inter_opPtr_AddrOf_VP}:\n" +
                "    \n" +
               $"    call {N_proc_LD_A_next_byte_of_prog}\n" +
               $"    call {N_proc_LD_HL_addrOf_varA}\n" +
                "    PUSH HL\n" +
                "    POP BC\n" +
                "    \n" +
               $"    call {N_proc_LD_A_next_byte_of_prog}\n" +
               $"    call {N_proc_LD_varA_BC}\n" +
                "    \n" +
                "    ret\n"
        };

        #endregion INTERPRETATION PROCEDURES
    }
}
