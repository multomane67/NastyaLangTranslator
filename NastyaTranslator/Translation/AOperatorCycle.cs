﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator.Translation {
    abstract class AOperatorCycle : AOperatorNode {

        public string breakLabel = null;
        public string continueLabel = null;

        #region STATIC
        
        public static string currBreakLab    => currCycle != null ? currCycle.breakLabel    : throw new Exception("currCycle is null");
        public static string currContinueLab => currCycle != null ? currCycle.continueLabel : throw new Exception("currCycle is null");

        private static List<AOperatorCycle> cyclesStack = new List<AOperatorCycle>(); // init by one null ref
        public static AOperatorCycle currCycle {
            get {
                int cnt = cyclesStack.Count;
                if (cnt <= 0) {
                    //throw new Exception($"You don't have any cycle in stack!");
                    return null;
                } else {
                    AOperatorCycle topCycle = cyclesStack[cnt - 1];
                    if (topCycle != null) {
                        return topCycle;
                    } else {
                        //throw new Exception("The top cycle is null!");
                        return null;
                    }
                }
            }
        }

        protected static void CyclesStack_Add(AOperatorCycle opCycle) {
            if (opCycle == null) {
                throw new Exception($"AddLastCycle: try to add null cycle node!");
            } else if (cyclesStack.IndexOf(opCycle) != -1) {
                throw new Exception($"AddLastCycle: try to add existing cycle node!");
            } else if (opCycle.breakLabel.IsNullOrEmpty()) {
                throw new Exception($"AddLastCycle: opCycle.breakLabel is null or empty!");
            } else if (opCycle.continueLabel.IsNullOrEmpty()) {
                throw new Exception($"AddLastCycle: opCycle.continueLabel is null or empty!");
            } else {
                cyclesStack.Add(opCycle);
            }
        }

        protected static void CyclesStack_Remove(AOperatorCycle opCycle) {
            if (opCycle == null) {
                throw new Exception($"RemoveLastCycle: try to remove null cycle node!");
            } else if (currCycle != opCycle) {
                throw new Exception($"RemoveLastCycle: removing cycle node is not on top!");
            } else {
                cyclesStack.RemoveAt(cyclesStack.Count-1);
            }
        }

        #endregion STATIC
    }
}
