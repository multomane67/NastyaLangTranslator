﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator.Translation {
    class OperatorDoWhile : OperatorWhile {
        public OperatorDoWhile() {
            this.opName = "do_while";

            this.doWhileRepeatLabel = $"do_while{doWhileNumber}_repeat";

            this.breakLabel = $"do_while{doWhileNumber}_break";
            this.continueLabel = $"do_while{doWhileNumber}_continue";

            doWhileNumber++;
        }

        private string doWhileRepeatLabel = null;

        private static int doWhileNumber = 0;

        public override void TranslateToAsm(StringBuilder sb) {
            bool isCheckExists = IsCheckExists();

            string check_a = !string.IsNullOrEmpty(check.aConst) ? check.aConst : check.aVar;
            string check_b = !string.IsNullOrEmpty(check.bConst) ? check.bConst : check.bVar;
            string sCheck = isCheckExists ? $"{check_a} {check.comparisonOperator} {check_b}" : "";

            sb.Append($"                    ; {opName} ({sCheck}) \n");

            CyclesStack_Add(this);

            sb.Append(doWhileRepeatLabel + ":\n");

            foreach (var op in operators)
                op.TranslateToAsm(sb);

            sb.Append(this.continueLabel + ":\n");

            if (isCheckExists) {
                check.ifOperators = new List<AOperatorNode>();
                check.ifOperators.Add(new OperatorAsm { asmText = $"    JP {doWhileRepeatLabel}" });
                check.TranslateToAsm(sb);
                check.ifOperators = null; // nulling for correct repeat compiling
            } else {
                OperatorAsm jump = new OperatorAsm { asmText = $"    JP {doWhileRepeatLabel}" };
                jump.TranslateToAsm(sb);
            }

            sb.Append(this.breakLabel + ":\n");

            CyclesStack_Remove(this);
        }

        public override void TranslateToBc(StringBuilder sb, InterpretatorBuilder ib) {
            bool isCheckExists = IsCheckExists();

            string check_a = !string.IsNullOrEmpty(check.aConst) ? check.aConst : check.aVar;
            string check_b = !string.IsNullOrEmpty(check.bConst) ? check.bConst : check.bVar;
            string sCheck = isCheckExists ? $"{check_a} {check.comparisonOperator} {check_b}" : "";

            sb.Append($"                    ; {opName} ({sCheck}) \n");

            CyclesStack_Add(this);

            sb.Append(doWhileRepeatLabel + ":\n");

            foreach (var op in operators)
                op.TranslateToBc(sb, ib);

            sb.Append(this.continueLabel + ":\n");

            if (isCheckExists) {
                check.ifOperators = new List<AOperatorNode>();
                check.ifOperators.Add(
                    new OperatorByteCodeText {
                        action = () => { ib.Registrate(PBCI.inter_opJmpW); },
                        bcText = $"    DEFB {PBCI.inter_opJmpW.opCode}\n" +
                                 $"    DEFW {doWhileRepeatLabel}\n",
                    }
                );
                check.TranslateToBc(sb, ib);
                check.ifOperators = null; // nulling for correct repeat compiling
            } else {
                OperatorByteCodeText jump = new OperatorByteCodeText {
                    action = () => { ib.Registrate(PBCI.inter_opJmpW); },
                    bcText = $"    DEFB {PBCI.inter_opJmpW.opCode}\n" +
                             $"    DEFW {doWhileRepeatLabel}\n",
                };
                jump.TranslateToBc(sb, ib);
            }

            sb.Append(this.breakLabel + ":\n");

            CyclesStack_Remove(this);
        }
    }
}
