﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator.Translation {
    class OperatorCalcUnary : AOperatorNode {
        public OperatorCalcUnary(OperatorCalc ext) {
            this.opName = "calc_unary";

            string varName = ext.aVar != null ? ext.aVar : ext.bVar;

            string unarOp = ext.operation;
            unarOp = unarOp == "+" ? "++" : unarOp;
            unarOp = unarOp == "-" ? "--" : unarOp;

            this.aVar = varName;
            this.operation = unarOp;
        }

        public string aVar = null;

        public string operation = null;

        public override void TranslateToAsm(StringBuilder sb) {
            sb.Append($"                    ; {opName} {aVar} {operation}\n");

            string vaType = !string.IsNullOrEmpty(aVar) ? fn.GetVariableWithName(aVar).type : null;

            bool isByte = vaType == ST.BYTE;
            bool isWord = !isByte;

            bool isLogicShift = operation == "<<" || operation == ">>";

            if (isLogicShift) {

                // load operable in DE
                if (vaType == ST.BYTE) {
                    sb.Append($"    LD DE, ({Utils.VarNameToAsm(aVar)})\n"); // load 2 bytes from one-byte-variable
                    sb.Append($"    LD D, 0\n"); // but high byte will be ignored
                } else if (vaType == ST.WORD) {
                    sb.Append($"    LD DE, ({Utils.VarNameToAsm(aVar)})\n");
                } else {
                    throw new Exception("error");
                }

                if (operation == "<<") {
                    sb.Append($"    SLA E\n");
                    sb.Append($"    RL D\n");
                } else if (operation == ">>") {
                    sb.Append($"    SRL D\n");
                    sb.Append($"    RR E\n");
                } else {
                    throw new Exception("operation select error");
                }

                // unload bits of result to result var
                if (vaType == ST.BYTE) {
                    sb.Append($"    LD A, E\n");
                    sb.Append($"    LD ({Utils.VarNameToAsm(aVar)}), A\n");
                } else if (vaType == ST.WORD) {
                    sb.Append($"    LD ({Utils.VarNameToAsm(aVar)}), DE\n");
                } else {
                    throw new Exception("error");
                }

            } else {
                if (isByte) {
                    // BYTEs
                    sb.Append($"    LD HL, {Utils.VarNameToAsm(aVar)}\n");

                    if (!string.IsNullOrEmpty(operation)) {
                        if (operation == "++") {
                            sb.Append($"    INC (HL)\n");
                        } else if (operation == "--") {
                            sb.Append($"    DEC (HL)\n");
                        } else {
                            throw new Exception("assembling  error");
                        }
                    } else {
                        throw new Exception("assembling  error");
                    }

                } else if (isWord) {
                    // WORDs

                    sb.Append($"    LD HL, ({Utils.VarNameToAsm(aVar)})\n");

                    if (!string.IsNullOrEmpty(operation)) {
                        if (operation == "++") {
                            sb.Append($"    INC HL\n");
                        } else if (operation == "--") {
                            sb.Append($"    DEC HL\n");
                        } else {
                            throw new Exception("assembling  error");
                        }
                    } else {
                        throw new Exception("assembling  error");
                    }

                    sb.Append($"    LD ({Utils.VarNameToAsm(aVar)}), HL\n");

                } else {
                    throw new Exception($"assembling  error");
                }
            }
        }

        public override void TranslateToBc(StringBuilder sb, InterpretatorBuilder ib) {
            sb.Append($"                    ; {opName} {aVar} {operation}\n");

            string opCode = null;

            byte argA = fn.pn.GetVariableWithName(aVar).idForByteCode;

            AsmProc asmProc = null;

            if (operation == "++") {
                asmProc = PBCI.inter_opCalc_PlusPlus_A;
            } else if (operation == "--") {
                asmProc = PBCI.inter_opCalc_MinusMinus_A;
            } else if (operation == "<<") {
                asmProc = PBCI.inter_opCalc_SHL_A;
            } else if (operation == ">>") {
                asmProc = PBCI.inter_opCalc_SHR_A;
            } else {
                throw new Exception("assembling  error");
            }

            ib.Registrate(asmProc);
            opCode = asmProc.opCode;

            sb.Append($"    DEFB {opCode}, {argA}\n");
        }

        public override void CreateVarsForConsts() {
            // nothing for unary
        }

        public override void CheckOnValid() {
            throw new Exception("don't check OperatorCalcRAB!");
        }

    }
}
