﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator.Translation {
    class InterpretatorBuilder {

        private AsmProcList asmProcList = new AsmProcList();
        private InterpretationProcList interProcList = new InterpretationProcList();

        public InterpretatorBuilder() {
            Dictionary<string, AsmProc> externalDictForAllProcedures = new Dictionary<string, AsmProc>();

            asmProcList.SetExternalDictionary(externalDictForAllProcedures);
            interProcList.SetExternalDictionary(externalDictForAllProcedures);

            PBCI.InitAll();

            Registrate(PBCI.inter_main);
            Registrate(PBCI.inter_opRet);
            Registrate(PBCI.inter_opJmpW);
        }

        

        public void WriteAllTextTo(StringBuilder sb) {
            sb.Append($"    OP_CODES_COUNT EQU {interProcList.GetUsedCodesCount()}\n");
            sb.Append("\n");
            interProcList.WriteOpCodeConstsTo(sb);
            sb.Append("\n");
            WriteShortCallConstsTo(sb);
            sb.Append("\n");
            sb.Append("inter_table:\n");
            interProcList.WriteTableTo(sb);
            sb.Append("inter_table_end:\n");
            sb.Append("\n");
            sb.Append("module_short_calls:\n");
            WriteShortCallTableTo(sb);
            sb.Append("\n");
            asmProcList.WriteAllTextTo(sb);
            sb.Append("\n");
            interProcList.WriteProceduresTo(sb);
        }

        public void Registrate(AsmProc asmProc) {
            RegistrateOne(asmProc);

            List<AsmProc> deps = PBCI.GetAllDependenciesFor(asmProc);

            foreach (AsmProc dep in deps)
                RegistrateOne(dep);
        }

        private void RegistrateOne(AsmProc asmProc) {
            AsmProcList regList = null;

            if (asmProc.opCode == null) {
                regList = asmProcList;
            } else {
                regList = interProcList;
            }

            regList.Registrate(asmProc);
        }

        public int GetFreeCodesCount() {
            return interProcList.GetFreeCodesCount();
        }

        #region REMOVE TO MODULES PROCESSING/IMPLEMENTATION

        private List<string> shortCallNames = null;

        // use for clear too
        public void SetShortCalls(List<string> nameList) {
            shortCallNames = nameList;
        }

        public string GetOpCode_forShortCall(string funcName) {
            return $"opCall_{funcName}";
        }

        public void WriteShortCallConstsTo(StringBuilder sb) {
            if (shortCallNames != null) {
                int startNum = interProcList.GetUsedCodesCount();

                // gather aligning information
                int opCodeMaxLen = 0;
                int numberMaxLen = 0;
                int number = startNum;
                foreach (string funcName in shortCallNames) {
                    opCodeMaxLen = Math.Max(opCodeMaxLen, GetOpCode_forShortCall(funcName).Length);
                    numberMaxLen = Math.Max(numberMaxLen, (number++).ToString().Length);
                }

                sb.Append("    ; general module short calls consts\n");
                number = startNum;
                foreach (string funcName in shortCallNames) {
                    string allignedOpCode = GetOpCode_forShortCall(funcName).AlignBySpacesLeft(opCodeMaxLen);
                    string allignedNumber = (number++).ToString().AlignBySpacesRight(numberMaxLen);
                    sb.Append($"    {allignedOpCode} EQU {allignedNumber}\n");
                }
            }
        }

        public void WriteShortCallTableTo(StringBuilder sb) {
            if (shortCallNames != null) {
                foreach (string funcName in shortCallNames) {
                    sb.Append($"    DEFW {Utils.FuncNameToAsm(funcName)}\n");
                }
            }
        }

        #endregion REMOVE TO MODULES PROCESSING/IMPLEMENTATION

        #region NESTED CLASSES

        public class AsmProcList {
            protected List<AsmProc> procList = new List<AsmProc>();  // i want to save order of adding procedures
            protected Dictionary<string, AsmProc> procDict = new Dictionary<string, AsmProc>();

            public virtual void SetExternalDictionary(Dictionary<string, AsmProc> externalDict) {
                procDict = externalDict;
            }

            public virtual void Registrate(AsmProc asmProc) {
                if (!IsContains(asmProc)) {
                    Add(asmProc);
                }
            }

            protected virtual bool IsContains(AsmProc asmProc) {
                return procDict.ContainsKey(asmProc.name);
            }

            protected virtual bool IsContains(string procName) {
                return procDict.ContainsKey(procName);
            }

            protected virtual void Add(AsmProc newAsmProc) {
                if (newAsmProc == null)
                    throw new Exception("newAsmProc is null!");

                if (!IsContains(newAsmProc)) {
                    procList.Add(newAsmProc);
                    procDict.Add(newAsmProc.name, newAsmProc);
                } else {
                    throw new Exception($"newAsmProc with name '{newAsmProc.name}' is already added");
                }
            }

            public virtual void WriteAllTextTo(StringBuilder sb) {
                foreach (AsmProc asmProc in procList) {
                    sb.Append(asmProc.text);
                    sb.Append("\n");
                }
            }
        }

        public class InterpretationProcList : AsmProcList {

            const int OP_CODES_MAX = 256;

            protected override void Add(AsmProc newAsmProc) {
                if (procList.Count < OP_CODES_MAX) {
                    base.Add(newAsmProc);
                } else {
                    throw new Exception("you try to add to many procedures for interpretation");
                }
            }

            public virtual void WriteOpCodeConstsTo(StringBuilder sb) {
                // gather aligning information
                int opCodeMaxLen = 0;
                int numberMaxLen = 0;
                int number = 0;
                foreach (AsmProc asmProc in procList) {
                    opCodeMaxLen = Math.Max(opCodeMaxLen, asmProc.opCode.Length);
                    numberMaxLen = Math.Max(numberMaxLen, (number++).ToString().Length);
                }

                // write opCode constants table
                number = 0;
                foreach (AsmProc asmProc in procList) {
                    string opCode = asmProc.opCode.AlignBySpacesLeft(opCodeMaxLen);
                    string numberVal = (number++).ToString().AlignBySpacesRight(numberMaxLen);
                    sb.Append($"    {opCode} EQU {numberVal}\n");
                }
            }

            public virtual void WriteTableTo(StringBuilder sb) {
                foreach (AsmProc asmProc in procList) {
                    sb.Append($"    DEFW {asmProc.name}\n");
                }
            }

            public virtual void WriteProceduresTo(StringBuilder sb) {
                base.WriteAllTextTo(sb);
            }

            public override void WriteAllTextTo(StringBuilder sb) {
                throw new Exception("Don't use this func! Use other write funcs of this class!");
            }

            public int GetFreeCodesCount() {
                int usedCodes = procList.Count;
                return OP_CODES_MAX - usedCodes;
            }

            public int GetUsedCodesCount() {
                return procList.Count;
            }
        }

        #endregion NESTED CLASSES
    }
}
