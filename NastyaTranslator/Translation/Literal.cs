﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator.Translation {
    class Literal {
        private const char sFiBr = '(';
        private const char sLaBr = ')';

        public static string GetAsmValue(string constStr) {
            string typedefStr = GetTypeOfCast(constStr);
            if (!typedefStr.IsNullOrEmpty()) {
                return constStr.Replace($"{sFiBr}{typedefStr}{sLaBr}", "");
            } else {
                return constStr;
            }
        }

        public static string GetTypeOfCast(string constStr) {
            int firstBracketIndex = constStr.IndexOf(sFiBr);
            int lastBracketIndex = constStr.IndexOf(sLaBr);

            if (firstBracketIndex == -1 && lastBracketIndex == -1) {
                return null;
            } else if (firstBracketIndex == -1 || lastBracketIndex == -1) {
                if (firstBracketIndex == -1)
                    throw new Exception("Const have only last typedef bracket without first.");
                if (lastBracketIndex == -1)
                    throw new Exception("Const have only first typedef bracket without last.");
            } else if (firstBracketIndex > lastBracketIndex) {
                throw new Exception("Const have disposition of typedef brackets.");
            } else {
                // if other verification was succes then we can take the type string
                string typedefStr = constStr.Substring(firstBracketIndex + 1, lastBracketIndex-firstBracketIndex-1);

                if (typedefStr != ST.BYTE && typedefStr != ST.WORD)
                    throw new Exception($"Const have unknown typedef string '({typedefStr})'.");

                return typedefStr;
            }

            return null;
        }

        public static string GetTypeForConstStorage(string strConst) {
            string defType = Literal.GetTypeOfCast(strConst);
            if (defType != null) {
                return defType;
            } else {
                if (Int32.TryParse(strConst, out int val)) {
                    return ST.GetMinimumTypeForStorage(val);
                } else {
                    throw new Exception("Assembly error!");
                }
            }
        }
    }
}
