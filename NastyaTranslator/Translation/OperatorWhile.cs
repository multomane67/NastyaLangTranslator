﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator.Translation {
    class OperatorWhile : AOperatorCycle {
        public OperatorWhile() {
            this.opName = "while";

            this.whileRepeatLabel = $"while{whileNumber}_repeat";

            this.breakLabel = $"while{whileNumber}_break";
            this.continueLabel = $"while{whileNumber}_continue";

            whileNumber++;
        }

        private string whileRepeatLabel = null;

        private static int whileNumber = 0;

        public OperatorIf check = null;

        public List<AOperatorNode> operators = new List<AOperatorNode>();

        public override void Init(FuncNode fn) {
            base.Init(fn);

            check.Init(fn);

            foreach (AOperatorNode on in operators)
                on.Init(fn);
        }

        protected bool IsCheckExists() {
            return check.aVar != null || check.aConst != null || check.comparisonOperator != null || check.bVar != null || check.bConst != null;
        }

        public override void CheckOnValid() {
            CheckLoc();

            if (IsCheckExists()) {
                try {
                    check.CheckOnValid();
                } catch (Exception e) {
                    string message = e.Message.Replace(" 'if'", "");
                    throw new MessageForUserException($"{LogPrefix} error in condition:\n {message}");
                }
            }

            CyclesStack_Add(this);

            foreach (var op in operators) {
                op.CheckOnValid();
            }

            CyclesStack_Remove(this);
        }

        public override void TranslateToAsm(StringBuilder sb) {
            bool isCheckExists = IsCheckExists();

            string check_a = !string.IsNullOrEmpty(check.aConst) ? check.aConst : check.aVar;
            string check_b = !string.IsNullOrEmpty(check.bConst) ? check.bConst : check.bVar;
            string sCheck = isCheckExists ? $"{check_a} {check.comparisonOperator} {check_b}" : "";

            sb.Append($"                    ; {opName} ({sCheck}) \n");

            CyclesStack_Add(this);

            List<AOperatorNode> insideOperators = new List<AOperatorNode>(operators);

            sb.Append(whileRepeatLabel + ":\n");
            sb.Append(this.continueLabel + ":\n");

            insideOperators.Add(new OperatorAsm { asmText = $"    JP {whileRepeatLabel}" });

            if (isCheckExists) {
                check.ifOperators = insideOperators;
                check.TranslateToAsm(sb);
                check.ifOperators = new List<AOperatorNode>(); // for correct repeat compiling
            } else {
                foreach (var op in insideOperators)
                    op.TranslateToAsm(sb);
            }

            sb.Append(this.breakLabel + ":\n");

            CyclesStack_Remove(this);
        }

        public override void TranslateToBc(StringBuilder sb, InterpretatorBuilder ib) {
            bool isCheckExists = IsCheckExists();

            string check_a = !string.IsNullOrEmpty(check.aConst) ? check.aConst : check.aVar;
            string check_b = !string.IsNullOrEmpty(check.bConst) ? check.bConst : check.bVar;
            string sCheck = isCheckExists ? $"{check_a} {check.comparisonOperator} {check_b}" : "";

            sb.Append($"                    ; {opName} ({sCheck}) \n");

            CyclesStack_Add(this);

            List<AOperatorNode> insideOperators = new List<AOperatorNode>(operators);

            sb.Append(whileRepeatLabel + ":\n");
            sb.Append(this.continueLabel + ":\n");

            insideOperators.Add(
                new OperatorByteCodeText {
                    action = ()=> { ib.Registrate(PBCI.inter_opJmpW); },
                    bcText = $"    DEFB {PBCI.inter_opJmpW.opCode}\n" +
                             $"    DEFW {whileRepeatLabel}\n",
                }
            );

            if (isCheckExists) {
                check.ifOperators = insideOperators;
                check.TranslateToBc(sb, ib);
                check.ifOperators = null; // nulling for correct repeat compiling
            } else {
                foreach (var op in insideOperators)
                    op.TranslateToBc(sb, ib);
            }

            sb.Append(this.breakLabel + ":\n");

            CyclesStack_Remove(this);
        }

        public override void CreateVarsForConsts() {
            if (IsCheckExists()) {
                check.CreateVarsForConsts();
            }

            foreach (AOperatorNode op in operators) {
                op.CreateVarsForConsts();
            }
        }
    }
}
