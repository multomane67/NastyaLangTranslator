﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NastyaTranslator.Translation {
    class OperatorCalcRAB : AOperatorNode 
    {
        public OperatorCalcRAB(OperatorCalc ext) {
            this.opName = "calc_rab";

            this.rVar = ext.rVar;
            this.aVar = ext.aVar;
            this.aConst = ext.aConst;
            this.operation = ext.operation;
            this.bVar = ext.bVar;
            this.bConst = ext.bConst;
        }

        public string rVar = null;

        public string aVar = null;
        public string aConst = null;

        public string operation = null;

        public string bVar = null;
        public string bConst = null;

        private const string shiftLeft = "<<";      // means <<
        private const string shiftRight = ">>";     // means >>

        private static HashSet<string> posibleOps = new HashSet<string>() {
            "+", "-", shiftLeft, shiftRight
        };

        private int logicShiftNumber {
            get => OperatorCalc.logicShiftNumber;
            set => OperatorCalc.logicShiftNumber = value;
        }

        public override void TranslateToAsm(StringBuilder sb) {
            string aOperand = !string.IsNullOrEmpty(aConst) ? aConst : !string.IsNullOrEmpty(aVar) ? aVar : "undefined";
            string bOperand = !string.IsNullOrEmpty(bConst) ? bConst : !string.IsNullOrEmpty(bVar) ? bVar : "undefined";
            sb.Append($"                    ; {opName} {rVar} = {aOperand} {operation} {bOperand} \n");

            string vaType = !string.IsNullOrEmpty(aVar) ? fn.GetVariableWithName(aVar).type : null;
            string vbType = !string.IsNullOrEmpty(bVar) ? fn.GetVariableWithName(bVar).type : null;
            string caType = !string.IsNullOrEmpty(aConst) ? Literal.GetTypeForConstStorage(aConst) : null;
            string cbType = !string.IsNullOrEmpty(bConst) ? Literal.GetTypeForConstStorage(bConst) : null;

            string aType = !vaType.IsNullOrEmpty() ? vaType : caType;
            string bType = !vbType.IsNullOrEmpty() ? vbType : cbType;

            Variable vr = !string.IsNullOrEmpty(rVar) ? fn.GetVariableWithName(rVar) : null;

            bool isByte = vr.type == ST.BYTE && aType == ST.BYTE && bType == ST.BYTE;
            bool isWord = !isByte;

            bool isLogicShift = operation == shiftLeft || operation == shiftRight;

            if (isLogicShift) {
                string logicShiftLabel_repeat = $"logicShift{logicShiftNumber}_repeat";
                string logicShiftLabel_skipIfZero = $"logicShift{logicShiftNumber}_skipIfZero";
                logicShiftNumber++;

                // load cycle counter in B
                if (!bVar.IsNullOrEmpty()) {
                    // equal for both var types and B
                    if (vbType == ST.BYTE) {
                        sb.Append($"    LD A, ({Utils.VarNameToAsm(bVar)})\n");
                        sb.Append($"    LD B, A\n");
                    } else if (vbType == ST.WORD) {
                        sb.Append($"    LD A, ({Utils.VarNameToAsm(bVar)})\n");
                        sb.Append($"    LD B, A\n"); // will be truncated to lower byte, (lower is first, second will be ignored)
                    } else {
                        throw new Exception("error");
                    }
                } else if (!bConst.IsNullOrEmpty()) {
                    string cbAsmVal = Literal.GetAsmValue(bConst);

                    if (Int64.Parse(cbAsmVal) == 0f) {
                        Console.WriteLine($"WARNING: some calc try to make logic shift with zero const counter!");
                    }

                    if (cbType == ST.BYTE) {
                        sb.Append($"    LD B, {cbAsmVal}\n");
                    } else if (cbType == ST.WORD) {
                        sb.Append($"    LD HL, {cbAsmVal}\n");
                        sb.Append($"    LD B, L\n");
                    } else {
                        throw new Exception("error");
                    }
                } else {
                    throw new Exception("error");
                }

                // load operable in DE
                if (!aVar.IsNullOrEmpty()) {
                    if (vaType == ST.BYTE) {
                        sb.Append($"    LD DE, ({Utils.VarNameToAsm(aVar)})\n"); // load 2 bytes from one-byte-variable
                        sb.Append($"    LD D, 0\n"); // but high byte will be ignored
                    } else if (vaType == ST.WORD) {
                        sb.Append($"    LD DE, ({Utils.VarNameToAsm(aVar)})\n");
                    } else {
                        throw new Exception("error");
                    }
                } else if (!aConst.IsNullOrEmpty()) {
                    string caAsmVal = Literal.GetAsmValue(aConst);
                    // equal for both consts types and DE
                    if (caType == ST.BYTE) {
                        sb.Append($"    LD DE, {caAsmVal}\n");
                    } else if (caType == ST.WORD) {
                        sb.Append($"    LD DE, {caAsmVal}\n");
                    } else {
                        throw new Exception("error");
                    }
                } else {
                    throw new Exception("error");
                }

                sb.Append($"    LD A, 0\n");
                sb.Append($"    CP B\n");
                sb.Append($"    JR Z, {logicShiftLabel_skipIfZero}\n");

                sb.Append($"{logicShiftLabel_repeat}:\n");

                if (operation == shiftLeft) {
                    sb.Append($"    SLA E\n");
                    sb.Append($"    RL D\n");
                    sb.Append($"    DJNZ {logicShiftLabel_repeat}\n");
                } else if (operation == shiftRight) {
                    sb.Append($"    SRL D\n");
                    sb.Append($"    RR E\n");
                    sb.Append($"    DJNZ {logicShiftLabel_repeat}\n");
                } else {
                    throw new Exception("operation select error");
                }

                sb.Append($"{logicShiftLabel_skipIfZero}:\n");

                // unload bits of result to result var
                if (vr.type == ST.BYTE) {
                    sb.Append($"    LD A, E\n");
                    sb.Append($"    LD ({Utils.VarNameToAsm(rVar)}), A\n");
                } else if (vr.type == ST.WORD) {
                    sb.Append($"    LD ({Utils.VarNameToAsm(rVar)}), DE\n");
                } else {
                    throw new Exception("error");
                }

            } else {
                if (isByte) {
                    // BYTEs
                    if (!string.IsNullOrEmpty(bConst)) {
                        sb.Append($"    LD A, {Literal.GetAsmValue(bConst)} \n");
                    } else if (!string.IsNullOrEmpty(bVar)) {
                        sb.Append($"    LD A, ({Utils.VarNameToAsm(bVar)}) \n");
                    } else {
                        throw new Exception("assembling  error");
                    }
                    sb.Append($"    LD B, A \n");

                    if (!string.IsNullOrEmpty(aConst)) {
                        sb.Append($"    LD A, {Literal.GetAsmValue(aConst)} \n");
                    } else if (!string.IsNullOrEmpty(aVar)) {
                        sb.Append($"    LD A, ({Utils.VarNameToAsm(aVar)}) \n");
                    } else {
                        throw new Exception("assembling  error");
                    }

                    if (!string.IsNullOrEmpty(operation)) {
                        if (operation == "+") {
                            sb.Append($"    ADD A, B \n");
                        } else if (operation == "-") {
                            sb.Append($"    SUB B \n");
                        } else {
                            throw new Exception("assembling  error");
                        }
                    } else {
                        throw new Exception("assembling  error");
                    }

                    if (!string.IsNullOrEmpty(rVar)) {
                        sb.Append($"    LD ({Utils.VarNameToAsm(rVar)}), A \n");
                    } else {
                        throw new Exception("assembling  error");
                    }
                } else if (isWord) {
                    // WORDs
                    if (!string.IsNullOrEmpty(aConst)) {
                        sb.Append($"    LD HL, {Literal.GetAsmValue(aConst)} \n");
                    } else if (!string.IsNullOrEmpty(aVar)) {
                        if (vaType == ST.BYTE) {
                            sb.Append($"    LD HL, ({Utils.VarNameToAsm(aVar)}) \n"); // load 2 bytes
                            sb.Append($"    LD H, 0 \n"); // but only lower will be used.
                        } else if (vaType == ST.WORD) {
                            sb.Append($"    LD HL, ({Utils.VarNameToAsm(aVar)}) \n");
                        } else {
                            throw new Exception("assembling  error");
                        }
                    } else {
                        throw new Exception("assembling  error");
                    }

                    if (!string.IsNullOrEmpty(bConst)) {
                        sb.Append($"    LD BC, {Literal.GetAsmValue(bConst)} \n");
                    } else if (!string.IsNullOrEmpty(bVar)) {
                        if (vbType == ST.BYTE) {
                            sb.Append($"    LD BC, ({Utils.VarNameToAsm(bVar)}) \n"); // load 2 bytes
                            sb.Append($"    LD B, 0 \n"); // but only lower will be used.
                        } else if (vbType == ST.WORD) {
                            sb.Append($"    LD BC, ({Utils.VarNameToAsm(bVar)}) \n");
                        } else {
                            throw new Exception("assembling  error");
                        }
                    } else {
                        throw new Exception("assembling  error");
                    }

                    if (!string.IsNullOrEmpty(operation)) {
                        if (operation == "+") {
                            sb.Append($"    ADD HL, BC \n");
                        } else if (operation == "-") {
                            sb.Append($"    AND A \n");
                            sb.Append($"    SBC HL, BC \n");
                        } else {
                            throw new Exception("assembling  error");
                        }
                    } else {
                        throw new Exception("assembling  error");
                    }

                    if (!string.IsNullOrEmpty(rVar)) {
                        if (vr.type == ST.BYTE) {
                            sb.Append($"    LD A, L \n");
                            sb.Append($"    LD ({Utils.VarNameToAsm(rVar)}), A \n");
                        } else if (vr.type == ST.WORD) {
                            sb.Append($"    LD ({Utils.VarNameToAsm(rVar)}), HL \n");
                        } else {
                            throw new Exception("assembling  error");
                        }
                    } else {
                        throw new Exception("assembling  error");
                    }
                } else {
                    throw new Exception($"assembling  error r='{rVar}'({vr.type}) av='{aOperand}'({vaType}) bv='{bOperand}'({vbType}) ");
                }
            }
        }

        public override void TranslateToBc(StringBuilder sb, InterpretatorBuilder ib) {
            string aOperand = !string.IsNullOrEmpty(aConst) ? aConst : !string.IsNullOrEmpty(aVar) ? aVar : "undefined";
            string bOperand = !string.IsNullOrEmpty(bConst) ? bConst : !string.IsNullOrEmpty(bVar) ? bVar : "undefined";
            sb.Append($"                    ; {opName} {rVar} = {aOperand} {operation} {bOperand} \n");

            string opCode = null;

            byte argA = fn.pn.GetVariableWithName(aVar).idForByteCode;
            byte argB = fn.pn.GetVariableWithName(bVar).idForByteCode;
            byte argR = fn.pn.GetVariableWithName(rVar).idForByteCode;

            if (operation == "+") {
                ib.Registrate(PBCI.inter_opCalc_Plus_ABR);
                opCode = PBCI.inter_opCalc_Plus_ABR.opCode;
            } else if (operation == "-") {
                ib.Registrate(PBCI.inter_opCalc_Minus_ABR);
                opCode = PBCI.inter_opCalc_Minus_ABR.opCode;
            } else if (operation == shiftLeft) {
                ib.Registrate(PBCI.inter_opCalc_SHL_ABR);
                opCode = PBCI.inter_opCalc_SHL_ABR.opCode;
            } else if (operation == shiftRight) {
                ib.Registrate(PBCI.inter_opCalc_SHR_ABR);
                opCode = PBCI.inter_opCalc_SHR_ABR.opCode;
            } else {
                throw new Exception("assembling  error");
            }

            sb.Append($"    DEFB {opCode}, {argA}, {argB}, {argR}\n");
        }

        public override void CreateVarsForConsts() {
            this.fn.pn.ExchangeConstToVar(ref aConst, ref aVar, this);
            this.fn.pn.ExchangeConstToVar(ref bConst, ref bVar, this);
        }

        public override void CheckOnValid() {
            throw new Exception("don't check OperatorCalcRAB!");
        }
    }
}
