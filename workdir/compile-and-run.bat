echo off

setlocal

set ifnam=quick-test
set ifext=n
set tm=bc

set ifn=%ifnam%.%ifext%

set asmfn=%ifnam%-%tm%.asm
set cmpfn=%ifnam%-%tm%-zxcmp.bin
set runfn=%ifnam%-%tm%-zxrun.tap



del %asmfn%
del %cmpfn%
del %runfn%

"nt" -%tm% %ifn% %ifnam%2.n %asmfn%


"../../compilers/pasmo-0.5.3/pasmo.exe"           %asmfn%  %cmpfn%
"../../compilers/pasmo-0.5.3/pasmo.exe" --tapbas  %asmfn%  %runfn%

%runfn%

endlocal

echo on