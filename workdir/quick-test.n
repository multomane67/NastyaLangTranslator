[MaxNumberOfVars(240)]
namespace Main {
	
	byte str[] = "-\"-\'-\\-\n-\x411-\x48ello!-\ns-\x5E-e\n";
	
	byte c = 0;
	byte i = 0;

	word a[10] = { 'A' , 'B' , 'C', 0xA , 0xB , 1 , 22 , 333 };

	void Main() {
		do {
			c = str[i];

			if (c != 0) asm 
			{
				LD A, (_c)
				RST 16
			}
	
			i++;
		} while (c != 0);

		for (i = (word)'0'; i < (word)':'; i ++)
			asm {
				LD A, (_i)
				RST 16
			}

		test1();
		test2();
	}
	
}


namespace Main {
	void test1() {
		asm {
			LD A, "a"
			RST 16
		}
	}
}