    ORG 32768

    OP_CODES_COUNT EQU 15

    opRet                EQU  0
    opJmpW               EQU  1
    opIf_Equal_AB        EQU  2
    opIf_NotEqual_AB     EQU  3
    opSet_AR             EQU  4
    opCalc_SHL_ABR       EQU  5
    opCalc_Plus_ABR      EQU  6
    opPtr_Put_PV         EQU  7
    opPtr_Get_PV         EQU  8
    opCalc_Minus_ABR     EQU  9
    opAsmW               EQU 10
    opIf_GreaterEqual_AB EQU 11
    opPtr_Put_PIV        EQU 12
    opPtr_Get_PIV        EQU 13
    opPtr_AddrOf_VP      EQU 14

    ; general module short calls consts
    opCall_draw_attr      EQU 15
    opCall_set_dir_by_key EQU 16
    opCall_calc_attr_ptr  EQU 17
    opCall_print_scores   EQU 18
    opCall_reset_game     EQU 19
    opCall_apply_dir      EQU 20
    opCall_ptr_mult_32    EQU 21
    opCall_clear_screen   EQU 22
    opCall_get_attr       EQU 23

inter_table:
    DEFW inter_opRet
    DEFW inter_opJmpW
    DEFW inter_opIf_Equal_AB
    DEFW inter_opIf_NotEqual_AB
    DEFW inter_opSet_AR
    DEFW inter_opCalc_SHL_ABR
    DEFW inter_opCalc_Plus_ABR
    DEFW inter_opPtr_Put_PV
    DEFW inter_opPtr_Get_PV
    DEFW inter_opCalc_Minus_ABR
    DEFW inter_opAsmW
    DEFW inter_opIf_GreaterEqual_AB
    DEFW inter_opPtr_Put_PIV
    DEFW inter_opPtr_Get_PIV
    DEFW inter_opPtr_AddrOf_VP
inter_table_end:

module_short_calls:
    DEFW func_draw_attr
    DEFW func_set_dir_by_key
    DEFW func_calc_attr_ptr
    DEFW func_print_scores
    DEFW func_reset_game
    DEFW func_apply_dir
    DEFW func_ptr_mult_32
    DEFW func_clear_screen
    DEFW func_get_attr

inter_main:
    
inter_main__repeat:
    call proc_LD_A_next_byte_of_prog
    LD B, OP_CODES_COUNT
    LD C, A
    SUB B
    JR NC, inter_main__short_call
    
inter_main__inter_call:
    LD A, C
    LD BC, inter_table
    call N_proc_LD_DE_from_word_table_BC_by_index_A
    call proc_CALL_asm_at_DE
    JR inter_main__jump_to_repeat
    
inter_main__short_call:
    LD BC, module_short_calls
    call N_proc_LD_DE_from_word_table_BC_by_index_A
    call proc_CALL_ByteCode_at_DE
    
inter_main__jump_to_repeat:
inter_main__erase_for_halt:
    JR inter_main__repeat
    
    ret

proc_LD_A_next_byte_of_prog:
    
    PUSH HL
    
    LD HL, (curr_exec_addr)
    LD A, (HL)
    INC HL
    LD (curr_exec_addr), HL
    
    POP HL
    
    ret

proc_CALL_asm_at_DE:
    
    LD (proc_CALL_asm_at_DE__call+1), DE
proc_CALL_asm_at_DE__call:
    call 0000
    
    ret

proc_CALL_ByteCode_at_DE:
    
    PUSH AF
    PUSH BC
    PUSH HL
    
    LD BC, 65534
    LD HL, (bc_callstack_top)
    PUSH HL
    AND A
    SBC HL, BC
    POP HL
    JR NC, proc_CALL_ByteCode_at_DE__stack_overflow
    LD BC, (curr_exec_addr)
    LD (HL), C
    INC HL
    LD (HL), B
    INC HL
    LD (bc_callstack_top), HL
    LD (curr_exec_addr), DE
    
    JR proc_CALL_ByteCode_at_DE__exit
proc_CALL_ByteCode_at_DE__stack_overflow:
    
    LD A, 2
    call 5633
    LD A, '#'
    rst 16
    LD A, '1'
    rst 16
    
    LD BC, 0000
    LD (inter_main__erase_for_halt), BC
    
proc_CALL_ByteCode_at_DE__exit:
    POP HL
    POP BC
    POP AF
    
    ret

N_proc_LD_DE_from_word_table_BC_by_index_A:
    
    PUSH HL
    
    LD H, 0
    LD L, A
    SLA L
    RL H
    ADD HL, BC
    LD E, (HL)
    INC HL
    LD D, (HL)
    
    POP HL
    
    ret

proc_LD_BC_next_word_of_prog:
    
    PUSH HL
    
    LD HL, (curr_exec_addr)
    LD C, (HL)
    INC HL
    LD B, (HL)
    INC HL
    LD (curr_exec_addr), HL
    
    POP HL
    
    ret

proc_opIf_Compare_AB:
    
    call proc_LD_A_next_byte_of_prog
    call proc_LD_BC_varA
    PUSH BC
    call proc_LD_A_next_byte_of_prog
    call proc_LD_BC_varA
    PUSH BC
    call proc_LD_BC_next_word_of_prog
    PUSH BC
    
    POP DE
    POP BC
    POP HL
    
    AND A
    SBC HL, BC
proc_opIf_Compare_AB__jmp_op:
    JR Z, proc_opIf_Compare_AB__exit
    LD (curr_exec_addr), DE
    
proc_opIf_Compare_AB__exit:
    ret

proc_LD_BC_varA:
    
    PUSH HL
    
    call proc_LD_HL_addrOf_varA
    LD C, (HL)
    CP addr_gv_word_table - addr_gv_byte_table
    JR NC, proc_LD_BC_varA__word  ;jump if A >= count
;load 1 byte by addr
    LD B, 0
    JR proc_LD_BC_varA__exit    
proc_LD_BC_varA__word:;load 2 bytes by addr
    INC HL
    LD B, (HL)
    
proc_LD_BC_varA__exit:
    
    POP HL
    
    ret

proc_LD_HL_addrOf_varA:
    
    PUSH BC
    PUSH AF
    
    LD H, 0
    
    LD C, addr_gv_word_table - addr_gv_byte_table  ;byte vars count
    CP C
    JR NC, proc_LD_HL_addrOf_varA__word_table  ;jump if A >= C
;calc like byte addr
    LD L, A
    LD BC, addr_gv_byte_table
    ADD HL, BC
    JR proc_LD_HL_addrOf_varA__exit    
proc_LD_HL_addrOf_varA__word_table:
;calc like word addr
    SUB C
    LD L, A
    SLA L
    RL H
    LD BC, addr_gv_word_table
    ADD HL, BC
proc_LD_HL_addrOf_varA__exit:
    POP AF
    POP BC
    
    ret

proc_LD_varA_BC:
    
    PUSH HL
    
    call proc_LD_HL_addrOf_varA
    LD (HL), C
    CP addr_gv_word_table - addr_gv_byte_table
    JR C, proc_LD_varA_BC__exit  ;jump if A < count
    INC HL
    LD (HL), B
    
proc_LD_varA_BC__exit:
    
    POP HL
    
    ret

proc_CALL_DE_for_ABR:
    
    PUSH AF
    PUSH BC
    PUSH HL
    
    call proc_LD_A_next_byte_of_prog
    call proc_LD_BC_varA
    
    PUSH BC
    POP HL
    
    call proc_LD_A_next_byte_of_prog
    call proc_LD_BC_varA
    
    call proc_CALL_asm_at_DE
    
    PUSH HL
    POP BC
    
    call proc_LD_A_next_byte_of_prog
    call proc_LD_varA_BC
    
    POP HL
    POP BC
    POP AF
    
    ret

proc_SHL_HL_BC:
    
    PUSH AF
    PUSH BC
    
    LD B, C  ; DJNZ works with B only. higher byte stored in B is not needed for counting only lower C.
    LD A, 0
    CP B
    JR Z, proc_SHL_HL_BC__skipIfZero
    
proc_SHL_HL_BC__repeat:
    SLA L
    RL H
    DJNZ proc_SHL_HL_BC__repeat
    
proc_SHL_HL_BC__skipIfZero:
    
    POP BC
    POP AF
    
    ret

proc_ADD_HL_BC:
    
    ADD HL, BC
    
    ret

proc_opPtr_GetPut_PV:
    
    call proc_LD_A_next_byte_of_prog
    call proc_LD_BC_varA
    PUSH BC
    POP HL
    
    call proc_LD_A_next_byte_of_prog
    CP addr_gv_word_table - addr_gv_byte_table
    JR NC, proc_opPtr_GetPut_PV__word
    
proc_opPtr_GetPut_PV__byte:
    JP proc_opPtr_GetPut_PV__put_byte
proc_opPtr_GetPut_PV__put_byte:
    call proc_LD_BC_varA
    LD (HL), C
    JR proc_opPtr_GetPut_PV__exit
proc_opPtr_GetPut_PV__get_byte:
    LD C, (HL)
    call proc_LD_varA_BC
    JR proc_opPtr_GetPut_PV__exit
    
proc_opPtr_GetPut_PV__word:
    JP proc_opPtr_GetPut_PV__put_word
proc_opPtr_GetPut_PV__put_word:
    call proc_LD_BC_varA
    LD (HL), C
    INC HL
    LD (HL), B
    JR proc_opPtr_GetPut_PV__exit
proc_opPtr_GetPut_PV__get_word:
    LD C, (HL)
    INC HL
    LD B, (HL)
    call proc_LD_varA_BC
    
proc_opPtr_GetPut_PV__exit:
    
    ret

proc_SUB_HL_BC:
    
    PUSH AF
    
    AND A 
    SBC HL, BC 
    
    POP AF
    
    ret

proc_opPtr_GetPut_PIV:
    
    call proc_LD_A_next_byte_of_prog
    call proc_LD_BC_varA
    PUSH BC
    POP HL
    
    call proc_LD_A_next_byte_of_prog
    call proc_LD_BC_varA
    PUSH BC
    POP DE
    
    call proc_LD_A_next_byte_of_prog
    CP addr_gv_word_table - addr_gv_byte_table
    JR NC, proc_opPtr_GetPut_PIV__word
    
proc_opPtr_GetPut_PIV__byte:
    JP proc_opPtr_GetPut_PIV__put_byte
proc_opPtr_GetPut_PIV__put_byte:
    call proc_LD_BC_varA
    ADD HL, DE
    LD (HL), C
    JR proc_opPtr_GetPut_PIV__exit
proc_opPtr_GetPut_PIV__get_byte:
    ADD HL, DE
    LD C, (HL)
    call proc_LD_varA_BC
    JR proc_opPtr_GetPut_PIV__exit
    
proc_opPtr_GetPut_PIV__word:
    JP proc_opPtr_GetPut_PIV__put_word
proc_opPtr_GetPut_PIV__put_word:
    call proc_LD_BC_varA
    SLA E
    RL D
    ADD HL, DE
    LD (HL), C
    INC HL
    LD (HL), B
    JR proc_opPtr_GetPut_PIV__exit
proc_opPtr_GetPut_PIV__get_word:
    SLA E
    RL D
    ADD HL, DE
    LD C, (HL)
    INC HL
    LD B, (HL)
    call proc_LD_varA_BC
    
proc_opPtr_GetPut_PIV__exit:
    
    ret


inter_opRet:
    
    LD HL, (bc_callstack_top)
    DEC HL
    DEC HL
    LD (bc_callstack_top), HL
    
    LD BC, bc_callstack
    PUSH HL
    AND A
    SBC HL, BC
    POP HL
    JR C, inter_opRet__exit_program
    
    LD C, (HL)
    INC HL
    LD B, (HL)
    
    LD (curr_exec_addr), BC
    
    ret
inter_opRet__exit_program:
    LD BC, 0
    LD (inter_main__erase_for_halt), BC
    ret

inter_opJmpW:
    call proc_LD_BC_next_word_of_prog
    LD (curr_exec_addr), BC
    
    ret

inter_opIf_Equal_AB:
    
    LD A, #28
    LD (proc_opIf_Compare_AB__jmp_op), A
    call proc_opIf_Compare_AB
    ret

inter_opIf_NotEqual_AB:
    
    LD A, #20
    LD (proc_opIf_Compare_AB__jmp_op), A
    call proc_opIf_Compare_AB
    ret

inter_opSet_AR:
    
    call proc_LD_A_next_byte_of_prog
    call proc_LD_BC_varA
    call proc_LD_A_next_byte_of_prog
    call proc_LD_varA_BC
    
    ret

inter_opCalc_SHL_ABR:
    
    LD DE, proc_SHL_HL_BC
    call proc_CALL_DE_for_ABR
    
    ret

inter_opCalc_Plus_ABR:
    
    LD DE, proc_ADD_HL_BC
    call proc_CALL_DE_for_ABR
    
    ret

inter_opPtr_Put_PV:
    
    LD BC, proc_opPtr_GetPut_PV__put_byte
    LD DE, proc_opPtr_GetPut_PV__put_word
    LD (proc_opPtr_GetPut_PV__byte+1), BC
    LD (proc_opPtr_GetPut_PV__word+1), DE
    
    call proc_opPtr_GetPut_PV
    
    ret

inter_opPtr_Get_PV:
    
    LD BC, proc_opPtr_GetPut_PV__get_byte
    LD DE, proc_opPtr_GetPut_PV__get_word
    LD (proc_opPtr_GetPut_PV__byte+1), BC
    LD (proc_opPtr_GetPut_PV__word+1), DE
    
    call proc_opPtr_GetPut_PV
    
    ret

inter_opCalc_Minus_ABR:
    
    LD DE, proc_SUB_HL_BC
    call proc_CALL_DE_for_ABR
    
    ret

inter_opAsmW:
    call proc_LD_BC_next_word_of_prog
    PUSH BC
    LD DE, (curr_exec_addr)
    call proc_CALL_asm_at_DE
    POP DE
    LD (curr_exec_addr), DE
    
    ret

inter_opIf_GreaterEqual_AB:
    
    LD A, #30
    LD (proc_opIf_Compare_AB__jmp_op), A
    call proc_opIf_Compare_AB
    ret

inter_opPtr_Put_PIV:
    
    LD BC, proc_opPtr_GetPut_PIV__put_byte
    LD DE, proc_opPtr_GetPut_PIV__put_word
    LD (proc_opPtr_GetPut_PIV__byte+1), BC
    LD (proc_opPtr_GetPut_PIV__word+1), DE
    
    call proc_opPtr_GetPut_PIV
    
    ret

inter_opPtr_Get_PIV:
    
    LD BC, proc_opPtr_GetPut_PIV__get_byte
    LD DE, proc_opPtr_GetPut_PIV__get_word
    LD (proc_opPtr_GetPut_PIV__byte+1), BC
    LD (proc_opPtr_GetPut_PIV__word+1), DE
    
    call proc_opPtr_GetPut_PIV
    
    ret

inter_opPtr_AddrOf_VP:
    
    call proc_LD_A_next_byte_of_prog
    call proc_LD_HL_addrOf_varA
    PUSH HL
    POP BC
    
    call proc_LD_A_next_byte_of_prog
    call proc_LD_varA_BC
    
    ret


curr_exec_addr:
    DEFW func_main

bc_program:
func_set_dir_by_key: 
                    ; if (lastKey == sdbk_tKey) 
    DEFB opIf_Equal_AB, 26, 29
    DEFW if0_not
                    ; if (oldKey != sdbk_bKey) 
    DEFB opIf_NotEqual_AB, 27, 30
    DEFW if1_not
                    ; set dirX = sdbk_nDirX
    DEFB opSet_AR, 31, 22
                    ; set dirY = sdbk_nDirY
    DEFB opSet_AR, 32, 23
                    ; set keyDetected = acb_1
    DEFB opSet_AR, 43, 28
if1_not: 
if0_not: 
    
    DEFB opRet

func_ptr_mult_32: 
                    ; calc ptr = ptr << acb_5 
    DEFB opCalc_SHL_ABR, 55, 44, 55
    
    DEFB opRet

func_calc_attr_ptr: 
                    ; set ptr = attr_y
    DEFB opSet_AR, 57, 55
                    ; call ptr_mult_32()
    DEFB opCall_ptr_mult_32

                    ; calc ptr = ptr + attr_x 
    DEFB opCalc_Plus_ABR, 55, 56, 55
                    ; calc ptr = ptr + ADDR_ATTRIBUTES 
    DEFB opCalc_Plus_ABR, 55, 53, 55
    
    DEFB opRet

func_draw_attr: 
                    ; call calc_attr_ptr()
    DEFB opCall_calc_attr_ptr

                    ; ptr *ptr = attr_code
DEFB opPtr_Put_PV, 55, 33
    
    DEFB opRet

func_get_attr: 
                    ; call calc_attr_ptr()
    DEFB opCall_calc_attr_ptr

                    ; ptr attr_code = *ptr
DEFB opPtr_Get_PV, 55, 33
    
    DEFB opRet

func_apply_dir: 
                    ; ptr ad_tmp_coord = *ad_coord_ptr
DEFB opPtr_Get_PV, 58, 37
                    ; if (ad_dir == acb_1) 
    DEFB opIf_Equal_AB, 34, 43
    DEFW if2_not
                    ; if (ad_tmp_coord != ad_max) 
    DEFB opIf_NotEqual_AB, 37, 36
    DEFW if3_not
                    ; calc ad_tmp_coord = ad_tmp_coord + acb_1 
    DEFB opCalc_Plus_ABR, 37, 43, 37
if3_not: 
if2_not: 
                    ; if (ad_dir == acb_2) 
    DEFB opIf_Equal_AB, 34, 45
    DEFW if4_not
                    ; if (ad_tmp_coord != ad_min) 
    DEFB opIf_NotEqual_AB, 37, 35
    DEFW if5_not
                    ; calc ad_tmp_coord = ad_tmp_coord - acb_1 
    DEFB opCalc_Minus_ABR, 37, 43, 37
if5_not: 
if4_not: 
                    ; ptr *ad_coord_ptr = ad_tmp_coord
DEFB opPtr_Put_PV, 58, 37
    
    DEFB opRet

func_clear_screen: 
                    ; asm code
    DEFB opAsmW
    DEFW asm0_end
asm0_begin:
; ------ asm { ----------------------------------------- 

			LD    A,7
			CALL  8859
			LD	A, 56
			LD	(23693),A
			call	3435
			LD	A, 2
			CALL	5633
		
; ------ } -------------------------------------- END -- 
    ret
asm0_end:
                    ; set attr_code = PLAY_AREA_COLOR
    DEFB opSet_AR, 8, 33
                    ; for (attr_x = MIN_X; attr_x <= acb_19; attr_x = attr_x + acb_1) 
                    ; set attr_x = MIN_X
    DEFB opSet_AR, 4, 56
for1_repeat:
                    ; if (attr_x <= acb_19) 
    DEFB opIf_GreaterEqual_AB, 46, 56
    DEFW if6_not
                    ; for (attr_y = MIN_Y; attr_y <= acb_19; attr_y = attr_y + acb_1) 
                    ; set attr_y = MIN_Y
    DEFB opSet_AR, 5, 57
for0_repeat:
                    ; if (attr_y <= acb_19) 
    DEFB opIf_GreaterEqual_AB, 46, 57
    DEFW if7_not
                    ; call draw_attr()
    DEFB opCall_draw_attr

for0_continue:
                    ; calc attr_y = attr_y + acb_1 
    DEFB opCalc_Plus_ABR, 57, 43, 57
    DEFB opJmpW
    DEFW for0_repeat
if7_not: 
for0_break:
for1_continue:
                    ; calc attr_x = attr_x + acb_1 
    DEFB opCalc_Plus_ABR, 56, 43, 56
    DEFB opJmpW
    DEFW for1_repeat
if6_not: 
for1_break:
    
    DEFB opRet

func_print_scores: 
                    ; asm code
    DEFB opAsmW
    DEFW asm1_end
asm1_begin:
; ------ asm { ----------------------------------------- 

			jr	lab2
		len_tex:
			DEFB	22, 1, 21
			DEFM	"S:    "
			DEFB	22, 1, 23
		len_cnt:
		lab2:
			LD	DE, len_tex
			LD	BC, len_cnt-len_tex
			CALL	8252
			LD	BC, (_scores)
			CALL	6683
		
; ------ } -------------------------------------- END -- 
    ret
asm1_end:
    
    DEFB opRet

func_reset_game: 
                    ; call clear_screen()
    DEFB opCall_clear_screen

                    ; set scores = acb_0
    DEFB opSet_AR, 47, 52
                    ; call print_scores()
    DEFB opCall_print_scores

                    ; set target_x = TARGET_START_X
    DEFB opSet_AR, 18, 15
                    ; set target_y = TARGET_START_Y
    DEFB opSet_AR, 19, 16
                    ; set x = SNAKE_START_X
    DEFB opSet_AR, 20, 24
                    ; set y = SNAKE_START_Y
    DEFB opSet_AR, 21, 25
                    ; set tail_len = START_TAIL_LEN
    DEFB opSet_AR, 13, 14
                    ; set oldKey = acb_0
    DEFB opSet_AR, 47, 27
                    ; ptr *ADDR_LAST_KEY = acb_0
DEFB opPtr_Put_PV, 54, 47
                    ; set lastKey = acb_0
    DEFB opSet_AR, 47, 26
                    ; set dirX = acb_0
    DEFB opSet_AR, 47, 22
                    ; set dirY = acb_0
    DEFB opSet_AR, 47, 23
                    ; ptr tail_arr[acb_0] = x
DEFB opPtr_Put_PIV, 51, 47, 24
                    ; ptr tail_arr[acb_1] = y
DEFB opPtr_Put_PIV, 51, 43, 25
    
    DEFB opRet

func_main: 
                    ; call reset_game()
    DEFB opCall_reset_game

                    ; do_while (UP == UP) 
do_while1_repeat:
                    ; ptr lastKey = *ADDR_LAST_KEY
DEFB opPtr_Get_PV, 54, 26
                    ; set keyDetected = acb_0
    DEFB opSet_AR, 47, 28
                    ; set sdbk_tKey = UP
    DEFB opSet_AR, 0, 29
                    ; set sdbk_bKey = DN
    DEFB opSet_AR, 1, 30
                    ; set sdbk_nDirX = acb_0
    DEFB opSet_AR, 47, 31
                    ; set sdbk_nDirY = acb_2
    DEFB opSet_AR, 45, 32
                    ; call set_dir_by_key()
    DEFB opCall_set_dir_by_key

                    ; set sdbk_tKey = DN
    DEFB opSet_AR, 1, 29
                    ; set sdbk_bKey = UP
    DEFB opSet_AR, 0, 30
                    ; set sdbk_nDirX = acb_0
    DEFB opSet_AR, 47, 31
                    ; set sdbk_nDirY = acb_1
    DEFB opSet_AR, 43, 32
                    ; call set_dir_by_key()
    DEFB opCall_set_dir_by_key

                    ; set sdbk_tKey = LE
    DEFB opSet_AR, 2, 29
                    ; set sdbk_bKey = RI
    DEFB opSet_AR, 3, 30
                    ; set sdbk_nDirX = acb_2
    DEFB opSet_AR, 45, 31
                    ; set sdbk_nDirY = acb_0
    DEFB opSet_AR, 47, 32
                    ; call set_dir_by_key()
    DEFB opCall_set_dir_by_key

                    ; set sdbk_tKey = RI
    DEFB opSet_AR, 3, 29
                    ; set sdbk_bKey = LE
    DEFB opSet_AR, 2, 30
                    ; set sdbk_nDirX = acb_1
    DEFB opSet_AR, 43, 31
                    ; set sdbk_nDirY = acb_0
    DEFB opSet_AR, 47, 32
                    ; call set_dir_by_key()
    DEFB opCall_set_dir_by_key

                    ; if (keyDetected == acb_1) 
    DEFB opIf_Equal_AB, 28, 43
    DEFW if8_not
                    ; set oldKey = lastKey
    DEFB opSet_AR, 26, 27
if8_not: 
                    ; set attr_code = PLAY_AREA_COLOR
    DEFB opSet_AR, 8, 33
                    ; set tmp_byte = tail_len
    DEFB opSet_AR, 14, 42
                    ; calc tmp_byte = tmp_byte - acb_2 
    DEFB opCalc_Minus_ABR, 42, 45, 42
                    ; ptr tmp_byte = tail_arr[tmp_byte]
DEFB opPtr_Get_PIV, 51, 42, 42
                    ; set attr_x = tmp_byte
    DEFB opSet_AR, 42, 56
                    ; set tmp_byte = tail_len
    DEFB opSet_AR, 14, 42
                    ; calc tmp_byte = tmp_byte - acb_1 
    DEFB opCalc_Minus_ABR, 42, 43, 42
                    ; ptr tmp_byte = tail_arr[tmp_byte]
DEFB opPtr_Get_PIV, 51, 42, 42
                    ; set attr_y = tmp_byte
    DEFB opSet_AR, 42, 57
                    ; call draw_attr()
    DEFB opCall_draw_attr

                    ; ptr ad_coord_ptr = &x
DEFB opPtr_AddrOf_VP, 24, 58
                    ; set ad_dir = dirX
    DEFB opSet_AR, 22, 34
                    ; set ad_min = MIN_X
    DEFB opSet_AR, 4, 35
                    ; set ad_max = MAX_X
    DEFB opSet_AR, 6, 36
                    ; call apply_dir()
    DEFB opCall_apply_dir

                    ; ptr ad_coord_ptr = &y
DEFB opPtr_AddrOf_VP, 25, 58
                    ; set ad_dir = dirY
    DEFB opSet_AR, 23, 34
                    ; set ad_min = MIN_Y
    DEFB opSet_AR, 5, 35
                    ; set ad_max = MAX_Y
    DEFB opSet_AR, 7, 36
                    ; call apply_dir()
    DEFB opCall_apply_dir

                    ; set attr_x = x
    DEFB opSet_AR, 24, 56
                    ; set attr_y = y
    DEFB opSet_AR, 25, 57
                    ; call get_attr()
    DEFB opCall_get_attr

                    ; set next = attr_code
    DEFB opSet_AR, 33, 38
                    ; if (next == TAIL_DARK) 
    DEFB opIf_Equal_AB, 38, 10
    DEFW if9_not
                    ; set next = TAIL_BRIGHT
    DEFB opSet_AR, 9, 38
if9_not: 
                    ; if (next == TAIL_BRIGHT) 
    DEFB opIf_Equal_AB, 38, 9
    DEFW if10_not
                    ; call reset_game()
    DEFB opCall_reset_game

if10_not: 
                    ; if (next == TARGET_COLOR) 
    DEFB opIf_Equal_AB, 38, 11
    DEFW if11_not
                    ; calc tail_len = tail_len + acb_2 
    DEFB opCalc_Plus_ABR, 14, 45, 14
                    ; calc scores = scores + acb_1 
    DEFB opCalc_Plus_ABR, 52, 43, 52
                    ; call print_scores()
    DEFB opCall_print_scores

                    ; if (tail_len == WIN_TAIL) 
    DEFB opIf_Equal_AB, 14, 12
    DEFW if12_not
                    ; break 
DEFB opJmpW
DEFW do_while1_break
    DEFB opJmpW
    DEFW if12_end
if12_not: 
                    ; else 
                    ; if (target_n == acb_0) 
    DEFB opIf_Equal_AB, 17, 47
    DEFW if13_not
                    ; set target_n = acb_1
    DEFB opSet_AR, 43, 17
                    ; set target_x = acb_14
    DEFB opSet_AR, 48, 15
                    ; set target_y = acb_14
    DEFB opSet_AR, 48, 16
    DEFB opJmpW
    DEFW if13_end
if13_not: 
                    ; else 
                    ; set target_n = acb_0
    DEFB opSet_AR, 47, 17
                    ; set target_x = acb_4
    DEFB opSet_AR, 49, 15
                    ; set target_y = acb_4
    DEFB opSet_AR, 49, 16
if13_end: 
if12_end: 
if11_not: 
                    ; if (tail_len >= acb_4) 
    DEFB opIf_GreaterEqual_AB, 14, 49
    DEFW if14_not
                    ; set i = tail_len
    DEFB opSet_AR, 14, 39
                    ; calc i = i - acb_2 
    DEFB opCalc_Minus_ABR, 39, 45, 39
                    ; do_while (i != acb_0) 
do_while0_repeat:
                    ; calc i = i - acb_2 
    DEFB opCalc_Minus_ABR, 39, 45, 39
                    ; set i_src = i
    DEFB opSet_AR, 39, 40
                    ; set i_dst = i
    DEFB opSet_AR, 39, 41
                    ; calc i_dst = i_dst + acb_2 
    DEFB opCalc_Plus_ABR, 41, 45, 41
                    ; ptr tmp_byte = tail_arr[i_src]
DEFB opPtr_Get_PIV, 51, 40, 42
                    ; ptr tail_arr[i_dst] = tmp_byte
DEFB opPtr_Put_PIV, 51, 41, 42
                    ; calc i_dst = i_dst + acb_1 
    DEFB opCalc_Plus_ABR, 41, 43, 41
                    ; calc i_src = i_src + acb_1 
    DEFB opCalc_Plus_ABR, 40, 43, 40
                    ; ptr tmp_byte = tail_arr[i_src]
DEFB opPtr_Get_PIV, 51, 40, 42
                    ; ptr tail_arr[i_dst] = tmp_byte
DEFB opPtr_Put_PIV, 51, 41, 42
do_while0_continue:
                    ; if (i != acb_0) 
    DEFB opIf_NotEqual_AB, 39, 47
    DEFW if15_not
    DEFB opJmpW
    DEFW do_while0_repeat
if15_not: 
do_while0_break:
if14_not: 
                    ; ptr tail_arr[acb_0] = x
DEFB opPtr_Put_PIV, 51, 47, 24
                    ; ptr tail_arr[acb_1] = y
DEFB opPtr_Put_PIV, 51, 43, 25
                    ; set i = acb_0
    DEFB opSet_AR, 47, 39
                    ; set i_src = acb_0
    DEFB opSet_AR, 47, 40
                    ; while (i_src != tail_len) 
while1_repeat:
while1_continue:
                    ; if (i_src != tail_len) 
    DEFB opIf_NotEqual_AB, 40, 14
    DEFW if16_not
                    ; ptr tmp_byte = tail_arr[i_src]
DEFB opPtr_Get_PIV, 51, 40, 42
                    ; set attr_x = tmp_byte
    DEFB opSet_AR, 42, 56
                    ; calc i_src = i_src + acb_1 
    DEFB opCalc_Plus_ABR, 40, 43, 40
                    ; ptr tmp_byte = tail_arr[i_src]
DEFB opPtr_Get_PIV, 51, 40, 42
                    ; set attr_y = tmp_byte
    DEFB opSet_AR, 42, 57
                    ; if (i == acb_0) 
    DEFB opIf_Equal_AB, 39, 47
    DEFW if17_not
                    ; set tmp_byte = TAIL_BRIGHT
    DEFB opSet_AR, 9, 42
    DEFB opJmpW
    DEFW if17_end
if17_not: 
                    ; else 
                    ; set tmp_byte = TAIL_DARK
    DEFB opSet_AR, 10, 42
if17_end: 
                    ; calc i = i + acb_1 
    DEFB opCalc_Plus_ABR, 39, 43, 39
                    ; if (i == acb_3) 
    DEFB opIf_Equal_AB, 39, 50
    DEFW if18_not
                    ; set i = acb_0
    DEFB opSet_AR, 47, 39
if18_not: 
                    ; set attr_code = tmp_byte
    DEFB opSet_AR, 42, 33
                    ; call draw_attr()
    DEFB opCall_draw_attr

                    ; calc i_src = i_src + acb_1 
    DEFB opCalc_Plus_ABR, 40, 43, 40
    DEFB opJmpW
    DEFW while1_repeat
if16_not: 
while1_break:
                    ; set attr_code = TARGET_COLOR
    DEFB opSet_AR, 11, 33
                    ; set attr_x = target_x
    DEFB opSet_AR, 15, 56
                    ; set attr_y = target_y
    DEFB opSet_AR, 16, 57
                    ; call draw_attr()
    DEFB opCall_draw_attr

                    ; asm code
    DEFB opAsmW
    DEFW asm2_end
asm2_begin:
; ------ asm { ----------------------------------------- 

				LD 	BC, 5
				CALL 	7997
			
; ------ } -------------------------------------- END -- 
    ret
asm2_end:
do_while1_continue:
                    ; if (UP == UP) 
    DEFB opIf_Equal_AB, 0, 0
    DEFW if19_not
    DEFB opJmpW
    DEFW do_while1_repeat
if19_not: 
do_while1_break:
    
    DEFB opRet

func_draw_target: 
    
    DEFB opRet

func_long_print: 
                    ; asm code
    DEFB opAsmW
    DEFW asm3_end
asm3_begin:
; ------ asm { ----------------------------------------- 

			jr	prn_beg
		prn_text:
			DEFB	22, 2, 21
			DEFM	"p:    "
			DEFB	22, 2, 23
		prn_cnt:
		prn_beg:
			LD	DE, prn_text
			LD	BC, prn_cnt-prn_text
			CALL	8252
			LD	BC, (_print)
			CALL	6683
		
; ------ } -------------------------------------- END -- 
    ret
asm3_end:
                    ; asm code
    DEFB opAsmW
    DEFW asm4_end
asm4_begin:
; ------ asm { ----------------------------------------- 

			LD 	BC, 50
			CALL 	7997
		
; ------ } -------------------------------------- END -- 
    ret
asm4_end:
    
    DEFB opRet



addr_gv_byte_table:
_UP:               DEFB 11                 ; id 0
_DN:               DEFB 10                 ; id 1
_LE:               DEFB 8                  ; id 2
_RI:               DEFB 9                  ; id 3
_MIN_X:            DEFB 0                  ; id 4
_MIN_Y:            DEFB 0                  ; id 5
_MAX_X:            DEFB 19                 ; id 6
_MAX_Y:            DEFB 19                 ; id 7
_PLAY_AREA_COLOR:  DEFB 255                ; id 8
_TAIL_BRIGHT:      DEFB 96                 ; id 9
_TAIL_DARK:        DEFB 32                 ; id 10
_TARGET_COLOR:     DEFB 208                ; id 11
_WIN_TAIL:         DEFB 50                 ; id 12
_START_TAIL_LEN:   DEFB 2                  ; id 13
_tail_len:         DEFB 2                  ; id 14
_target_x:         DEFB 0                  ; id 15
_target_y:         DEFB 0                  ; id 16
_target_n:         DEFB 0                  ; id 17
_TARGET_START_X:   DEFB 15                 ; id 18
_TARGET_START_Y:   DEFB 10                 ; id 19
_SNAKE_START_X:    DEFB 10                 ; id 20
_SNAKE_START_Y:    DEFB 10                 ; id 21
_dirX:             DEFB 0                  ; id 22
_dirY:             DEFB 0                  ; id 23
_x:                DEFB 10                 ; id 24
_y:                DEFB 10                 ; id 25
_lastKey:          DEFB 0                  ; id 26
_oldKey:           DEFB 0                  ; id 27
_keyDetected:      DEFB 0                  ; id 28
_sdbk_tKey:        DEFB 0                  ; id 29
_sdbk_bKey:        DEFB 0                  ; id 30
_sdbk_nDirX:       DEFB 0                  ; id 31
_sdbk_nDirY:       DEFB 0                  ; id 32
_attr_code:        DEFB 0                  ; id 33
_ad_dir:           DEFB 0                  ; id 34
_ad_min:           DEFB 0                  ; id 35
_ad_max:           DEFB 0                  ; id 36
_ad_tmp_coord:     DEFB 0                  ; id 37
_next:             DEFB 0                  ; id 38
_i:                DEFB 0                  ; id 39
_i_src:            DEFB 0                  ; id 40
_i_dst:            DEFB 0                  ; id 41
_tmp_byte:         DEFB 0                  ; id 42
_acb_1:            DEFB 1                  ; id 43
_acb_5:            DEFB 5                  ; id 44
_acb_2:            DEFB 2                  ; id 45
_acb_19:           DEFB 19                 ; id 46
_acb_0:            DEFB 0                  ; id 47
_acb_14:           DEFB 14                 ; id 48
_acb_4:            DEFB 4                  ; id 49
_acb_3:            DEFB 3                  ; id 50

addr_gv_word_table:
_tail_arr:         DEFW _defArrS_tail_arr_ ; id 51
_scores:           DEFW 0                  ; id 52
_ADDR_ATTRIBUTES:  DEFW 22528              ; id 53
_ADDR_LAST_KEY:    DEFW 23560              ; id 54
_ptr:              DEFW 0                  ; id 55
_attr_x:           DEFW 0                  ; id 56
_attr_y:           DEFW 0                  ; id 57
_ad_coord_ptr:     DEFW 0                  ; id 58
_print:            DEFW 0                  ; id 59

_defArrS_tail_arr_: 
 DEFB 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
_defArrE_tail_arr_: 

bc_callstack_top:
DEFW bc_callstack

bc_callstack:

    end inter_main
