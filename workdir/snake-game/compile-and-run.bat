echo off

setlocal

set ifnam=snake-n
set ifext=n
set tm=asm

set ifn=%ifnam%.%ifext%

set asmfn=%ifnam%-%tm%.asm
set cmpfn=%ifnam%-%tm%-zxcmp.bin
set runfn=%ifnam%-%tm%-zxrun.tap



del %asmfn%
del %cmpfn%
del %runfn%

"../nt" -%tm% %ifn% %asmfn%


"../../../compilers/pasmo-0.5.3/pasmo.exe"           %asmfn%  %cmpfn%
"../../../compilers/pasmo-0.5.3/pasmo.exe" --tapbas  %asmfn%  %runfn%

%runfn%

endlocal

echo on