namespace Main 
{
	byte UP = 11;
	byte DN = 10;
	byte LE = 8;
	byte RI = 9;

	byte MIN_X = 0;
	byte MIN_Y = 0;
	byte MAX_X = 19;
	byte MAX_Y = 19;

	byte PLAY_AREA_COLOR = 255;
	
	byte TAIL_BRIGHT = 96;
	byte TAIL_DARK = 32;
	byte TARGET_COLOR = 208;

	byte WIN_TAIL = 50;
	byte START_TAIL_LEN = 2;
	byte tail_len = 2;
	byte tail_arr[52];

	word scores = 0;
	
	byte target_x = 0;
	byte target_y = 0;
	byte target_n = 0;

	byte TARGET_START_X = 15;
	byte TARGET_START_Y = 10;
	
	byte SNAKE_START_X = 10;
	byte SNAKE_START_Y = 10;

	word ADDR_ATTRIBUTES = 22528;
	word ADDR_LAST_KEY = 23560;
	

	byte dirX = 0;
	byte dirY = 0;
	byte x = 10;
	byte y = 10;
	
	
	byte lastKey = 0;
	byte oldKey = 0;
	
	byte keyDetected = 0;
	
	word ptr = 0;



	byte sdbk_tKey;
	byte sdbk_bKey;
	byte sdbk_nDirX;
	byte sdbk_nDirY;
	void set_dir_by_key() {
		if (lastKey==sdbk_tKey) {
			if (oldKey!=sdbk_bKey) {
				dirX = sdbk_nDirX;
				dirY = sdbk_nDirY;
				keyDetected = 1;
			}
		}
	}



	void ptr_mult_32()
	{
		














		ptr = ptr << 5;
	
	}

	

	byte attr_code;
	word attr_x;
	word attr_y;

	void calc_attr_ptr()
	{
		ptr = attr_y;
		ptr_mult_32();
		ptr = ptr + attr_x;
		
		ptr = ptr + ADDR_ATTRIBUTES;
	}

	void draw_attr() {
		calc_attr_ptr();
		*ptr = attr_code;
	}
	
	void get_attr() {
		calc_attr_ptr();
		attr_code = *ptr;
	}



	word ad_coord_ptr;
	byte ad_dir;
	byte ad_min;
	byte ad_max;
	byte ad_tmp_coord;
	void apply_dir() {
		ad_tmp_coord = *ad_coord_ptr;
		
		if (ad_dir == 1) {
			if (ad_tmp_coord != ad_max) {
				ad_tmp_coord = ad_tmp_coord + 1;
			}
		}
		if (ad_dir == 2) {
			if (ad_tmp_coord != ad_min) {
				ad_tmp_coord = ad_tmp_coord - 1;
			}
		}
		
		*ad_coord_ptr = ad_tmp_coord;
	}


	
	void clear_screen() {
		asm {
			LD    A,7
			CALL  8859
			LD	A, 56
			LD	(23693),A
			call	3435
			LD	A, 2
			CALL	5633
		}
		
		attr_code = PLAY_AREA_COLOR;
		
		for (attr_x = MIN_X; attr_x <= 19; attr_x = attr_x + 1)
		{


			for (attr_y = MIN_Y; attr_y <= 19; attr_y = attr_y + 1)
			{


				draw_attr();
			}
		}
	}



	void print_scores() {
		asm {
			jr	lab2
		len_tex:
			DEFB	22, 1, 21
			DEFM	"S:    "
			DEFB	22, 1, 23
		len_cnt:
		lab2:
			LD	DE, len_tex
			LD	BC, len_cnt-len_tex
			CALL	8252
			LD	BC, (_scores)
			CALL	6683
		}
	}



	void reset_game() {
		clear_screen();
		scores = 0;
		print_scores();
		
		target_x = TARGET_START_X;
		target_y = TARGET_START_Y;
		x = SNAKE_START_X;
		y = SNAKE_START_Y;
		tail_len = START_TAIL_LEN;
		oldKey = 0;
		*ADDR_LAST_KEY = (byte)0;
		lastKey = 0;
		dirX = 0;
		dirY = 0;
		
		tail_arr[0] = x;
		tail_arr[1] = y;
	}



	byte next;
	byte i;
	byte i_src;
	byte i_dst;
	byte tmp_byte;
	void Main() {
		reset_game();
	
		


		do {
			lastKey = *ADDR_LAST_KEY;
			
			keyDetected = 0;
			
			sdbk_tKey = UP; sdbk_bKey = DN;
			sdbk_nDirX = 0; sdbk_nDirY= 2;
			set_dir_by_key();
			
			sdbk_tKey = DN; sdbk_bKey = UP;
			sdbk_nDirX = 0; sdbk_nDirY = 1;
			set_dir_by_key();
			
			sdbk_tKey = LE; sdbk_bKey = RI;
			sdbk_nDirX = 2; sdbk_nDirY = 0;
			set_dir_by_key();
			
			sdbk_tKey = RI; sdbk_bKey = LE;
			sdbk_nDirX = 1; sdbk_nDirY = 0;
			set_dir_by_key();
		
			if (keyDetected == 1) {
				oldKey = lastKey;
			}
			
			
			attr_code = PLAY_AREA_COLOR;
			
			tmp_byte = tail_len;
			tmp_byte = tmp_byte - 2;
			tmp_byte = tail_arr[tmp_byte];
			attr_x = tmp_byte;

			tmp_byte = tail_len;
			tmp_byte = tmp_byte - 1;
			tmp_byte = tail_arr[tmp_byte];
			attr_y = tmp_byte;
			
			draw_attr();
			

			

			ad_coord_ptr = &x; ad_dir = dirX;
			ad_min = MIN_X; ad_max = MAX_X;
			apply_dir();
			
			ad_coord_ptr = &y; ad_dir = dirY;
			ad_min = MIN_Y; ad_max = MAX_Y;
			apply_dir();
			
			
			
			attr_x = x;
			attr_y = y;
			get_attr();
			next = attr_code;
			
			if (next == TAIL_DARK){
				next = TAIL_BRIGHT;
			}
			
			if (next == TAIL_BRIGHT) {
				reset_game();
			}
			
			if (next == TARGET_COLOR) {
				tail_len = tail_len + 2;
				scores = scores + 1;
				print_scores();
				if (tail_len == WIN_TAIL) {
					break;
				}
				else {

					if (target_n == 0) {
						target_n = 1;
						target_x = 14;
						target_y = 14;
					}
					else {
						target_n = 0;
						target_x = 4;
						target_y = 4;
					}
				}
			}
			

			
			if (tail_len >= 4) {
				i = tail_len;
				i = i - 2;
				
				do {
					i = i - 2;
					
					i_src = i;
					i_dst = i;
					i_dst = i_dst + 2;
					tmp_byte = tail_arr[i_src];
					tail_arr[i_dst] = tmp_byte;
					
					i_dst = i_dst + 1;
					i_src = i_src + 1;
					tmp_byte = tail_arr[i_src];
					tail_arr[i_dst] = tmp_byte;
				} while(i != 0);
			
















			}

			

			tail_arr[0] = x;
			tail_arr[1] = y;


			
			i = 0;
			i_src = 0;
			while (i_src != tail_len) {

				tmp_byte = tail_arr[i_src];
				attr_x = tmp_byte;

				i_src = i_src + 1;
				tmp_byte = tail_arr[i_src];
				attr_y = tmp_byte;

				if (i == 0){
					tmp_byte = TAIL_BRIGHT;
				}
				else {
					tmp_byte = TAIL_DARK;
				}
				i = i + 1;
				if (i == 3) {
					i = 0;
				}

				attr_code = tmp_byte;
				draw_attr();
				
				i_src = i_src + 1;
			}





			attr_code = TARGET_COLOR;
			attr_x = target_x;
			attr_y = target_y;
			draw_attr();

			



			asm {
				LD 	BC, 5
				CALL 	7997
			}
		} while (UP == UP);
		
	}

	void draw_target() {

	}

	word print;
	void long_print () {
		asm {
			jr	prn_beg
		prn_text:
			DEFB	22, 2, 21
			DEFM	"p:    "
			DEFB	22, 2, 23
		prn_cnt:
		prn_beg:
			LD	DE, prn_text
			LD	BC, prn_cnt-prn_text
			CALL	8252
			LD	BC, (_print)
			CALL	6683
		}

		asm {
			LD 	BC, 50
			CALL 	7997
		}
	}
}