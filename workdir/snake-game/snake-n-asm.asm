    ORG 32768
func_set_dir_by_key: 
                    ; if (lastKey == sdbk_tKey) 
    LD A, (_sdbk_tKey) 
    LD B, A 
    LD A, (_lastKey) 
    CP B 
    JP NZ, if0_not 
                    ; if (oldKey != sdbk_bKey) 
    LD A, (_sdbk_bKey) 
    LD B, A 
    LD A, (_oldKey) 
    CP B 
    JP Z, if1_not 
                    ; set dirX = sdbk_nDirX
    LD A, (_sdbk_nDirX) 
    LD (_dirX), A 
                    ; set dirY = sdbk_nDirY
    LD A, (_sdbk_nDirY) 
    LD (_dirY), A 
                    ; set keyDetected = 1
    LD A, 1 
    LD (_keyDetected), A 
if1_not: 
if0_not: 

    ret 
func_ptr_mult_32: 
                    ; calc ptr = ptr << 5 
    LD B, 5
    LD DE, (_ptr)
    LD A, 0
    CP B
    JR Z, logicShift0_skipIfZero
logicShift0_repeat:
    SLA E
    RL D
    DJNZ logicShift0_repeat
logicShift0_skipIfZero:
    LD (_ptr), DE

    ret 
func_calc_attr_ptr: 
                    ; set ptr = attr_y
    LD BC, (_attr_y) 
    LD (_ptr), BC 
                    ; call ptr_mult_32()
    call func_ptr_mult_32
                    ; calc ptr = ptr + attr_x 
    LD HL, (_ptr) 
    LD BC, (_attr_x) 
    ADD HL, BC 
    LD (_ptr), HL 
                    ; calc ptr = ptr + ADDR_ATTRIBUTES 
    LD HL, (_ptr) 
    LD BC, (_ADDR_ATTRIBUTES) 
    ADD HL, BC 
    LD (_ptr), HL 

    ret 
func_draw_attr: 
                    ; call calc_attr_ptr()
    call func_calc_attr_ptr
                    ; ptr *ptr = attr_code
    LD HL, (_ptr) 
    LD A, (_attr_code) 
    LD (HL), A 

    ret 
func_get_attr: 
                    ; call calc_attr_ptr()
    call func_calc_attr_ptr
                    ; ptr attr_code = *ptr
    LD HL, (_ptr) 
    LD A, (HL) 
    LD (_attr_code), A 

    ret 
func_apply_dir: 
                    ; ptr ad_tmp_coord = *ad_coord_ptr
    LD HL, (_ad_coord_ptr) 
    LD A, (HL) 
    LD (_ad_tmp_coord), A 
                    ; if (ad_dir == 1) 
    LD B, 1 
    LD A, (_ad_dir) 
    CP B 
    JP NZ, if2_not 
                    ; if (ad_tmp_coord != ad_max) 
    LD A, (_ad_max) 
    LD B, A 
    LD A, (_ad_tmp_coord) 
    CP B 
    JP Z, if3_not 
                    ; calc ad_tmp_coord = ad_tmp_coord + 1 
    LD A, 1 
    LD B, A 
    LD A, (_ad_tmp_coord) 
    ADD A, B 
    LD (_ad_tmp_coord), A 
if3_not: 
if2_not: 
                    ; if (ad_dir == 2) 
    LD B, 2 
    LD A, (_ad_dir) 
    CP B 
    JP NZ, if4_not 
                    ; if (ad_tmp_coord != ad_min) 
    LD A, (_ad_min) 
    LD B, A 
    LD A, (_ad_tmp_coord) 
    CP B 
    JP Z, if5_not 
                    ; calc ad_tmp_coord = ad_tmp_coord - 1 
    LD A, 1 
    LD B, A 
    LD A, (_ad_tmp_coord) 
    SUB B 
    LD (_ad_tmp_coord), A 
if5_not: 
if4_not: 
                    ; ptr *ad_coord_ptr = ad_tmp_coord
    LD HL, (_ad_coord_ptr) 
    LD A, (_ad_tmp_coord) 
    LD (HL), A 

    ret 
func_clear_screen: 
; ------ asm { ----------------------------------------- 

			LD    A,7
			CALL  8859
			LD	A, 56
			LD	(23693),A
			call	3435
			LD	A, 2
			CALL	5633
		
; ------ } -------------------------------------- END -- 
                    ; set attr_code = PLAY_AREA_COLOR
    LD A, (_PLAY_AREA_COLOR) 
    LD (_attr_code), A 
                    ; for (attr_x = MIN_X; attr_x <= 19; attr_x = attr_x + 1) 
                    ; set attr_x = MIN_X
    LD B, 0 
    LD A, (_MIN_X) 
    LD C, A 
    LD (_attr_x), BC 
for1_repeat:
                    ; if (attr_x <= 19) 
    LD HL, 19 
    LD BC, (_attr_x) 
    AND A 
    SBC HL, BC 
    JP C, if6_not 
                    ; for (attr_y = MIN_Y; attr_y <= 19; attr_y = attr_y + 1) 
                    ; set attr_y = MIN_Y
    LD B, 0 
    LD A, (_MIN_Y) 
    LD C, A 
    LD (_attr_y), BC 
for0_repeat:
                    ; if (attr_y <= 19) 
    LD HL, 19 
    LD BC, (_attr_y) 
    AND A 
    SBC HL, BC 
    JP C, if7_not 
                    ; call draw_attr()
    call func_draw_attr
for0_continue:
                    ; calc attr_y = attr_y + 1 
    LD HL, (_attr_y) 
    LD BC, 1 
    ADD HL, BC 
    LD (_attr_y), HL 
    JP for0_repeat
if7_not: 
for0_break:
for1_continue:
                    ; calc attr_x = attr_x + 1 
    LD HL, (_attr_x) 
    LD BC, 1 
    ADD HL, BC 
    LD (_attr_x), HL 
    JP for1_repeat
if6_not: 
for1_break:

    ret 
func_print_scores: 
; ------ asm { ----------------------------------------- 

			jr	lab2
		len_tex:
			DEFB	22, 1, 21
			DEFM	"S:    "
			DEFB	22, 1, 23
		len_cnt:
		lab2:
			LD	DE, len_tex
			LD	BC, len_cnt-len_tex
			CALL	8252
			LD	BC, (_scores)
			CALL	6683
		
; ------ } -------------------------------------- END -- 

    ret 
func_reset_game: 
                    ; call clear_screen()
    call func_clear_screen
                    ; set scores = 0
    LD BC, 0 
    LD (_scores), BC 
                    ; call print_scores()
    call func_print_scores
                    ; set target_x = TARGET_START_X
    LD A, (_TARGET_START_X) 
    LD (_target_x), A 
                    ; set target_y = TARGET_START_Y
    LD A, (_TARGET_START_Y) 
    LD (_target_y), A 
                    ; set x = SNAKE_START_X
    LD A, (_SNAKE_START_X) 
    LD (_x), A 
                    ; set y = SNAKE_START_Y
    LD A, (_SNAKE_START_Y) 
    LD (_y), A 
                    ; set tail_len = START_TAIL_LEN
    LD A, (_START_TAIL_LEN) 
    LD (_tail_len), A 
                    ; set oldKey = 0
    LD A, 0 
    LD (_oldKey), A 
                    ; ptr *ADDR_LAST_KEY = (byte)0
    LD HL, (_ADDR_LAST_KEY) 
    LD A, 0 
    LD (HL), A 
                    ; set lastKey = 0
    LD A, 0 
    LD (_lastKey), A 
                    ; set dirX = 0
    LD A, 0 
    LD (_dirX), A 
                    ; set dirY = 0
    LD A, 0 
    LD (_dirY), A 
                    ; ptr tail_arr[0] = x
    LD HL, (_tail_arr) 
    LD BC, 0 
    ADD HL, BC 
    LD A, (_x) 
    LD (HL), A 
                    ; ptr tail_arr[1] = y
    LD HL, (_tail_arr) 
    LD BC, 1 
    ADD HL, BC 
    LD A, (_y) 
    LD (HL), A 

    ret 
func_main: 
                    ; call reset_game()
    call func_reset_game
                    ; do_while (UP == UP) 
do_while1_repeat:
                    ; ptr lastKey = *ADDR_LAST_KEY
    LD HL, (_ADDR_LAST_KEY) 
    LD A, (HL) 
    LD (_lastKey), A 
                    ; set keyDetected = 0
    LD A, 0 
    LD (_keyDetected), A 
                    ; set sdbk_tKey = UP
    LD A, (_UP) 
    LD (_sdbk_tKey), A 
                    ; set sdbk_bKey = DN
    LD A, (_DN) 
    LD (_sdbk_bKey), A 
                    ; set sdbk_nDirX = 0
    LD A, 0 
    LD (_sdbk_nDirX), A 
                    ; set sdbk_nDirY = 2
    LD A, 2 
    LD (_sdbk_nDirY), A 
                    ; call set_dir_by_key()
    call func_set_dir_by_key
                    ; set sdbk_tKey = DN
    LD A, (_DN) 
    LD (_sdbk_tKey), A 
                    ; set sdbk_bKey = UP
    LD A, (_UP) 
    LD (_sdbk_bKey), A 
                    ; set sdbk_nDirX = 0
    LD A, 0 
    LD (_sdbk_nDirX), A 
                    ; set sdbk_nDirY = 1
    LD A, 1 
    LD (_sdbk_nDirY), A 
                    ; call set_dir_by_key()
    call func_set_dir_by_key
                    ; set sdbk_tKey = LE
    LD A, (_LE) 
    LD (_sdbk_tKey), A 
                    ; set sdbk_bKey = RI
    LD A, (_RI) 
    LD (_sdbk_bKey), A 
                    ; set sdbk_nDirX = 2
    LD A, 2 
    LD (_sdbk_nDirX), A 
                    ; set sdbk_nDirY = 0
    LD A, 0 
    LD (_sdbk_nDirY), A 
                    ; call set_dir_by_key()
    call func_set_dir_by_key
                    ; set sdbk_tKey = RI
    LD A, (_RI) 
    LD (_sdbk_tKey), A 
                    ; set sdbk_bKey = LE
    LD A, (_LE) 
    LD (_sdbk_bKey), A 
                    ; set sdbk_nDirX = 1
    LD A, 1 
    LD (_sdbk_nDirX), A 
                    ; set sdbk_nDirY = 0
    LD A, 0 
    LD (_sdbk_nDirY), A 
                    ; call set_dir_by_key()
    call func_set_dir_by_key
                    ; if (keyDetected == 1) 
    LD B, 1 
    LD A, (_keyDetected) 
    CP B 
    JP NZ, if8_not 
                    ; set oldKey = lastKey
    LD A, (_lastKey) 
    LD (_oldKey), A 
if8_not: 
                    ; set attr_code = PLAY_AREA_COLOR
    LD A, (_PLAY_AREA_COLOR) 
    LD (_attr_code), A 
                    ; set tmp_byte = tail_len
    LD A, (_tail_len) 
    LD (_tmp_byte), A 
                    ; calc tmp_byte = tmp_byte - 2 
    LD A, 2 
    LD B, A 
    LD A, (_tmp_byte) 
    SUB B 
    LD (_tmp_byte), A 
                    ; ptr tmp_byte = tail_arr[tmp_byte]
    LD HL, (_tail_arr) 
    LD B, 0 
    LD A, (_tmp_byte) 
    LD C, A 
    ADD HL, BC 
    LD A, (HL) 
    LD (_tmp_byte), A 
                    ; set attr_x = tmp_byte
    LD B, 0 
    LD A, (_tmp_byte) 
    LD C, A 
    LD (_attr_x), BC 
                    ; set tmp_byte = tail_len
    LD A, (_tail_len) 
    LD (_tmp_byte), A 
                    ; calc tmp_byte = tmp_byte - 1 
    LD A, 1 
    LD B, A 
    LD A, (_tmp_byte) 
    SUB B 
    LD (_tmp_byte), A 
                    ; ptr tmp_byte = tail_arr[tmp_byte]
    LD HL, (_tail_arr) 
    LD B, 0 
    LD A, (_tmp_byte) 
    LD C, A 
    ADD HL, BC 
    LD A, (HL) 
    LD (_tmp_byte), A 
                    ; set attr_y = tmp_byte
    LD B, 0 
    LD A, (_tmp_byte) 
    LD C, A 
    LD (_attr_y), BC 
                    ; call draw_attr()
    call func_draw_attr
                    ; ptr ad_coord_ptr = &x
    LD BC, _x 
    LD (_ad_coord_ptr), BC 
                    ; set ad_dir = dirX
    LD A, (_dirX) 
    LD (_ad_dir), A 
                    ; set ad_min = MIN_X
    LD A, (_MIN_X) 
    LD (_ad_min), A 
                    ; set ad_max = MAX_X
    LD A, (_MAX_X) 
    LD (_ad_max), A 
                    ; call apply_dir()
    call func_apply_dir
                    ; ptr ad_coord_ptr = &y
    LD BC, _y 
    LD (_ad_coord_ptr), BC 
                    ; set ad_dir = dirY
    LD A, (_dirY) 
    LD (_ad_dir), A 
                    ; set ad_min = MIN_Y
    LD A, (_MIN_Y) 
    LD (_ad_min), A 
                    ; set ad_max = MAX_Y
    LD A, (_MAX_Y) 
    LD (_ad_max), A 
                    ; call apply_dir()
    call func_apply_dir
                    ; set attr_x = x
    LD B, 0 
    LD A, (_x) 
    LD C, A 
    LD (_attr_x), BC 
                    ; set attr_y = y
    LD B, 0 
    LD A, (_y) 
    LD C, A 
    LD (_attr_y), BC 
                    ; call get_attr()
    call func_get_attr
                    ; set next = attr_code
    LD A, (_attr_code) 
    LD (_next), A 
                    ; if (next == TAIL_DARK) 
    LD A, (_TAIL_DARK) 
    LD B, A 
    LD A, (_next) 
    CP B 
    JP NZ, if9_not 
                    ; set next = TAIL_BRIGHT
    LD A, (_TAIL_BRIGHT) 
    LD (_next), A 
if9_not: 
                    ; if (next == TAIL_BRIGHT) 
    LD A, (_TAIL_BRIGHT) 
    LD B, A 
    LD A, (_next) 
    CP B 
    JP NZ, if10_not 
                    ; call reset_game()
    call func_reset_game
if10_not: 
                    ; if (next == TARGET_COLOR) 
    LD A, (_TARGET_COLOR) 
    LD B, A 
    LD A, (_next) 
    CP B 
    JP NZ, if11_not 
                    ; calc tail_len = tail_len + 2 
    LD A, 2 
    LD B, A 
    LD A, (_tail_len) 
    ADD A, B 
    LD (_tail_len), A 
                    ; calc scores = scores + 1 
    LD HL, (_scores) 
    LD BC, 1 
    ADD HL, BC 
    LD (_scores), HL 
                    ; call print_scores()
    call func_print_scores
                    ; if (tail_len == WIN_TAIL) 
    LD A, (_WIN_TAIL) 
    LD B, A 
    LD A, (_tail_len) 
    CP B 
    JP NZ, if12_not 
                    ; break 
    JP do_while1_break
    JP if12_end 
if12_not: 
                    ; else 
                    ; if (target_n == 0) 
    LD B, 0 
    LD A, (_target_n) 
    CP B 
    JP NZ, if13_not 
                    ; set target_n = 1
    LD A, 1 
    LD (_target_n), A 
                    ; set target_x = 14
    LD A, 14 
    LD (_target_x), A 
                    ; set target_y = 14
    LD A, 14 
    LD (_target_y), A 
    JP if13_end 
if13_not: 
                    ; else 
                    ; set target_n = 0
    LD A, 0 
    LD (_target_n), A 
                    ; set target_x = 4
    LD A, 4 
    LD (_target_x), A 
                    ; set target_y = 4
    LD A, 4 
    LD (_target_y), A 
if13_end: 
if12_end: 
if11_not: 
                    ; if (tail_len >= 4) 
    LD B, 4 
    LD A, (_tail_len) 
    CP B 
    JP C, if14_not 
                    ; set i = tail_len
    LD A, (_tail_len) 
    LD (_i), A 
                    ; calc i = i - 2 
    LD A, 2 
    LD B, A 
    LD A, (_i) 
    SUB B 
    LD (_i), A 
                    ; do_while (i != 0) 
do_while0_repeat:
                    ; calc i = i - 2 
    LD A, 2 
    LD B, A 
    LD A, (_i) 
    SUB B 
    LD (_i), A 
                    ; set i_src = i
    LD A, (_i) 
    LD (_i_src), A 
                    ; set i_dst = i
    LD A, (_i) 
    LD (_i_dst), A 
                    ; calc i_dst = i_dst + 2 
    LD A, 2 
    LD B, A 
    LD A, (_i_dst) 
    ADD A, B 
    LD (_i_dst), A 
                    ; ptr tmp_byte = tail_arr[i_src]
    LD HL, (_tail_arr) 
    LD B, 0 
    LD A, (_i_src) 
    LD C, A 
    ADD HL, BC 
    LD A, (HL) 
    LD (_tmp_byte), A 
                    ; ptr tail_arr[i_dst] = tmp_byte
    LD HL, (_tail_arr) 
    LD B, 0 
    LD A, (_i_dst) 
    LD C, A 
    ADD HL, BC 
    LD A, (_tmp_byte) 
    LD (HL), A 
                    ; calc i_dst = i_dst + 1 
    LD A, 1 
    LD B, A 
    LD A, (_i_dst) 
    ADD A, B 
    LD (_i_dst), A 
                    ; calc i_src = i_src + 1 
    LD A, 1 
    LD B, A 
    LD A, (_i_src) 
    ADD A, B 
    LD (_i_src), A 
                    ; ptr tmp_byte = tail_arr[i_src]
    LD HL, (_tail_arr) 
    LD B, 0 
    LD A, (_i_src) 
    LD C, A 
    ADD HL, BC 
    LD A, (HL) 
    LD (_tmp_byte), A 
                    ; ptr tail_arr[i_dst] = tmp_byte
    LD HL, (_tail_arr) 
    LD B, 0 
    LD A, (_i_dst) 
    LD C, A 
    ADD HL, BC 
    LD A, (_tmp_byte) 
    LD (HL), A 
do_while0_continue:
                    ; if (i != 0) 
    LD B, 0 
    LD A, (_i) 
    CP B 
    JP Z, if15_not 
    JP do_while0_repeat
if15_not: 
do_while0_break:
if14_not: 
                    ; ptr tail_arr[0] = x
    LD HL, (_tail_arr) 
    LD BC, 0 
    ADD HL, BC 
    LD A, (_x) 
    LD (HL), A 
                    ; ptr tail_arr[1] = y
    LD HL, (_tail_arr) 
    LD BC, 1 
    ADD HL, BC 
    LD A, (_y) 
    LD (HL), A 
                    ; set i = 0
    LD A, 0 
    LD (_i), A 
                    ; set i_src = 0
    LD A, 0 
    LD (_i_src), A 
                    ; while (i_src != tail_len) 
while1_repeat:
while1_continue:
                    ; if (i_src != tail_len) 
    LD A, (_tail_len) 
    LD B, A 
    LD A, (_i_src) 
    CP B 
    JP Z, if16_not 
                    ; ptr tmp_byte = tail_arr[i_src]
    LD HL, (_tail_arr) 
    LD B, 0 
    LD A, (_i_src) 
    LD C, A 
    ADD HL, BC 
    LD A, (HL) 
    LD (_tmp_byte), A 
                    ; set attr_x = tmp_byte
    LD B, 0 
    LD A, (_tmp_byte) 
    LD C, A 
    LD (_attr_x), BC 
                    ; calc i_src = i_src + 1 
    LD A, 1 
    LD B, A 
    LD A, (_i_src) 
    ADD A, B 
    LD (_i_src), A 
                    ; ptr tmp_byte = tail_arr[i_src]
    LD HL, (_tail_arr) 
    LD B, 0 
    LD A, (_i_src) 
    LD C, A 
    ADD HL, BC 
    LD A, (HL) 
    LD (_tmp_byte), A 
                    ; set attr_y = tmp_byte
    LD B, 0 
    LD A, (_tmp_byte) 
    LD C, A 
    LD (_attr_y), BC 
                    ; if (i == 0) 
    LD B, 0 
    LD A, (_i) 
    CP B 
    JP NZ, if17_not 
                    ; set tmp_byte = TAIL_BRIGHT
    LD A, (_TAIL_BRIGHT) 
    LD (_tmp_byte), A 
    JP if17_end 
if17_not: 
                    ; else 
                    ; set tmp_byte = TAIL_DARK
    LD A, (_TAIL_DARK) 
    LD (_tmp_byte), A 
if17_end: 
                    ; calc i = i + 1 
    LD A, 1 
    LD B, A 
    LD A, (_i) 
    ADD A, B 
    LD (_i), A 
                    ; if (i == 3) 
    LD B, 3 
    LD A, (_i) 
    CP B 
    JP NZ, if18_not 
                    ; set i = 0
    LD A, 0 
    LD (_i), A 
if18_not: 
                    ; set attr_code = tmp_byte
    LD A, (_tmp_byte) 
    LD (_attr_code), A 
                    ; call draw_attr()
    call func_draw_attr
                    ; calc i_src = i_src + 1 
    LD A, 1 
    LD B, A 
    LD A, (_i_src) 
    ADD A, B 
    LD (_i_src), A 
    JP while1_repeat
if16_not: 
while1_break:
                    ; set attr_code = TARGET_COLOR
    LD A, (_TARGET_COLOR) 
    LD (_attr_code), A 
                    ; set attr_x = target_x
    LD B, 0 
    LD A, (_target_x) 
    LD C, A 
    LD (_attr_x), BC 
                    ; set attr_y = target_y
    LD B, 0 
    LD A, (_target_y) 
    LD C, A 
    LD (_attr_y), BC 
                    ; call draw_attr()
    call func_draw_attr
; ------ asm { ----------------------------------------- 

				LD 	BC, 5
				CALL 	7997
			
; ------ } -------------------------------------- END -- 
do_while1_continue:
                    ; if (UP == UP) 
    LD A, (_UP) 
    LD B, A 
    LD A, (_UP) 
    CP B 
    JP NZ, if19_not 
    JP do_while1_repeat
if19_not: 
do_while1_break:

    ret 
func_draw_target: 

    ret 
func_long_print: 
; ------ asm { ----------------------------------------- 

			jr	prn_beg
		prn_text:
			DEFB	22, 2, 21
			DEFM	"p:    "
			DEFB	22, 2, 23
		prn_cnt:
		prn_beg:
			LD	DE, prn_text
			LD	BC, prn_cnt-prn_text
			CALL	8252
			LD	BC, (_print)
			CALL	6683
		
; ------ } -------------------------------------- END -- 
; ------ asm { ----------------------------------------- 

			LD 	BC, 50
			CALL 	7997
		
; ------ } -------------------------------------- END -- 

    ret 

global_vars:
_UP:               DEFB 11                 
_DN:               DEFB 10                 
_LE:               DEFB 8                  
_RI:               DEFB 9                  
_MIN_X:            DEFB 0                  
_MIN_Y:            DEFB 0                  
_MAX_X:            DEFB 19                 
_MAX_Y:            DEFB 19                 
_PLAY_AREA_COLOR:  DEFB 255                
_TAIL_BRIGHT:      DEFB 96                 
_TAIL_DARK:        DEFB 32                 
_TARGET_COLOR:     DEFB 208                
_WIN_TAIL:         DEFB 50                 
_START_TAIL_LEN:   DEFB 2                  
_tail_len:         DEFB 2                  
_tail_arr:         DEFW _defArrS_tail_arr_ 
_defArrS_tail_arr_: 
 DEFB 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
_defArrE_tail_arr_: 
_scores:           DEFW 0                  
_target_x:         DEFB 0                  
_target_y:         DEFB 0                  
_target_n:         DEFB 0                  
_TARGET_START_X:   DEFB 15                 
_TARGET_START_Y:   DEFB 10                 
_SNAKE_START_X:    DEFB 10                 
_SNAKE_START_Y:    DEFB 10                 
_ADDR_ATTRIBUTES:  DEFW 22528              
_ADDR_LAST_KEY:    DEFW 23560              
_dirX:             DEFB 0                  
_dirY:             DEFB 0                  
_x:                DEFB 10                 
_y:                DEFB 10                 
_lastKey:          DEFB 0                  
_oldKey:           DEFB 0                  
_keyDetected:      DEFB 0                  
_ptr:              DEFW 0                  
_sdbk_tKey:        DEFB 0                  
_sdbk_bKey:        DEFB 0                  
_sdbk_nDirX:       DEFB 0                  
_sdbk_nDirY:       DEFB 0                  
_attr_code:        DEFB 0                  
_attr_x:           DEFW 0                  
_attr_y:           DEFW 0                  
_ad_coord_ptr:     DEFW 0                  
_ad_dir:           DEFB 0                  
_ad_min:           DEFB 0                  
_ad_max:           DEFB 0                  
_ad_tmp_coord:     DEFB 0                  
_next:             DEFB 0                  
_i:                DEFB 0                  
_i_src:            DEFB 0                  
_i_dst:            DEFB 0                  
_tmp_byte:         DEFB 0                  
_print:            DEFW 0                  

    end func_main
