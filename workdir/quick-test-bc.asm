    ORG 32768

    OP_CODES_COUNT EQU 8

    opRet             EQU 0
    opJmpW            EQU 1
    opPtr_Get_PIV     EQU 2
    opIf_NotEqual_AB  EQU 3
    opAsmW            EQU 4
    opCalc_PlusPlus_A EQU 5
    opSet_AR          EQU 6
    opIf_Less_AB      EQU 7

    ; general module short calls consts
    opCall_test1 EQU 8
    opCall_test2 EQU 9

inter_table:
    DEFW inter_opRet
    DEFW inter_opJmpW
    DEFW inter_opPtr_Get_PIV
    DEFW inter_opIf_NotEqual_AB
    DEFW inter_opAsmW
    DEFW inter_opCalc_PlusPlus_A
    DEFW inter_opSet_AR
    DEFW inter_opIf_Less_AB
inter_table_end:

module_short_calls:
    DEFW func_test1
    DEFW func_test2

inter_main:
    
inter_main__repeat:
    call proc_LD_A_next_byte_of_prog
    LD B, OP_CODES_COUNT
    LD C, A
    SUB B
    JR NC, inter_main__short_call
    
inter_main__inter_call:
    LD A, C
    LD BC, inter_table
    call N_proc_LD_DE_from_word_table_BC_by_index_A
    call proc_CALL_asm_at_DE
    JR inter_main__jump_to_repeat
    
inter_main__short_call:
    LD BC, module_short_calls
    call N_proc_LD_DE_from_word_table_BC_by_index_A
    call proc_CALL_ByteCode_at_DE
    
inter_main__jump_to_repeat:
inter_main__erase_for_halt:
    JR inter_main__repeat
    
    ret

proc_LD_A_next_byte_of_prog:
    
    PUSH HL
    
    LD HL, (curr_exec_addr)
    LD A, (HL)
    INC HL
    LD (curr_exec_addr), HL
    
    POP HL
    
    ret

proc_CALL_asm_at_DE:
    
    LD (proc_CALL_asm_at_DE__call+1), DE
proc_CALL_asm_at_DE__call:
    call 0000
    
    ret

proc_CALL_ByteCode_at_DE:
    
    PUSH AF
    PUSH BC
    PUSH HL
    
    LD BC, 65534
    LD HL, (bc_callstack_top)
    PUSH HL
    AND A
    SBC HL, BC
    POP HL
    JR NC, proc_CALL_ByteCode_at_DE__stack_overflow
    LD BC, (curr_exec_addr)
    LD (HL), C
    INC HL
    LD (HL), B
    INC HL
    LD (bc_callstack_top), HL
    LD (curr_exec_addr), DE
    
    JR proc_CALL_ByteCode_at_DE__exit
proc_CALL_ByteCode_at_DE__stack_overflow:
    
    LD A, 2
    call 5633
    LD A, '#'
    rst 16
    LD A, '1'
    rst 16
    
    LD BC, 0000
    LD (inter_main__erase_for_halt), BC
    
proc_CALL_ByteCode_at_DE__exit:
    POP HL
    POP BC
    POP AF
    
    ret

N_proc_LD_DE_from_word_table_BC_by_index_A:
    
    PUSH HL
    
    LD H, 0
    LD L, A
    SLA L
    RL H
    ADD HL, BC
    LD E, (HL)
    INC HL
    LD D, (HL)
    
    POP HL
    
    ret

proc_LD_BC_next_word_of_prog:
    
    PUSH HL
    
    LD HL, (curr_exec_addr)
    LD C, (HL)
    INC HL
    LD B, (HL)
    INC HL
    LD (curr_exec_addr), HL
    
    POP HL
    
    ret

proc_opPtr_GetPut_PIV:
    
    call proc_LD_A_next_byte_of_prog
    call proc_LD_BC_varA
    PUSH BC
    POP HL
    
    call proc_LD_A_next_byte_of_prog
    call proc_LD_BC_varA
    PUSH BC
    POP DE
    
    call proc_LD_A_next_byte_of_prog
    CP addr_gv_word_table - addr_gv_byte_table
    JR NC, proc_opPtr_GetPut_PIV__word
    
proc_opPtr_GetPut_PIV__byte:
    JP proc_opPtr_GetPut_PIV__put_byte
proc_opPtr_GetPut_PIV__put_byte:
    call proc_LD_BC_varA
    ADD HL, DE
    LD (HL), C
    JR proc_opPtr_GetPut_PIV__exit
proc_opPtr_GetPut_PIV__get_byte:
    ADD HL, DE
    LD C, (HL)
    call proc_LD_varA_BC
    JR proc_opPtr_GetPut_PIV__exit
    
proc_opPtr_GetPut_PIV__word:
    JP proc_opPtr_GetPut_PIV__put_word
proc_opPtr_GetPut_PIV__put_word:
    call proc_LD_BC_varA
    SLA E
    RL D
    ADD HL, DE
    LD (HL), C
    INC HL
    LD (HL), B
    JR proc_opPtr_GetPut_PIV__exit
proc_opPtr_GetPut_PIV__get_word:
    SLA E
    RL D
    ADD HL, DE
    LD C, (HL)
    INC HL
    LD B, (HL)
    call proc_LD_varA_BC
    
proc_opPtr_GetPut_PIV__exit:
    
    ret

proc_LD_BC_varA:
    
    PUSH HL
    
    call proc_LD_HL_addrOf_varA
    LD C, (HL)
    CP addr_gv_word_table - addr_gv_byte_table
    JR NC, proc_LD_BC_varA__word  ;jump if A >= count
;load 1 byte by addr
    LD B, 0
    JR proc_LD_BC_varA__exit    
proc_LD_BC_varA__word:;load 2 bytes by addr
    INC HL
    LD B, (HL)
    
proc_LD_BC_varA__exit:
    
    POP HL
    
    ret

proc_LD_varA_BC:
    
    PUSH HL
    
    call proc_LD_HL_addrOf_varA
    LD (HL), C
    CP addr_gv_word_table - addr_gv_byte_table
    JR C, proc_LD_varA_BC__exit  ;jump if A < count
    INC HL
    LD (HL), B
    
proc_LD_varA_BC__exit:
    
    POP HL
    
    ret

proc_LD_HL_addrOf_varA:
    
    PUSH BC
    PUSH AF
    
    LD H, 0
    
    LD C, addr_gv_word_table - addr_gv_byte_table  ;byte vars count
    CP C
    JR NC, proc_LD_HL_addrOf_varA__word_table  ;jump if A >= C
;calc like byte addr
    LD L, A
    LD BC, addr_gv_byte_table
    ADD HL, BC
    JR proc_LD_HL_addrOf_varA__exit    
proc_LD_HL_addrOf_varA__word_table:
;calc like word addr
    SUB C
    LD L, A
    SLA L
    RL H
    LD BC, addr_gv_word_table
    ADD HL, BC
proc_LD_HL_addrOf_varA__exit:
    POP AF
    POP BC
    
    ret

proc_opIf_Compare_AB:
    
    call proc_LD_A_next_byte_of_prog
    call proc_LD_BC_varA
    PUSH BC
    call proc_LD_A_next_byte_of_prog
    call proc_LD_BC_varA
    PUSH BC
    call proc_LD_BC_next_word_of_prog
    PUSH BC
    
    POP DE
    POP BC
    POP HL
    
    AND A
    SBC HL, BC
proc_opIf_Compare_AB__jmp_op:
    JR Z, proc_opIf_Compare_AB__exit
    LD (curr_exec_addr), DE
    
proc_opIf_Compare_AB__exit:
    ret

proc_CALL_DE_for_A:
    
    PUSH AF
    PUSH BC
    PUSH HL
    
    call proc_LD_A_next_byte_of_prog
    call proc_LD_BC_varA
    
    PUSH BC
    POP HL
    
    LD BC, 1
    
    call proc_CALL_asm_at_DE
    
    PUSH HL
    POP BC
    
    call proc_LD_varA_BC
    
    POP HL
    POP BC
    POP AF
    
    ret

proc_ADD_HL_BC:
    
    ADD HL, BC
    
    ret


inter_opRet:
    
    LD HL, (bc_callstack_top)
    DEC HL
    DEC HL
    LD (bc_callstack_top), HL
    
    LD BC, bc_callstack
    PUSH HL
    AND A
    SBC HL, BC
    POP HL
    JR C, inter_opRet__exit_program
    
    LD C, (HL)
    INC HL
    LD B, (HL)
    
    LD (curr_exec_addr), BC
    
    ret
inter_opRet__exit_program:
    LD BC, 0
    LD (inter_main__erase_for_halt), BC
    ret

inter_opJmpW:
    call proc_LD_BC_next_word_of_prog
    LD (curr_exec_addr), BC
    
    ret

inter_opPtr_Get_PIV:
    
    LD BC, proc_opPtr_GetPut_PIV__get_byte
    LD DE, proc_opPtr_GetPut_PIV__get_word
    LD (proc_opPtr_GetPut_PIV__byte+1), BC
    LD (proc_opPtr_GetPut_PIV__word+1), DE
    
    call proc_opPtr_GetPut_PIV
    
    ret

inter_opIf_NotEqual_AB:
    
    LD A, #20
    LD (proc_opIf_Compare_AB__jmp_op), A
    call proc_opIf_Compare_AB
    ret

inter_opAsmW:
    call proc_LD_BC_next_word_of_prog
    PUSH BC
    LD DE, (curr_exec_addr)
    call proc_CALL_asm_at_DE
    POP DE
    LD (curr_exec_addr), DE
    
    ret

inter_opCalc_PlusPlus_A:
    
    LD DE, proc_ADD_HL_BC
    call proc_CALL_DE_for_A
    
    ret

inter_opSet_AR:
    
    call proc_LD_A_next_byte_of_prog
    call proc_LD_BC_varA
    call proc_LD_A_next_byte_of_prog
    call proc_LD_varA_BC
    
    ret

inter_opIf_Less_AB:
    
    LD A, #38
    LD (proc_opIf_Compare_AB__jmp_op), A
    call proc_opIf_Compare_AB
    ret


curr_exec_addr:
    DEFW func_Main

bc_program:
func_Main: 
                    ; do_while (c != acb_0) 
do_while0_repeat:
                    ; ptr c = str[i]
DEFB opPtr_Get_PIV, 3, 1, 0
                    ; if (c != acb_0) 
    DEFB opIf_NotEqual_AB, 0, 2
    DEFW if0_not
                    ; asm code
    DEFB opAsmW
    DEFW asm0_end
asm0_begin:
; ------ asm { ----------------------------------------- 

				LD A, (_c)
				RST 16
			
; ------ } -------------------------------------- END -- 
    ret
asm0_end:
if0_not: 
                    ; calc_unary i ++
    DEFB opCalc_PlusPlus_A, 1
do_while0_continue:
                    ; if (c != acb_0) 
    DEFB opIf_NotEqual_AB, 0, 2
    DEFW if1_not
    DEFB opJmpW
    DEFW do_while0_repeat
if1_not: 
do_while0_break:
                    ; for (i = acw_48; i < acw_58; i = i + 1) 
                    ; set i = acw_48
    DEFB opSet_AR, 5, 1
for0_repeat:
                    ; if (i < acw_58) 
    DEFB opIf_Less_AB, 1, 6
    DEFW if2_not
                    ; asm code
    DEFB opAsmW
    DEFW asm1_end
asm1_begin:
; ------ asm { ----------------------------------------- 

				LD A, (_i)
				RST 16
			
; ------ } -------------------------------------- END -- 
    ret
asm1_end:
for0_continue:
                    ; calc_unary i ++
    DEFB opCalc_PlusPlus_A, 1
    DEFB opJmpW
    DEFW for0_repeat
if2_not: 
for0_break:
                    ; call test1()
    DEFB opCall_test1

                    ; call test2()
    DEFB opCall_test2

    
    DEFB opRet

func_test1: 
                    ; asm code
    DEFB opAsmW
    DEFW asm2_end
asm2_begin:
; ------ asm { ----------------------------------------- 

			LD A, "a"
			RST 16
		
; ------ } -------------------------------------- END -- 
    ret
asm2_end:
    
    DEFB opRet

func_test2: 
                    ; asm code
    DEFB opAsmW
    DEFW asm3_end
asm3_begin:
; ------ asm { ----------------------------------------- 

			LD A, "b"
			RST 16
		
; ------ } -------------------------------------- END -- 
    ret
asm3_end:
    
    DEFB opRet



addr_gv_byte_table:
_c:       DEFB 0             ; id 0
_i:       DEFB 0             ; id 1
_acb_0:   DEFB 0             ; id 2

addr_gv_word_table:
_str:     DEFW _defArrS_str_ ; id 3
_a:       DEFW _defArrS_a_   ; id 4
_acw_48:  DEFW 48            ; id 5
_acw_58:  DEFW 58            ; id 6

_defArrS_str_: 
DEFM "-"
DEFB 34
DEFM "-"
DEFB 39
DEFM "-"
DEFB 92
DEFM "-"
DEFB 13
DEFM "-"
DEFB 65
DEFM "1-"
DEFB 72
DEFM "ello!-"
DEFB 13
DEFM "s-"
DEFB 94
DEFM "-e"
DEFB 13
DEFB 0
_defArrE_str_: 
_defArrS_a_: 
 DEFW 65, 66, 67, 10, 11, 1, 22, 333, 0, 0
_defArrE_a_: 

bc_callstack_top:
DEFW bc_callstack

bc_callstack:

    end inter_main
