    ORG 32768
func_main: 
                    ; do_while (c != 0) 
do_while0_repeat:
                    ; ptr c = str[i]
    LD HL, (_str) 
    LD B, 0 
    LD A, (_i) 
    LD C, A 
    ADD HL, BC 
    LD A, (HL) 
    LD (_c), A 
                    ; if (c != 0) 
    LD B, 0 
    LD A, (_c) 
    CP B 
    JP Z, if0_not 
; ------ asm { ----------------------------------------- 

				LD A, (_c)
				RST 16
			
; ------ } -------------------------------------- END -- 
if0_not: 
                    ; calc_unary i ++
    LD HL, _i
    INC (HL)
do_while0_continue:
                    ; if (c != 0) 
    LD B, 0 
    LD A, (_c) 
    CP B 
    JP Z, if1_not 
    JP do_while0_repeat
if1_not: 
do_while0_break:
                    ; for (i = (word)48; i < (word)58; i = i + (word)1) 
                    ; set i = (word)48
    LD A, 48 
    LD (_i), A 
for0_repeat:
                    ; if (i < (word)58) 
    LD HL, (_i) 
    LD H, 0 
    LD BC, 58 
    AND A 
    SBC HL, BC 
    JP NC, if2_not 
; ------ asm { ----------------------------------------- 

				LD A, (_i)
				RST 16
			
; ------ } -------------------------------------- END -- 
for0_continue:
                    ; calc_unary i ++
    LD HL, _i
    INC (HL)
    JP for0_repeat
if2_not: 
for0_break:

    ret 

global_vars:
_str:  DEFW _defArrS_str_ 
_defArrS_str_: 
DEFM "-"
DEFB 34
DEFM "-"
DEFB 39
DEFM "-"
DEFB 92
DEFM "-"
DEFB 13
DEFM "-"
DEFB 65
DEFM "1-"
DEFB 72
DEFM "ello!-"
DEFB 13
DEFM "s-"
DEFB 94
DEFM "-e"
DEFB 13
DEFB 0
_defArrE_str_: 
_c:    DEFB 0             
_i:    DEFB 0             
_a:    DEFW _defArrS_a_   
_defArrS_a_: 
 DEFW 65, 66, 10, 11, 1, 22, 333, 0, 0, 0
_defArrE_a_: 

    end func_main
