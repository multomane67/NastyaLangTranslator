# NastyaLangTranslator

This is a hobby project for creating own language translator. 
"N" or "NastyaLang" is simplified C language with minimum of features. 
It have few own modifications and some construction from C#. 

This is a console program. 
It translates from N language (file with extension .n) into Assembler language for ZX Spectrum 48K (file with extension .asm). 
This project is my trying to simplify creating games for ZX Spectrum and make this process more manualy controllable than in other C realizations and code more readable then Assembler. 
This is an experiment in creation of something in between. 
Maybe in the future this translator can help me in optimization or maybe in porting into other platforms my spectrum games.

Named as a joke by my dear wife by her own name. 
And I don't want to look for better titles than this. :)

**The most of work are finished. 
But there are still things to work on.
Deails you can see in task list.**

## Documentation
- [Command line using](docs/command-line.md)
- [Translation modes](docs/translation-modes.md)
- [Language specification](docs/n-lang-specs.md)
- [Task list](docs/task-list.md)