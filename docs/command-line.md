[[back to main page]](../README.md)

# Command line using

Usage:

```
usage: nt <-asm | -bc> [--wait-any-key] <source>... <translated>

  -asm   Translation into assembler code "as is" line by line.
         (will be optimized in future)

  -bc    Translation into byte code + assembler interpretator.

  --wait-any-key   Wait an any key after work.

  source           Source code for translation. It is a file name
                   with extention ".n". Can be used one or many
                   source files separated by spaces.

  translated       Result of translation will be saved into
                   translated file name with .asm extention.
                   This is ".asm" file.
```

About [translation modes](translation-modes.md).