[[back to main page]](../README.md)

# Task list

- Support of `.ntargs` files for storing command line arguments of nt.exe (something like project files)
- More calc operations
  - logic: `==, !=, >=, <=` with argument shortening in ByteCode. (returns 0 or 1)
  - bitwise: `&, |, ^, ~, <<*, *>>` (and, or, xor, not, roll bits left, roll bits right) rolling bits separately for word and byte
- Realize declaration of constants. (Now used only literal consts.)
- Generate new test programs.
- Refactor PBCI for using reflection.
- Use abstract parsing result for constructing after parsing not ANode.
- Own assmebler traslator (calculate size of assembler code need for relative jump using).
- Realize `len = arrlen(arrayName)` - can be interpretated like VariableSet a = 6;
- Add shortened:
  - OperationIf (check on zero, not zero, relative jump)
  - Cycles (thing about possible size optimithation and new construction for-inc(i=0;10){} for-dec(i;0){})
- Support many namespaces.
- Think about refactoring in Many_NoOrMore for storing cloned exams not ANodes.
- Renaming for refacotring:
  - Rename (A,B) notation to (1,2)
  - Rename 'Operator' to 'Statement'
- Signed types - `intb`, `intw`. Upgated parsing and translation.
- Optimisation for translation time.
  - Inlining of func code insted calling. (if it is more advantageous in weight)
  - Strip constans with same value but different names. Means literal constants and declared.
  - Remove unused functions.
  - Remove unused vars.
  - Remove unused modules.
- Log statistic about namespaces, vars and functions, used and stripped. Size of vars functions and interpretator.
- Complex expressions.
- Local vars
- Minimize call stack in parsing for better experience in debuging (need refactor/rework)
- Own assmebler traslator (compiller way).
- Realize disable-bc { }

## Misc

- Sorting
  - procedures of interpretator.
  - all consts and vars
  - op codes EQU
  - inter table
  - module short calls
- realize the RandBB()%num for random pos of target (wait for division calc operators)
- formating parsing error by groups? or minimize possible?
- Realise Stop points
    ```cs
    Debug.Stop(); //stop with file and line
    // will be writen in bottom line of screen
    // main.xml:123
    
    Debug.Stop($"variable full info {var}. only name '{var.name}' and only value '{var.value}'");
    // will be writen in bottom line of screen
    // variable full info var=10. only name 'var' and only value '10'
    ```



