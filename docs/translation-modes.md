[[back to main page]](../README.md)

# Translation modes
Currently this translator supports two translation modes.

- [Assembler code](#assembler-code)
- [Byte code](#byte-code) (with interpretator generated in assembler).

Both modes generate an `.asm` file as output. 
But they have different principles.

## Assembler code
This is a simple direct translation mode.
Each line is translated into its representation in assembly language.
Currently there are no optimizations.

Example:
```cpp
r = a + b;
```

Will be translated into z80 asm like this:
```asm
LD	A, (addrB)  // 3 bytes
LD	B, A        // 1 byte
LD	A, (addrA)  // 3 bytes
ADD	A, B        // 1 byte
LD	(addrR), A  // 3 bytes
// 11 bytes for all
```

> It is planned to optimize it in the future. 
> And will be used it in conjunction with Byte Code for targeted optimization of quick code.

## Byte code
During translation of a program, all its statements are translated into some code which I named `byte code`.

When translating a program into byte code, we also create an interpreter for this byte code. 
An interpreter is an assembly language program that contains a table with interpretation functions for each opcode.
Only those interpretation procedures whose op codes are used in the program are added.

Essentially, each of byte codes has its own interpretation procedure. 
Which reads the arguments following the code and does all its work.

It looks something like this:
```
[opCode][arg][arg][opCode][arg][opCode][opCode]
```

The operation code comes first, followed by a number of arguments.
Each code has its own number of arguments; some have no arguments at all.

### opcodes [opCode]
There are opcodes for everything.
To move values from one variable to another.
To perform arithmetic operations.
To call functions.
To compare variables and jump to other code.
Yes, calling function is also an opcode, and in the future, when function have their own arguments, then with arguments.

### opcode arguments [arg]
Usually the arguments are the numbers of the variables involved in the operations.
But sometimes it can be something else, such as the number of bytes to jump.

---

Example:

```cpp
r = a + b;
```

Will be translated into byte code like this:

```asm
DEFB opCalc, 0, 1, 2   // 4 bytes for all
```