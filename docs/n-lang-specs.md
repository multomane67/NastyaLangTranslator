[[back to main page]](../README.md)

# N language specification

- [Files](#files)
- [Namespaces](#namespaces)
- [Types](#types)
- [Variables](#variables)
- [Arrays](#arrays)
- [Strings](#strings)
- [Escape sequences](#escape-sequences) (for string and symbol literals)
- [Functions](#functions)
- [Assigment (Set)](#assigment-set)
- [Assigment with arithmetic expression (Calc)](#assigment-with-arithmetic-expression-calc)
- [Conditions (if, else, else if)](#conditions-if-else-else-if)
- [Cycle 'for'](#cycle-for)
- [Cycles 'while' and 'do while'](#cycles-while-and-do-while)
- [Pointer operations](#pointer-operations)
- [Array elements acces](#array-elements-acces)

> The N language is specially stripped down compared to standard C.
> Because it is translated into byte code as it is. One line, one opCode.
> And we can see exactly how many bytes each line will occupy.
> Essentially, the N language is an interpreted assembler with a syntax similar to C.
> Now this is a very naive and expensive weight optimization.
> Perhaps in the future I will be able to do something really worthwhile.

## Files
Used files with extension `.n`.

> Currently we can use one .n file only for whole program.

## Namespaces
The N language has namespaces. 
They were introduced to break the program into functional parts. 
And also reduce low-level addressing of variables and functions within one namespace.

Only inside the namespace can you declare variables, arrays and functions. 
Each source code file can contain several namespes with different names. 
Namespaces with the same name declared in different files are merged into one, but provided that the names of their members are not duplicated.

The order in which namespaces and the members within them are declared does not matter. 
Each program must have a `Main` namespace with a `Main` function inside.

> Currently, namespaces have a limit on the number of declared variables and literals of 256 each.

> Currently, we can use `Main` namespace only.

## Types
- `byte` is 8 bits in length and store unsigned value from 0 to 255.  
In addition can be used for store char.
- `word` is 16 bits in length and store unsigned value from 0 to 65535.  
In addition can be used like pointer to any type.

Booth types are unsigned. Signed types in plans.

Using:
- **Variables** can use `byte` and `word` only.
- **Arrays** can store elements of `byte` or `word` types.
But array variable have `word` type. 
And can be used like a pointer.  
More in [pointer operations](#pointer-operations) and [array elements acces](#array-elements-acces) articles.
- **Functions** can use `void` return type only at this time.
- **Pointers** we don't use a pointer types. 
But any `word` variable can be used like pointer to any type.  
More in [pointer operations](#pointer-operations) and [array elements acces](#array-elements-acces) articles.

## Constants

Only literal constants inside code are supported now.

## Variables
Currently only `global variables` are implemented. Local ones are still in the plans.

Variables are declared anywhere within the namespace. 
The order of announcement does not matter. 
You can specify default values if desired. 
If default values are not specified, then the variable receives the value 0.

```cpp
namespace Main 
{
    byte b;
    word w;

    void Main()
    {
        // ...
    }

    byte b_v = 255;         // default value in decimal
    word w_v = 0xFFFF;      // default value in hexadecimal
    byte bit8 = 0b10110100; // default value in binary
    word var = 'A';         // default value by symbol code 65
}
```

## Arrays
Here you can see the declaration of arrays.
If you specify the contents of the array, then its length is not necessary. 
If you specified a length greater than the number of elements then zero elements will be added to the end.

```cpp
// 4 bytes array filled by zero elements  {0, 0, 0, 0}
byte arrInBytes[4];

// 8 words array filled by {10, 100, 1000, 10000, 0, 0, 0, 0}
word arrInWords[8] = {10, 100, 1000, 10000}; 

// 4 words array filled by {65, 66, 10, 11}
word arr[] = {'A', 'B', 0xA, 0xB}
```

> Essentially, any array is a word type variable that stores the address of the beginning of the data.

## Strings

Strings are also arrays. 
But you can only use a `byte` to define them. 
By default, string literals always have a zero byte at the end of the string. 
If desired, you can turn it off by adding `nz` after string literal.

```cpp
// 12 bytes array "some string\0" with zero at the end
byte str1[] = "some string";

// 11 bytes array "some string" without zero at the end
byte str2[] = "some string"nz;

// 8 bytes array "abcd\0\0\0\0" filled by zero
byte str3[8] = "abcd";

// 8 bytes array "abcd\0\0\0\0" filled by zero
byte str4[8] = "abcd"nz;
```

## Escape sequences
You can use escape sequences inside string and symbol literals, but only those that are supported in zx spectrum.

```
Supported escape sequences

\0   - Zero symbol
\n   - End of line
\\   - Back slash
\'   - Single quote
\"   - Double quote
\xNN - Hex code of symbol. Allowed one or two hex symbols only. And values of byte only.


Examples with hex codes

\x8  - Cursor left symbol
\x41 - 'A'
\x78 - 'x'


More then 2 hex symbols interpretation

\x411 - Will be interpretated like code of 0x41 and '1'. "\x411" == "A1"
\x001 - Will be interpretated like code of 0x00 and '1'. You will got an end of the line and '1'.

This is a violation of the standard of many other languages. 
But this was done on purpose. 
For cases when you need to combine characters specified 
using hex code and regular characters in one word without spaces.

For example "\x48ello!" "Hello!"
```

## Functions

Just like a vars you can implement your functions anywere in namespace.
The function definition and call should look like this.

```cpp
namespace Main
{ 
	void Main ()
	{
		func();
	}

	void func()
	{

	}
}
```

Currently, functions do not return any values and do not receive any arguments. 
And factualy they are procedures.

> It is planned to add the return values and input arguments.

## Assigment (Set)

This is a classic assignment to a variable. 
A variable can be assigned by the value of another variable or a numeric literal. 

```cpp
a = b;          // assigning by value from var b
a = 35;         // assigning by dec numeric literal 
a = 0x64;       // assigning by hex numeric literal
a = 0b11001100; // assigning by bin numeric literal
a = 'A'         // assigning by 'A' symbol code
```

When assigning values of a larger type to a variable of a smaller type, the high byte will be truncated.

```cpp
namespace Main
{ 
	byte a;

	void Main ()
	{
		a = 0xA0A1; // after assigning a == 0xA1
	}
}
```

> Currently, when assigning a value of a larger type to a variable of a smaller type, no warnings are printed.

## Assigment with arithmetic expression (Calc)

Assigning the result of an arithmetic expression.

Now only in one of this formats.

```
resultVar = <var/literal> <operator> <var/literal>;
resultAndSourceVar <operator_binary> <var/literal>;
resultAndSourceVar <operator_unary>;
```

`<operator>`  
Has three operands: result var, first var or literal, second var or literal.  
The set of supported now: `+, -, <<, >>`.  

```cpp
r = a + b;
r = 123 - 456;
r = a << 2; 
r = a << b;
```

`<operator_binary>`  
The shortened version of `<operator>` with two operands: changeable var, changing var or literal.  
The set of supported now: `+=, -=, <<=, >>=`.  

```cpp
a += b; // increaste value of a by b
a -= 3; // decreaste value of a by 3
a <<= b; // shift left  of binary value of a by b bits
a >>= 2; // shift right of binary value of a by 2 bits
```

`<operator_unary>`  
The shortened version of `<operator>` with one operand: changeable var.  
The set of supported now: `++, --, <<, >>`.  

```cpp
a ++; // increaste value of a by 1
a --; // decreaste value of a by 1
a <<; // shift left  of binary value of a by 1 bit
a >>; // shift right of binary value of a by 1 bit
```

> I plan to implement other operators: `*, /, %, |, !, &, ^` .
> Including their shortened versions like unary ++, and binary +=. 
> As well as parsing of complex mathematical and logical expressions.
> For now, `+, -, <<, >>, ++, --, +=, -=, <<=, >>=` are all supported operators. 
> They are enough for programming the simplest things, where integer multiplication and division of multiple powers of two are enough.

> Currently, when assigning a value of a larger type to a variable of a smaller type, no warnings are printed.

## Conditions (if, else, else if)

This is the classic if and else if condition from the C language. 
But stripped down. 
As a logical expression, you can only use simple ones where only two operands are compared.

```cpp
if (<var/literal> <operator> <var/literal>)
{
	// ...
}
else if (<var/literal> <operator> <var/literal>)
{
	// ...
}
else
{
	// ...
}
```

The set of supported operators now: `==, !=, <, >, <=, >=`.
The operand can be either a variable or a numeric literal.

> Currently I plan use it like a jump analog.

## Cycle 'for'

You can use `for` cycle. 
It very similar to C. 
Look at this scheme.

```
for ([init]; [comparison]; [iterating])
{
	break;		// for exit from cycle
	continue;	// skip execution code
}
```

You can use three operations inside `for (...)`.
All of this parts is not necessery. 
You can use all, no any, or any of combinations.

`[init]`  
You can use one simple assigment operation like `i = 0`.  
But only one and you can't declare variables here.

`[comparison]`  
You can use simple comparison of two values like `i < 10`.  
But simple comparisons of two values only.

`[iterating]`  
You can use simple arythmetic operation like `i = i + 1` or `i += 1` or `i++`.  
But only one like this.

Example:
```cpp
for (i = 0; i < 10; i = i + 1) 
{
	// 10 iterations from 0..9
}

for (;;) 
{
	// ethernal cycle
}
```

> I plan to shorten the cycle 'for' even further in the future. 
> In order for it to occupy fewer bytes.

## Cycles 'while' and 'do while'

You can use `while` and `do while` loops similar to those in C.

Scheme:
```
while (<comparison>)
{
	break;		// for exit from cycle
	continue;	// skip execution code
}

do
{
	break;		// for exit from cycle
	continue;	// skip execution code
}
while (<comparison>);
```

Where `<comparison>` is simple comparison of two values like this `i < 10`.
Comparison is mandatory for `while` and `do while`.

Example:
```cpp
while (i < 10)
{
	// ...
}

do
{
	// ...
}
while (c != 0);
```

## Pointer operations

Here we have many differents to classic C.
We don't have pointer in usual sence.
Any `word` variable can be used like a pointer to any type.
Accessing array elements is also a pointer operation. 
And any variable of type `word` can be used as an array of any type, even if it is not an array. 
More about this in [array elements acces](#array-elements-acces) article.

Use `*` for acces to value under pointer. 
Program reads by the pointer bytes into variable.
As many bytes as the variable occupies will be copied.
Also we can put value of variable or literal const value by pointer.



Example:
```cpp
namespace Main {
	byte bVar;
	word wVar;
	word ptr;

	void Main () {
		ptr = 0xB800;
		
		bVar = *ptr; // will be copied one byte into bVar by address 0xB800
		wVar = *ptr; // will be copied two bytes into wVar by address 0xB800

		*ptr = bVar; // will be copied one byte from bVar by address 0xB800
		*ptr = wVar; // will be copied two bytes from wVar by addres 0xB800

		*ptr = (byte)0xFF; // will be copied one byte 0xFF by address 0xB800
		*ptr = (word)0xFF; // will be copied two bytes 0xFF and 0x00 by address 0xB800
	}
}
```

This operation have some bounds of syntax. 
Because it contradicts the principles of language at the current moment. 
In priority minimum various comands of byte code and using simplest operations only.
- You can't read and store inside one operation.
- You can't store literal consts without cast type.
- You can't store variable value with cast type. (will be realized in future.)

```cpp
// forbidden
*ptr1 = *ptr1;
// allowed
var = *ptr1;
*ptr2 = var;

// forbidden
*ptr = 0xFF;
// allowed
*ptr = (byte)0xFF;

// forbiden
*ptr = (byte)varWithWordType;
// allowed
tmpByte = varWithWordType;
*ptr = tmpByte;
```

> The current implementation does not completly satisfy me. And can be changed in future.

## Array elements acces

Acces to array elements is a variation of [pointer operation](#pointer-operations). 
Any array variable is a variable of `word` type. 
And any `word` variable can be used like a pointer or array. 
The only differences are in the syntax and the fact that here we can specify the element number.

Next operations `var = ptr[0]` and `ptr[0] = var`  
are equivalent to `var = *ptr` and `*ptr = var`.

Use `[]` for acces to array element. 
Put the element's index inside. 
And read the value into a variable. 
Or write the value of the variable in an array by index. 
Just as in the case of a pointer, as many bytes will be copied as the variable that participates in the expression occupies.
If we want to store the literal const then we must use cast type for this literal.

Example:
```cpp
namespace Main {
	byte bVar;   // only variable type define a number of transfered bytes
	word wVar;
	word ptr;

	byte ib = 2; // type of index does not matter
	word iw = 2;

	void Main () {
		ptr = 0xB800;
		
		bVar = ptr[0];  // will be copied one byte into bVar by address 0xB800
		bVar = ptr[1];  // will be copied one byte into bVar by address 0xB801
		bVar = ptr[ib]; // will be copied one byte into bVar by address 0xB802
		bVar = ptr[iw]; // will be copied one byte into bVar by address 0xB802

		wVar = ptr[0];  // will be copied two bytes into wVar by address 0xB800
		wVar = ptr[1];  // will be copied two bytes into wVar by address 0xB802
		wVar = ptr[ib]; // will be copied two bytes into wVar by address 0xB804
		wVar = ptr[iw]; // will be copied two bytes into wVar by address 0xB804

		ptr[0]  = bVar; // will be copied one byte from bVar by address 0xB800
		ptr[1]  = bVar; // will be copied one byte from bVar by address 0xB801
		ptr[ib] = bVar; // will be copied one byte from bVar by address 0xB802
		ptr[iw] = bVar; // will be copied one byte from bVar by address 0xB802

		ptr[0]  = wVar; // will be copied two bytes from wVar by address 0xB800
		ptr[1]  = wVar; // will be copied two bytes from wVar by address 0xB802
		ptr[ib] = wVar; // will be copied two bytes from wVar by address 0xB804
		ptr[iw] = wVar; // will be copied two bytes from wVar by address 0xB804

		ptr[0] = (byte)0xFF; // will be copied one byte 0xFF by address 0xB800
		ptr[0] = (word)0xFF; // will be copied two bytes 0xFF and 0x00 by address 0xB800
	}
}
```

This operation have same bounds of syntax like `*`. 
Because it contradicts the principles of language at the current moment. 
In priority minimum various comands of byte code and using simplest operations only.
- You can't read and store inside one operation.
- You can't store literal consts without cast type.
- You can't store variable value with cast type. (will be realized in future.)

```cpp
// forbidden
ptr[0] = ptr[1];
// allowed
var = ptr[1];
ptr[0] = var;

// forbidden
ptr[0] = 0xFF;
// allowed
ptr[0] = (byte)0xFF;

// forbiden
ptr[0] = (byte)varWithWordType;
// allowed
tmpByte = varWithWordType;
ptr[0] = tmpByte;
```

> The current implementation does not completly satisfy me. And can be changed in future.